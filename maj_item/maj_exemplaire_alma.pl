#!/usr/bin/perl
##############################
# auteur : Rachid
# date : 15-02-2021
# objet : mettre à jour des données exemplaire dans ALma via API. Source de données : un fichier CSV tabulé : CB[tabulation]nom_balise_xml à mettre à jour (la colonne contient les valeurs)
# date : 13-05-2022 : amélioration du programme : travailler avec un CSV contenant plus colonne pour traiter plusieurs en une seule passe
# date : 18-11-2022 : passer la lignes si code barre est vide
# date : 16-12-2022 : encoder correctement en utf8 les champs issus du fichier csv (même si le fichier est en utf8) sinon XML is not well-formed
# date : 06/01/2023 : correctif test si CB existe : faute de frappe corriger : les CB non entierement numérique ne passer pas le filtre : d'où souci avec les CB de type TMP123456
# date : 14/11/2023 : au choix si parametre preserve_resa = yes  : test si resa en cours, dans ce cas , pas de mise à jour -> logs .
##############################
use strict;
#use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use URI::Encode qw(uri_encode uri_decode);; #encoder decoder les URL   - requiert la bibliotheque liburi-encode-perl
use Encode qw(encode decode);
#  Toutes les chaînes de caractères du script sont interprétées comme des chaînes de texte:
use utf8;

#my $nom_balise_xml="";
my @tab_nom_balise_xml=""; # tableau des balise XML à traiter
my @ligne=();
my $nom_fic_csv="";
my $nom_fic_conf="config.conf";
my @tab_inter=();
my %hash_conf=();
my $i=0;
my $cb="";
my $valeur="";
my $block_xml="";
my $mms_id="";
my $holding_id="";
my $item_pid="";
my $item_request="";
my $chaine_a_remplacer="";
my $chaine_de_remplacement="";
my $maj="";
my $erreur="";
my $nbr_colonne=0;

if (!(exists $ARGV[0])) {print "Manque le fichier des exemplaires en parametre.\n";exit(0)};
$nom_fic_csv=$ARGV[0];
##### LIRE LE FICHIER DE CONF

open (CONF, $nom_fic_conf) or die "Ouverture fichier de configuration $nom_fic_conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
@tab_inter = <CONF>; 
close(CONF);
$i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	#print $tab_inter[$i];
	@ligne=split('\t',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf
	

}
print "-" x 20, "\n";

if (length($hash_conf{separateur_csv})<1) { # si le choix du séparateur est vide, le séparateur est forcé à tabulation
	$hash_conf{separateur_csv}='\t';
}


################ TRAITEMENT DU CSV LIGNE A LIGNE
open (CSV, '<:encoding(UTF-8)', $nom_fic_csv) or die "Ouverture fichier de CSV $nom_fic_csv impossible , cause : $! \n";
print "lecture des exemplaires:\n";
print "-" x 20, "\n";
@tab_inter = <CSV>; 
close(CSV);



# recupération du nom des champ XML à mettre à jour
#@ligne=split('\t',$tab_inter[0]); # decoupage ligne
@ligne=split($hash_conf{separateur_csv},$tab_inter[0]); # decoupage ligne
#$nom_balise_xml = epure($ligne[1]);
for ($i=1;$i<=$#ligne;$i++) {
	$tab_nom_balise_xml[$i]= epure($ligne[$i]);
	print "colonne $i => $tab_nom_balise_xml[$i]\n";
	if (length($tab_nom_balise_xml[$i])<1) {
		print "##############\n####### Un intitulé de colonne est vide ? il y a un souci -> arrêt du programme.#####\n############\n";
		exit(0);
	}
}

#exit(0);
######## Purge du rapport d' erreur
open (ERR, ">erreurs-$nom_fic_csv") or die "Ouverture fichier de erreurs impossible , cause : $! \n";
print ERR epure($tab_inter[0])."\tErreur\n"; # on récupére l'entête
close (ERR);


#print "Nom de la balise XML dont il faut changer la valeur : ".$nom_balise_xml."\n##################\n";

########## algo
#### Pour  chaque code barre, on fait un GET item pour obtenir la notice et y extraire le tuple MMS_ID/holdinf_id/item_pid
### si l'exemplaire existe , on remplace la valeur de la balise par la donnée de la colonne et on fait un PUT item pour la mise à jour

for ($i=1;$i<=$#tab_inter;$i++) { # on démarre à 1 , la ligne 0 contient l'entete
	
	@ligne=split($hash_conf{separateur_csv},$tab_inter[$i]); # decoupage ligne
	$cb=epure($ligne[0]);
	if (length($cb)>0) { # si code barre vide , on passe la ligne
	$block_xml=get_item($cb); # Recuperation de l'exemplaire via API
	#$valeur=epure($ligne[1]);
	#print "CB : $cb --- remplacer la valeur de la balise <$nom_balise_xml> par \"$valeur\"\n";
	if ($block_xml !~ /^MAX$/){ # la fonction retourne MAX si elle a echoué 3 fois de suite à faire son get sinon elle retourne le bloc XML
		#print $block_xml,"\n";
		($mms_id,$holding_id,$item_pid,$item_request)=extraire_mms_id_hold_item($block_xml);
		#################
		## Boucle For sur toutes les colonnes
		for ($nbr_colonne=1;$nbr_colonne<=$#tab_nom_balise_xml;$nbr_colonne++){
			print "Remplacement des données dans le block XML pour le tuple mms_id/holding_id/item_pid suivant : ".$mms_id."/".$holding_id."/".$item_pid."\n";
			$block_xml =~ /.*(<$tab_nom_balise_xml[$nbr_colonne].*<\/$tab_nom_balise_xml[$nbr_colonne]>).*/;
			$chaine_a_remplacer =$1;
			$valeur=epure($ligne[$nbr_colonne]);
			#########
			# Chaîne de caractères
			my $enc = 'utf-8';    # Ce programme est encodé et enregistré en UTF-8
			#my $text_str = decode( $valeur, $byte_str );
			#$text_str = lc $text_str;
			$valeur = encode( $enc, $valeur );    # 
			#########
			
			$chaine_de_remplacement="<".$tab_nom_balise_xml[$nbr_colonne].">".$valeur."</".$tab_nom_balise_xml[$nbr_colonne].">";
			if (length($chaine_a_remplacer)<1) { # balise introuvable , on ajoutera la balise juste avant la fin du bloc item_data
				$chaine_a_remplacer ="</item_data>";
				$chaine_de_remplacement="<".$tab_nom_balise_xml[$nbr_colonne].">".$valeur."</".$tab_nom_balise_xml[$nbr_colonne]."></item_data>";
			}
		
			print "Chaine à remplacer: ". $chaine_a_remplacer ."\n";
			print "Chaine de remplacement: ". $chaine_de_remplacement ."\n";
			$block_xml=~ s/\Q$chaine_a_remplacer/$chaine_de_remplacement/; # le /Q veut dire : attention, ne pas interpréter la chaine comme s'il s'agisait d'un regex : en effet la chaine pourrait très bien contenir des caractéres comme | ou /  qui ont une signification particuliére en regex
			#print $block_xml,"\n";
			
		} ## Fin de remplacement de tous les champs
		 if($item_request=~/true/ && $hash_conf{preserve_resa}=~/yes/) # preservation de la resa exemplaire si la conf le demande
			{
			print "PRESERVATION DE LA RESERVATION SUR Exemplaire\n";
			ecrire_log($tab_inter[$i],"Preservation de la reservation sur cet exemplaire");
			} 
			else{
			print "Mise à jour des données via API PUT item pour le tuple mms_id/holding_id/item_pid suivant : ".$mms_id."/".$holding_id."/".$item_pid."\n";
			($maj,$erreur)=put_maj_item($mms_id,$holding_id,$item_pid,$block_xml);
			if ($maj=~/false/) {
				print "ECHEC Mise à jour Exemplaire\n";
				#ecrire_log($cb,$valeur);
				ecrire_log($tab_inter[$i],$erreur);
			} else 
				{
				print "Mise à jour Exemplaire OK\n";
				}
			print "#########################-----------------------#########################\n";
			} # fin test pour preservation de la resa exemplaire
		} else { ## echec du get
			print "ECHEC RECUPERATION Exemplaire\n";
			#ecrire_log($cb,$valeur);
			ecrire_log($tab_inter[$i],"ECHEC RECUPERATION Exemplaire");
	}
  } # fin si code barre vide
}

#########################################################################################################################
########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	
	return $res;
}

############## Extraire et renvoyer le mms_id / holding_id et item_pid d'une notice exemplaire
sub extraire_mms_id_hold_item {
	print "extraire_mms_id_hold_item\n";
	my ($block_xml)=@_;
	my $twig_bib=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
	);
	
	$twig_bib->parse($block_xml); # ici tout est dans la structure twig
	#print $twig_bib->sprint; # affichage du contenu
	
	my $root= $twig_bib->root->first_child('bib_data');  
	my $bib_mms_id = defined $root->first_child('mms_id')?$root->first_child('mms_id')->text:"";
	print "Le MMS_ID est : ".$bib_mms_id."\n";
	
	my $root= $twig_bib->root->first_child('holding_data');  
	my $holding_id = defined $root->first_child('holding_id')?$root->first_child('holding_id')->text:"";
	print "Le holding_id est : ".$holding_id."\n";
	
	my $root= $twig_bib->root->first_child('item_data');  
	my $item_pid = defined $root->first_child('pid')?$root->first_child('pid')->text:"";
	print "Le pid exemplaire est : ".$item_pid."\n";
	
	my $root= $twig_bib->root->first_child('item_data');  
	my $item_request = defined $root->first_child('requested')?$root->first_child('requested')->text:"";
	print "Existance de reservation requested : ".$item_request."\n";
	
	$twig_bib->purge; #liberer la mémoire
	
	return ($bib_mms_id,$holding_id,$item_pid,$item_request);
	
}



############# fonction API GET pour questionner Alma : Recuperer toutes les information d'un exemplaires
sub get_item{
	my ($cb)=@_;  # on récupére l'uid en paramétre
	
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://".$hash_conf{serveur_api_exl}."/almaws/v1/items?view=label&item_barcode=$cb&apikey=".$hash_conf{api_cle_create_hold_item};
	#print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_create_hold_item}, $hash_conf{api_cle_create_hold_item}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=1; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n#############################################################################################","\n";
			#print $res->content;
			print "\nExistence exemplaire OK pour $cb";
			print "\n#############################################################################################","\n";
			$status=$res->content;
		} else {
			print "\n#############################################################################################","\n";
			print $res->status_line, "\n";
			print "\n#############################################################################################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}

############## fonction API PUT pour mettre à jour un exemplaire

sub put_maj_item {
	my($mms_id,$holding_id,$item_pid,$block_xml)=@_;
	my $erreur="";
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet
	######### s'assurer d'avoir un xml en utf8
	my $conv = XML::Twig::encode_convert( 'utf8');
	my $twig_fiche=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
        #pretty_print  => 'indented',
        #output_filter => $conv
	);
	$twig_fiche->parse($block_xml);
	#########
	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	#exemple : 	https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/990000667070205601/holdings/22184992810005601/items/23184992800005601?apikey=#######
	my $chaine_rq="https://".$hash_conf{serveur_api_exl}."/almaws/v1/bibs/$mms_id/holdings/$holding_id/items/$item_pid?apikey=".$hash_conf{api_cle_create_hold_item};
	#print $chaine_rq,"\n";
	my $req = HTTP::Request->new(PUT => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	#$req->content($block_xml);
	$req->content($twig_fiche->sprint);
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=1; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			print $res->content;
			$block_xml=$res->content; # block retour suite appel api post : on est censé avoir un block item XML 
			print "\nRetour appel API OK pour mms_id = $mms_id et holding_id = $holding_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			
			if ($res->content =~ /errorMessage>(.*)<\/errorMessage/) {
				$erreur=$1;
			} else {
				$erreur="Echec mise à jour";
			}

			print "############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
		}
	}
	$twig_fiche->purge; #liberer la mémoire
	
	if ($status !~ /MAX|NOK/) {
	#######Analyse du block XML retourné et alimentation des tables de hash globales
		my $twig_item=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
	
			pretty_print  => 'indented',
		);
	
		$twig_item->parse($block_xml); # ici tout est dans la structure twig
	
		my $root=$twig_item->get_xpath('//item_data',0);
		my $item_pid=$root->first_child('pid')->text ; #item_pid
		$twig_item->purge; #liberer la mémoire
		print "=============> Recuperation du pid item :$item_pid\n";
		return ($item_pid,"");
	} else {
		return ("false",$erreur); # echec de la maj de l'exemplaire
	}
	
}



#### Fonction erreur : ecrire une ligne dans le rapport d'erreur
sub ecrire_log() {
	my ($chaine,$erreur)=@_;

open (ERR, ">>erreurs-$nom_fic_csv") or die "Ouverture fichier des erreurs impossible , cause : $! \n";
print ERR epure($chaine)."\t$erreur\n";
close (ERR);

}
