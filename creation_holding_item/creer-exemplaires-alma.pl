#!/usr/bin/perl
##############################
# auteur : Rachid
# date : 09-12-2020
# objet : creer des exemplaire dans ALma via API. Source de données : un fichier CSV tabulé
# 08-12-2022 : correctif UTF8 - purge memoire XML - gestion holding BC ou BCH
# 12-12-2022 : la liste des champs exemplaire devient libre comme pour maj_exemplaire_alma.pl
# 14-12-2022 : ajout de la gestion 852 indicateur1 
# 23-01-2023 : modif fonction epure : enleve les espaces inutiles avant et apres la chaine
##############################
use strict;
#use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use URI::Encode qw(uri_encode uri_decode);; #encoder decoder les URL   - requiert la bibliotheque liburi-encode-perl

my $nom_fic_csv="";
my %hash_conf=();
my $nom_fic_conf="config.conf";
my @tab_inter = ();
my $i=0;
my $mms_id="";
my %hash_ppn=(); # PPN=> MMS_ID
my %hash_mms_id=(); # MMS_ID=> PPN
my %hash_holding=(); # holding_id =>PPN-B-C-H (ppn,bib,loc,cote)
my %hash_ppn_bch_holding=(); # l'inverse : PPN-B-C-H=>holding_id
my %hash_holding_mms_id=(); # holding_id => mms_id de la notice

my @tab_nom_balise_xml=""; # tableau des balise XML à traiter

#=pod
#$hash_ppn{'(PPN)002381606'}="INCONNU";
#$hash_ppn{'(PPN)002381612'}="990000667070205601";
#$hash_holding{'(PPN)002381610-BUSHS-0SOC-398.474'}="22246639370005601";
#=cut


if (!(exists $ARGV[0])) {print "Manque le fichier des exemplaires en parametre.\n";exit(0)};
$nom_fic_csv=$ARGV[0];
##### LIRE LE FICHIER DE CONF

open (CONF, $nom_fic_conf) or die "Ouverture fichier de configuration $nom_fic_conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
@tab_inter = <CONF>; 
close(CONF);
$i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	#print $tab_inter[$i];
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf
	

}
print "-" x 20, "\n";
################ TRAITEMENT DU CSV LIGNE A LIGNE
open (CSV, $nom_fic_csv) or die "Ouverture fichier de CSV $nom_fic_csv impossible , cause : $! \n";
print "lecture des exemplaires:\n";
print "-" x 20, "\n";
@tab_inter = <CSV>; 
close(CSV);
######## Ouvrir le rapport d'errreur des lignes non traité : par exemple PPN inconnu
open (ERR, ">erreurs.txt") or die "Ouverture fichier de erreurs.txt impossible , cause : $! \n";
print ERR "Motif_rejet\t$tab_inter[0]"; # on récupére l'entête , on ajoute une colone avant pour renseigner ensuite le motif du rejet
close (ERR);



# recupération du nom des champ XML à mettre à jour
#@ligne=split('\t',$tab_inter[0]); # decoupage ligne
my @ligne=split('\t',$tab_inter[0]); # decoupage ligne
#$nom_balise_xml = epure($ligne[1]);
print "Analyse de la colonne entête pour cibler les champs exemplaires (à partir de la colonne 4):\n";
for ($i=0;$i<=3;$i++) {
	print "colonne $i => ".epure($ligne[$i])."\n";
	}

print "##########\n\tColonnes exemplaires\n##########\n";
for ($i=4;$i<=$#ligne;$i++) {
	$tab_nom_balise_xml[$i]= epure($ligne[$i]);
	$tab_nom_balise_xml[$i]=~ s/ //g; # on nettoie les espaces inutiles
	print "\tcolonne $i => $tab_nom_balise_xml[$i]\n";
	if (length($tab_nom_balise_xml[$i])<1) {
		print "##############\n####### Un intitulé de colonne $i est vide ? il y a un souci -> arrêt du programme.#####\n############\n";
		exit(0);
	}
}



$i=1; # on passe la ligne d'entete => on commence à 1
for ($i;$i<=$#tab_inter;$i++) {
	print "#" x 80, "\n";
	print "#" x 80, "\n";
	print "#" x 80, "\n";
	print "############ Traitement de la ligne : ################\n$tab_inter[$i]#########################\n";
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne
	my $ppn=epure($ligne[0]); # récupération du ppn courant 
	my $hold_b=epure($ligne[1]);
	my $hold_c=epure($ligne[2]);
	my $hold_h=epure($ligne[3]);
	my $holding=$ppn."-".$hold_b."-".$hold_c."-".$hold_h; # recuperation de la holding courante
	print $ppn,"\n";
	my $reponse=""; # code retour lors de la création exemplaire : OK ou non
	######  ######
	##### si le PPN existe déjà dans la table des ppn en mémoire avec son MMS_ID : pas besoin de requete API
	#### 	Sinon,	Requete API,	Si le PPN n'existe pas dans Alma: STOP - passage à la ligne suivante
	#### 							Si le PPN existe dans Alma : Mémoriser le MMS_ID
	if($hash_ppn{$ppn} =~ /INCONNU/) { # la requete API a déjà été faite et que le PPN n'existe pas dans Alma
		print "PPN inconnu dans Alma : passage à l'exemplaire suivant.\n";
		# alimenter le fichier de log erreur
		ecrire_log("PPN",$tab_inter[$i]);
		} 
		else { ### 2 cas de figure : la requete API a déjà été faite et on a le MMS_ID , OU , la requete API est à faire
			if (!(exists $hash_ppn{$ppn})) { # Le PPN n'existe pas dans la table des ppn en mémoire : on n'a pas encore fait de requete API sur ce PPN
				# requete API PPN pour récuperer le MMS_ID
				print "Requete API sur le PPN\n";
				my $block_xml=get_bib_mms_id_from_network_number($ppn); # Appel de l'API sur toutes les bibs pour recuperer les mms_id
				if ($block_xml !~ /total_record_count=\"0\"/) { ## notice bib ok => retour OK  ===> doit être différent de bibs total_record_count="0" 
					$mms_id=extraire_mms_id($block_xml); # on récupére le MMS_ID : alimentation du tableau PPN=>MMS_ID
					$hash_ppn{$ppn}=$mms_id;
					$hash_mms_id{$mms_id}=$ppn; # tableau inverse MMS_ID=>PPN
					# Recuperer toutes les holding liées à cette notice bib : api GET holding + alimente tableau holding/b/c/h
					alimente_holding ($hash_ppn{$ppn});
					} 
					else  { # pas de notice renvoyée le PPN est inconnu dans Alma
						$hash_ppn{$ppn}="INCONNU";
						#ecrire la ligne dans le rapport d'erreur
						ecrire_log("PPN",$tab_inter[$i]);
						}
				
				} else { # On a déjà  le MMS_ID
					print "On a déjà le MMS_ID -- PPN=".$ppn." ---MMS_ID=".$hash_ppn{$ppn}."\n";
				
				}
			### Si le PPN a été trouvé (donc pas INCONNU)
			if($hash_ppn{$ppn} =~ /INCONNU/) { # la requete API a déjà été faite et que le PPN n'existe pas dans Alma
				print "PPN inconnu dans Alma : passage à l'exemplaire suivant.\n";
				# alimenter le fichier de log erreur
				#ecrire_log("PPN3",$tab_inter[$i]);
			} else { ### On peut maintenant passer au niveau holding
					###########  Le programme va vérifier l'existence d'une holding correspondante au tuple 852_b/852_c/852_h pour ce PPN
					###########  Si la holding n'existe pas : la CREER
					###########  Mémoriser la holding_id
					print "\tTraitement de la holding sur la base de : ".$holding."\n";
					print "Recherche en cache : ".($hash_ppn_bch_holding{$holding}?$hash_ppn_bch_holding{$holding}:"holding inconnue")."\n";
					if ($hash_ppn_bch_holding{$holding}) { ## Si la holding existe déjà , c'est qu' on a déja fait la requete API
						# Creer exemplaire rattaché à cette holding
						if ($hash_conf{holding_only} !~ /yes/ ) { ## ne pas creer item si on n'est pas en mode holding_only
							print "\tLa holding existe déjà , on peut essayer de creer l'exemplaire rattaché à la holding : ".$hash_ppn_bch_holding{$holding}."\n";
							$reponse=post_create_item($mms_id,$hash_ppn_bch_holding{$holding},$tab_inter[$i]);# creer l'exemplaire : on passe le mms_id , holding_id et la ligne d'exemplaire complete
							if ($reponse=~ /false/) {
								ecrire_log("ITEM",$tab_inter[$i]);
								print "Echec de la creation de l'exemplaire\n.";
								}
						}
					} else { # La holding n'existe pas, il faut la créer d'abord
						print "\tcréation de la holding : $holding\n";
						# Creer de la holding
						my $num_holding=post_create_holding($mms_id,$hold_b,$hold_c,$hold_h); # creation de la holding et récupération du holding_id
						if ($num_holding=~ /false/) {
									ecrire_log("HOLDING",$tab_inter[$i]);
									print "Echec de la creation de la holding\n.";
							} else {
							print "La holding $num_holding a ete cree.\n";
						}
						# Creer exemplaire
						if ($hash_conf{holding_only} !~ /yes/ ) { ## ne pas creer item si on n'est pas en mode holding_only
							print "Creation exemplaire sous la holding $num_holding ...\n";
							$reponse=post_create_item($mms_id,$hash_ppn_bch_holding{$holding},$tab_inter[$i]);# creer l'exemplaire : on passe le mms_id , holding_id et la ligne d'exemplaire complete
							if ($reponse=~ /false/) {
									ecrire_log("ITEM",$tab_inter[$i]);
									print "Echec de la creation de l'exemplaire ou holding\n.";
							}
						}
					}
			}
		}
	print "---------------------------------------------\n";
} ## FIN DU FOR

close (ERR);


########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	$res=~ s/\s+$//; ## enleve les espaces inutiles apres la chaine
	$res=~ s/^\s+//; ## enleve les espaces inutiles avant la chaine
	return $res;
}

#### Fonction erreur : ecrire une ligne dans le rapport d'erreur
sub ecrire_log() {
	my ($motif,$ligne)=@_;

open (ERR, ">>erreurs.txt") or die "Ouverture fichier de erreurs.txt impossible , cause : $! \n";
print ERR "$motif\t$ligne";
close (ERR);

}

########### Alimenter toutes les données sur les holding d'une notice
sub alimente_holding {
	my ($mms_id) = @_; # recuperation des paramétres
	
	my $holding_id="";
	my $holding_bib="";
	my $holding_loc="";
	my $holding_call="";
	print "Recupererer les holdings de la notice MMS_ID = $mms_id\n";
	my $conv = XML::Twig::encode_convert( 'utf8');
	my $twig_holding=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
		pretty_print  => 'indented',
		output_filter => $conv

	);
	my $block_xml=get_holding_from_mms_id($mms_id); # Appel de l'API sur le mms_id pour récuperer toutes les holding
	$twig_holding->parse($block_xml); # ici tout est dans la structure twig
	my $root= $twig_holding->root->first_child('holdings');  
	print "=============> Recuperation de tous les holding_id :\n";
	my @tab_holding_id = $twig_holding->root->children('holding'); 
	foreach my $un_holding_id (@tab_holding_id) {
			$holding_id=$un_holding_id->first_child('holding_id')->text ; #holding_id
			$holding_bib=$un_holding_id->first_child('library')->text ; #code de la bib =>B
			$holding_loc=$un_holding_id->first_child('location')->text ; #code de localisation (collection)=>C
			$holding_call=defined $un_holding_id->first_child('call_number')?$un_holding_id->first_child('call_number')->text:"" ; #cote=>H
			
			print "PPN-HOLDING_ID-BIB-LOC-CALL : $hash_mms_id{$mms_id}-$holding_id-$holding_bib-$holding_loc-$holding_call\n";
			$hash_holding{$holding_id}="$hash_mms_id{$mms_id}-$holding_bib-$holding_loc-$holding_call"; # holding_id =>PPN-B-C-H (ppn,bib,loc,cote)
			$hash_ppn_bch_holding{"$hash_mms_id{$mms_id}-$holding_bib-$holding_loc-$holding_call"}=$holding_id; #idem en inversé :PPN-B-C-H (ppn,bib,loc,cote) => holding_id
			$hash_holding_mms_id{$holding_id}=$mms_id; # holding_id => mms_id de la notice
			
			$holding_id="";
			$holding_bib="";
			$holding_loc="";
			$holding_call="";
	}
	$twig_holding->purge;# liberer la mémoire
	
}

############## Extraire et renvoyer le mms_id d'une notice bib xml
sub extraire_mms_id {
	print "appel fonction extraire_mms_id\n";
	my ($block_xml)=@_;
	my $conv = XML::Twig::encode_convert( 'utf8');
	my $twig_bib=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
		pretty_print  => 'indented',
		output_filter => $conv

	);
	
	$twig_bib->parse($block_xml); # ici tout est dans la structure twig
	my $root= $twig_bib->root->first_child('bib');  
	my $bib_mms_id = defined $root->first_child('mms_id')?$root->first_child('mms_id')->text:"";
	print "Le MMS_ID est : ".$bib_mms_id."\n";
	
	$twig_bib->purge;# liberer la mémoire
	
	return $bib_mms_id;
	
}

############## fonction API GET pour questionner Alma : Recuperer les MMS_ID sur la base du PPN
sub get_bib_mms_id_from_network_number{
	my ($network_number)=@_;  # on récupére le ppn (ou autre network_number) en paramétre
	
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs?view=full&expand=None&view=brief&other_system_id=$network_number&apikey=".$hash_conf{api_cle_create_hold_item};
	#print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			#print $res->content;
			print "Retour appel API OK pour $network_number\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}

############## fonction API GET pour questionner Alma : Recuperer les HOLDING sur la base du MMS_id
sub get_holding_from_mms_id{
	my ($mms_id)=@_;  # on récupére le mms_id en paramétre
	
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id/holdings?apikey=".$hash_conf{api_cle_create_hold_item};

	#print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			#print $res->content;
			print "Retour appel API OK pour $mms_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}
############## fonction API POST pour creer une notice de holding

sub post_create_holding {
	my ($mms_id,$hold_b,$hold_c,$hold_h)=@_;  # on récupére le mms_id et les infos holding en paramétre
	my $block_xml="";
	my $res="";
	#### construction de la fiche holding à importer en XML
	my $fiche_a_importer="<holding><suppress_from_publishing>false</suppress_from_publishing>";
	$fiche_a_importer.="<record>";
	$fiche_a_importer.="<datafield ind1=\"".$hash_conf{'852_indic1'}."\" ind2=\" \" tag=\"852\">";
	$fiche_a_importer.="<subfield code=\"b\">$hold_b</subfield>";
	$fiche_a_importer.="<subfield code=\"c\">$hold_c</subfield>";
	if (length($hold_h)>1) { # le cas ou est en BC et non pas BCH
		$fiche_a_importer.="<subfield code=\"h\">$hold_h</subfield>";
	}
	$fiche_a_importer.="</datafield>";
	$fiche_a_importer.="</record></holding>";
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id/holdings?apikey=".$hash_conf{api_cle_create_hold_item};

	#print $chaine_rq,"\n";
	print $fiche_a_importer,"\n";
	my $req = HTTP::Request->new(POST => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	$req->content($fiche_a_importer);

	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=1; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		$res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			print $res->content;
			$block_xml=$res->content; # block retour suite appel api post : on est censé avoir un block holding XML 
			print "Retour appel API OK pour $mms_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			$block_xml=$res->content;
			print "\n############################","\n";
			print $res->status_line, "\n";
			print $block_xml,"\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	#######Analyse du block XML retourné et alimentation des tables de hash globales
	if ($res->is_success) {
		my $conv = XML::Twig::encode_convert( 'utf8');
		my $twig_holding=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
	
		pretty_print  => 'indented',
		output_filter => $conv
		);
	
		$twig_holding->parse($block_xml); # ici tout est dans la structure twig
		#my $root= $twig_holding->root->first_child('holding');  
		my $root=$twig_holding->get_xpath('//holding',0);
		print "=============> Recuperation de tous les holding_id :\n";
		my $holding_id=$root->first_child('holding_id')->text ; #holding_id
		print "La holding a ete cree =>PPN-HOLDING_ID-BIB-LOC-CALL : $hash_mms_id{$mms_id}-$holding_id-$hold_b-$hold_c-$hold_h\n";
		$hash_holding{$holding_id}="$hash_mms_id{$mms_id}-$hold_b-$hold_c-$hold_h"; # holding_id =>PPN-B-C-H (ppn,bib,loc,cote)
		$hash_ppn_bch_holding{"$hash_mms_id{$mms_id}-$hold_b-$hold_c-$hold_h"}=$holding_id; #idem en inversé :PPN-B-C-H (ppn,bib,loc,cote) => holding_id
		$hash_holding_mms_id{$holding_id}=$mms_id; # holding_id => mms_id de la notice
	
		$twig_holding->purge; #libérer la mémoire
		return $holding_id;
	} else {
		return "false"; # echec creation holding
	}
}

############## fonction API POST pour creer un exemplaire

sub post_create_item {
	my ($mms_id,$holding_id,$ligne_item)=@_;  # on récupére le mms_id , holding_id et la ligne d'exemplaire
	my $block_xml="";
	my $nbr_colonne=0;
	my @ligne=split('\t',$ligne_item); # decoupage ligne

	my $fiche_a_importer="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	$fiche_a_importer.="<item><item_data>";

	## Boucle For sur toutes les intitulés de colonnes item : à partir de la colonne 4
		for ($nbr_colonne=4;$nbr_colonne<=$#tab_nom_balise_xml;$nbr_colonne++){
			if (length (epure($ligne[$nbr_colonne]))>0) { 
				# s'il y a une valeur dans la colonne on construit la balise : ouvrir 
				# ouvrir la balise - ajouter la valeur - fermer la balise
				$fiche_a_importer.="<".$tab_nom_balise_xml[$nbr_colonne].">";
				$fiche_a_importer.=epure($ligne[$nbr_colonne]);
				$fiche_a_importer.="</".$tab_nom_balise_xml[$nbr_colonne].">";
				}
			
		}
	$fiche_a_importer.="</item_data></item>";
	print "---------\n".$fiche_a_importer."\n-----------";
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id/holdings/$holding_id/items?apikey=".$hash_conf{api_cle_create_hold_item};
	#print $chaine_rq,"\n";
	my $req = HTTP::Request->new(POST => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	$req->content($fiche_a_importer);
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			print $res->content;
			$block_xml=$res->content; # block retour suite appel api post : on est censé avoir un block item XML 
			print "\nRetour appel API OK pour mms_id = $mms_id et holding_id = $holding_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
		}
	}
	if ($status !~ /MAX|NOK/) {
	#######Analyse du block XML retourné et alimentation des tables de hash globales
		my $conv = XML::Twig::encode_convert( 'utf8');
		my $twig_item=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
	
			pretty_print  => 'indented',
			output_filter => $conv

		);
	
		$twig_item->parse($block_xml); # ici tout est dans la structure twig
	
		my $root=$twig_item->get_xpath('//item_data',0);
		my $item_pid=$root->first_child('pid')->text ; #item_pid
		$twig_item->purge; #libérer la mémoire
		print "=============> Recuperation du pid item :$item_pid\n";
		return $item_pid;
	} else {
		return "false"; # echec de la création de l'exemplaire
	}
	
}
