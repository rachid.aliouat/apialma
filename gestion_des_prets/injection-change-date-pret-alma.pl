#!/usr/bin/perl -w
##################
# Rachid Aliouat
# Le 18/04/2019
# -Lire un fichier XML contenant 1 ou plusieurs fiches prêt
# -S'il existe pas : créer le prêt avec l'API POST
#######################
use strict;
use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use MIME::Lite;			# bibliothèque pour envoye un mail (rapport)

my %borrower=(); # liste des lecteurs = les primary_id
my %item_due_date=(); # tableau ibarcode/date de retour
my %item_borrower=(); # tableau ibarcode / lecteur
my %item_loan=(); # tableau ibarcode / numero du pret

##### LIRE LE FICHIER DE CONF
my %hash_conf=();
open (CONF, "config.conf") or die "Ouverture fichier de configuration config.conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
my @tab_inter = <CONF>; 
close(CONF);
my $i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	print $tab_inter[$i];
	my @ligne=split('=',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf

}
print "-" x 20, "\n";

# Lire le fichier XML passé en parametre
my $nom_fic=$ARGV[0];

my $twig=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
);
$twig->parsefile($nom_fic); # ici tout est dans la structure twig

#$twig->sprint; # affichage du contenu

my $root= $twig->root;  
my @les_prets= $root->children;  ### on récuperer une table de hash de tous les "enfants" du root = les entrées <loan> de la racine <loans>
$i=0; # compteur generale
my $nb_maj_date=0;
my $nb_creation=0;
my $nb_echec_creation=0;
my $nb_echec_maj_date=0;
my $primary_id ="";
my $ibarcode="";
my $reponse=""; # pour analyse des codes retour
my $block_xml="";
my $date_retour="";
my $rapport="Traitement du fichier : $nom_fic \n\n";
$rapport.="Rapport des anomalies MAJ DATE DE RETOUR prêts vers Alma :\n";
$rapport.="Il y a ".scalar @les_prets ." fiche(s) XML.\n";
$rapport.="###########\n";
$rapport.="Liste des anomalies : \n----------------\n";
$rapport.="MISE A JOUR DATE DE PRETOUR DES PRETS.\n";
foreach my $un_pret (@les_prets)      # liste chaque PRET de la liste des PRETS
 { 
	#$un_lecteur->print;               # afficher contenu xml de un_lecteur
   print "\n########################\n"; 
   # ################### RECUPÉRATION DU PRIMARY ID ibarcode et date_retour
   $primary_id = $un_pret->first_child('user_id')->text();
   $ibarcode = $un_pret->first_child('item_barcode')->text();
   $date_retour = $un_pret->first_child('due_date')->text();
   ######## Alimentation des table pour plus tard
   $borrower{$primary_id}++;
   $item_due_date{$ibarcode}=$date_retour;
   $item_borrower{$ibarcode}=$primary_id;
   ##########
   print "LECTEUR PRIMARY ID = $primary_id\n";
   print "ITEM BARCODE = $ibarcode\n";
   print "DATE RETOUR = $date_retour\n";
   $block_xml = $un_pret->sprint;
   print "################\n";
   # ################### QUESTIONER API POST pour créer le pret
   	#### Faire un get sur ce borrower pour récuperer ses prêts en cours
	$block_xml=get_pret($primary_id);
	#print $block_xml;
	if ($block_xml =~ /item_loan/) { ## lecteur ok 
	$twig->parse($block_xml); # ici tout est dans la structure twig
	print $twig->sprint; # affichage du contenu
	my $root= $twig->root;  
	########### Changer la date des prêts 
	#######################################################
	### l'API POST applique la regle de gestion Alma . il faut faire un PUT si on veut imposer la date
	### Pour chaque borrower, il faut récupperer les liste de ses prêts, recuperer le loan_id et le ibarcode de chaque pret pour pouvoir ensuite faire un PUT	
	my @les_prets= $root->children;  ### on récuperer une table de hash de tous les "enfants" du root = les entrées <loan> de la racine <loans>
	foreach my $un_pret (@les_prets)      # liste chaque PRET de la liste des PRETS
		{ 
		my $loan_id=$un_pret->first_child('loan_id')->text(); # on récupere le numero de prêt
		my $item_barcode=$un_pret->first_child('item_barcode')->text(); # on recupere le code barre ouvrage
		if ($item_due_date{$item_barcode}) { # Si le code barre est dans le tabeleau des code barre alimenté plus haut , alors on rallonge le pret
			$reponse = put_pret($loan_id,$item_due_date{$item_barcode}); # changer la date de prêt : loan_id,date_retour
			if ($reponse =~ /OK/) { # la date a été changé
				print "Modification date du PRET OK (core)\n";
				$nb_maj_date++;
		
					} else { # Echec de la création du pret
					print "MODIFCATION date du PRET ECHEC (core)\n";
					print "################\n";
				$block_xml = $un_pret->sprint;
				$nb_echec_maj_date++;
				$rapport.="ECHEC  : CODE BARRE LIVRE $ibarcode --- DATE DE RETOUR $item_due_date{$item_barcode} \n";
				
				}
		}
			
		}
   ##############################
   #### compteur
   $i++;
 } else {
	 $rapport.="Code lecteur $primary_id inconnu\n";
	 $nb_echec_maj_date++;
 }
}
$rapport.="--------------------------\n";
$rapport.="$nb_creation OK et $nb_echec_creation\n";
$rapport.="--------------------------\n";
$rapport.="--------------------------\n";
#######################################################




$rapport.="--------------------------\n";
$rapport.="$nb_maj_date OK et $nb_echec_maj_date ECHEC\n";


###############
#envoie email 
###############
my $date=date_claire(time);
print $date, "\n";

print  "Envoie email d'informations sur le creation de prêt Alma :\n";
my $email_subject = "[IMPORT-ALMA-PRETS]Rapport en date du $date";
my $email_body = $rapport;
my $Message = new MIME::Lite
From =>$hash_conf{from},
To =>$hash_conf{destinataires}, 
subject =>$email_subject,
Type =>'multipart/mixed';
attach $Message
	Type =>'TEXT',
	Data =>$email_body;

my $return=$Message->send_by_smtp($hash_conf{smtp});
print $return==1?"OK\n":"Echec\n";


############################
########## Fonctions
############################

########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	
	return $res;
}


############## fonction API POST pour créer le pret dans Alma 
sub post_pret{
		my ($uid,$item,$fiche_xml)= @_;
	my $fiche_a_importer="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" . $fiche_xml ;
	print "\n#############################################################################################","\n";
	print "AJOUT du pret pour le lecteur lecteur $uid\n";
	print "\n#############################################################################################","\n";
	print "CONTENU DE LA FICHE :\n";
	print "\n######################## FICHE XML #####################################################################","\n";
	print $fiche_a_importer,"\n";
	print "\n######################## FICHE XML #####################################################################","\n";
	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/$uid/loans?user_id_type=all_unique&item_barcode=$item&apikey=".$hash_conf{api_cle};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(POST => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user}, $hash_conf{api_cle}); # login  + mot de passe => user_api + cle api
	$req->content($fiche_a_importer);
	# Passer requete au user agent et récuperer la response
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /NOK/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n#############################################################################################","\n";
			#print $res->content;
			print "Ajout OK pour $uid";
			print "\n#############################################################################################","\n";
			$status="OK";
		} else {
			print "\n#############################################################################################","\n";
			print $res->status_line, "\n";
			
			print "\n#############################################################################################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
		
	return $status;
}

############## fonction API GET pour questionner Alma : Recuperer tous les prêt d'un lecteur
sub get_pret{
	my ($uid)=@_;  # on récupére l'uid en paramétre
	
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/".$uid."/loans?user_id_type=all_unique&limit=100&offset=0&order_by=id&direction=ASC&apikey=".$hash_conf{api_cle};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user}, $hash_conf{api_cle}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n#############################################################################################","\n";
			print $res->content;
			print "Existence de prets OK pour $uid";
			print "\n#############################################################################################","\n";
			$status=$res->content;
		} else {
			print "\n#############################################################################################","\n";
			print $res->status_line, "\n";
			print "\n#############################################################################################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}

############## fonction API PUT pour mettre à jour la fiche lecteur dans Alma 
sub put_pret{
	my ($loan_id,$date_retour)= @_;
	my $fiche_a_importer="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"  ;
	$fiche_a_importer.="<item_loan><due_date>$date_retour</due_date></item_loan>";
	
	print "\n#############################################################################################","\n";
	print "MISE A JOUR du pret $loan_id\n";
	print "\n#############################################################################################","\n";
	print "CONTENU DE LA FICHE :\n";
	print "\n######################## FICHE XML #####################################################################","\n";
	print $fiche_a_importer,"\n";
	print "\n######################## FICHE XML #####################################################################","\n";
	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/13101796/loans/$loan_id?apikey=".$hash_conf{api_cle};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(PUT => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user}, $hash_conf{api_cle}); # login  + mot de passe => user_api + cle api
	$req->content($fiche_a_importer);
	# Passer requete au user agent et récuperer la response
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /NOK/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n#############################################################################################","\n";
			#print $res->content;
			print "Mise à jour OK pour $loan_id";
			print "\n#############################################################################################","\n";
			$status="OK";
		} else {
			print "\n#############################################################################################","\n";
			print $res->status_line, "\n";
			print $res->content,"\n";
			print "\n#############################################################################################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
		
	return $status;
}

############################
sub date_claire { # Passage d'une date et retour d'une date en clair exemple : Lundi 22-09-2008 � 15h00
my @cejour = localtime($_[0]);
my $cejour_jour= $cejour[3];
my $cejour_mois= $cejour[4]+1;
my $cejour_annee= $cejour[5]+1900;
my $cejour_heure= $cejour[2];
my $cejour_minute= $cejour[1];
my $cejour_libelle=("Dimanche","Lundi", "Mardi", "Mercredi" ,"Jeudi", "Vendredi", "Samedi")[$cejour[6]];
#my $texte_date_claire= "$cejour_libelle $cejour_jour-$cejour_mois-$cejour_annee " . chr(133) . " $cejour_heure"."H".(length($cejour_minute)==1?"0" . $cejour_minute:$cejour_minute);
my $texte_date_claire= "$cejour_libelle ".(length($cejour_jour)==1?"0" . $cejour_jour:$cejour_jour)."-".(length($cejour_mois)==1?"0" . $cejour_mois:$cejour_mois)."-$cejour_annee -" . " $cejour_heure"."H".(length($cejour_minute)==1?"0" . $cejour_minute:$cejour_minute);
return  $texte_date_claire;
}
############################
sub date_simple { # Passage d'une date et retour d'une date en clair 
my @cejour = localtime($_[0]);
my $cejour_jour= $cejour[3];
my $cejour_mois= $cejour[4]+1;
my $cejour_annee= $cejour[5]+1900;
my $cejour_heure= $cejour[2];
my $cejour_minute= $cejour[1];
my $cejour_libelle=("Dimanche","Lundi", "Mardi", "Mercredi" ,"Jeudi", "Vendredi", "Samedi")[$cejour[6]];
my $texte_date_claire= (length($cejour_jour)==1?"0" . $cejour_jour:$cejour_jour) . "\/" . (length($cejour_mois)==1?"0" . $cejour_mois:$cejour_mois) . "\/" . $cejour_annee;
return  $texte_date_claire;
}
