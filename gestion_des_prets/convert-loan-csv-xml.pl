#!/usr/bin/perl -w
##################
# Rachid Aliouat
# Le 11/07/2019
# -convertir un csv contenant des pret en un fichier XML 
#######################
use strict;
use warnings;

## Format CSV tabule attendu 
## user_id	item_barcode	library	due_date(aaaa-mm-jj)

my $nom_fic_csv=$ARGV[0];

$nom_fic_csv=~/(.*)\..*/;
my $nom_fic_xml=$1.".xml";
print "convertion du fichier $nom_fic_csv en $nom_fic_xml\n";

open (XML, ">".$nom_fic_xml) or die "Ouverture fichier $nom_fic_xml impossible , cause : $! \n";

print XML "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<item_loans>";



open (CSV, $nom_fic_csv) or die "Ouverture fichier $nom_fic_csv impossible , cause : $! \n";
my @tab_inter = <CSV>; 
close(CSV);
my $i=0;

for ($i=1;$i<=$#tab_inter;$i++) { # on passe la 1er ligne d'entete
	print $tab_inter[$i];
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne

print XML "<item_loan>
    <circ_desk>DEFAULT_CIRC_DESK</circ_desk>
    <library>".$ligne[2]."</library>
     <user_id>".$ligne[0]."</user_id>
    <item_barcode>".$ligne[1]."</item_barcode>
    <due_date>".epure($ligne[3])."</due_date>
    <loan_status>Active</loan_status>
    <process_status>NORMAL</process_status>
</item_loan>\n";


}
print XML "<\/item_loans>";
close(XML);

########
# FONCTIONS
########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	
	return $res;
}
