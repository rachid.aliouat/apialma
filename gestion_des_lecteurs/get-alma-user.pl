#!/usr/bin/perl -w
##################
# Rachid Aliouat
# Le 26/02/2025 
# -Lire un fichier txt contenant une liste de primary_id
# -Recuperer la fiche XML avec l'api GET 
# -enregistrer la fiche avec le nom primary_id.xml
#######################
use strict;
use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use MIME::Lite;			# bibliothèque pour envoye un mail (rapport)
use utf8;

##### LIRE LE FICHIER DE CONF
my %hash_conf=();
open (CONF, "config.conf") or die "Ouverture fichier de configuration config.conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
my @tab_inter = <CONF>; 
close(CONF);
my $i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	print $tab_inter[$i];
	my @ligne=split('=',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf

}
print "-" x 20, "\n";


##### LIRE LE FICHIER txt/csv avec les primary_id
my %hash_liste=();
my $nom_fic_txt=$ARGV[0];
open (FIC, $nom_fic_txt) or die "Ouverture fichier de la liste primary_id impossible , cause : $! \n";
print "lecture du fichier des primary_id:\n";
print "-" x 20, "\n";
@tab_inter = <FIC>; 
close(FIC);
$i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
    #print $tab_inter[$i];
	$hash_liste{epure($tab_inter[$i])}++; # alimentation de la table de hash avec les primary_id
}
print "-" x 20, "\n";

#### Pour chaque primary_id , api get de la fiche et enregistrement sur disque dans fichier primary_id.xml

my $primary_id;
my $v;
my $reponse;
my $fiche_origine="";
my $twig=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
);
open(ERR,">erreurs.txt");
# Boucler sur le contenu de la table hash_liste
while (($primary_id,$v) = each(%hash_liste)) {
	#print $v."\t".$c."\n";
    print "Recupération du compte ".$primary_id."\n";

    # ################### QUESTIONER API GET POUR SAVOIR S'IL EXISTE OU PAS
   ($reponse,$fiche_origine)=get_user($primary_id); # appel de la fonction qui va questionner le webservice GET pour savoir si la personne existe déjà
   if ($reponse =~ /OK/) { # la personne existe déjà => on peut faire une mise à jour avec un PUT
		print "############## AFFICHAGE FICHE ORIGINE ####################\n";
		print $fiche_origine,"\n";
		print "###########################################################\n";
		############
        $twig->parse($fiche_origine); # ici tout est dans la structure twig
        open (FIC,'>:encoding(UTF-8)' ,"$primary_id".".xml") or die "Ouverture fichier de la liste primary_id impossible , cause : $! \n";

        print FIC $twig->sprint; # affichage du contenu
        #$twig->flush( FIC, pretty_print => 'indented');
        close(FIC);
 		
	} else { # la personne n'existe pas => on consigne en erreur
		print "Le compte lecteur n'existe pas dans Alma\n";
		print "################\n";
        print ERR "$primary_id : inconnu dans Alma\n";
		
   }



}
close(ERR);
############################
########## Fonctions
############################

########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	
	return $res;
}

############## fonction API GET pour questionner Alma : savoir si le lecteur existe déjà
sub get_user{
	my ($uid)=@_;  # on récupére l'uid en paramétre
	my $fiche_xml="";
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/".$uid."?user_id_type=all_unique&view=full&expand=none&apikey=".$hash_conf{api_cle};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user}, $hash_conf{api_cle}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la response
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /NOK/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n#############################################################################################","\n";
			$fiche_xml = $res->content;
			#print $fiche_xml;
			print "Existence OK pour $uid";
			print "\n#############################################################################################","\n";
			$status="OK";
		} else {
			print "\n#############################################################################################","\n";
			print $res->status_line, "\n";
			print "\n#############################################################################################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return ($status, $fiche_xml);
}