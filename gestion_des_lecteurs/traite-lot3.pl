#!/usr/bin/perl
##############################
# auteur : Rachid
# date : 08-01-2019
# objet : appeler un programme pour lui faire traiter un lot de fichier. Puis renommer les fichiers traités.
# 
# parametre : fichier de conf 
# version  2 : choix de la place du nom de fichier passé en paramétre
# version 3 : possibilité du parametre --FIC-- que le nom du fichier ou --PARAM-- (construction du chemin+fichier)
##############################
use MIME::Lite;

%hash_conf=();
%hash_fichiers_a_traiter=();
############
#Lire le fichier de conf
#####################
print "nombre d'arguments : " . (($#ARGV)+1) , "\n";

if ($#ARGV ==0) { # il y a un argument
	$fic_conf=$ARGV[0];
	print "Fichier de conf = $fic_conf\n";
} else {
		print "Probleme avec les liste des arguments :\n";
		print "Arguments attendus : fichier_de_conf\n";
		exit(0);
		}
#####################
print "-----------\n";
print "lecture du fichier de conf $fic_conf : \n";
open (CONF, "$fic_conf") or die "Ouverture fichier de configuration $fic_conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
@tab_inter = <CONF>; 
close(CONF);
for ($i=0;$i<=$#tab_inter;$i++) {
	@ligne=split('=',$tab_inter[$i]); # decoupage ligne
	chomp($ligne[1]);
	$hash_conf{$ligne[0]}=$ligne[1]; # alimentation de la table de hash avec la conf
	print $ligne[0] ,"->",$hash_conf{$ligne[0]} ,"\n";

}
print "-" x 20, "\n";

#####################
##### Lire le répertoire et stocker les fichiers retenus pour traitement
#####################
opendir(my $dh, $hash_conf{repertoire_a_traiter}) || die "Impossible de lire $hash_conf{repertoire_a_traiter}: $!";
print "contenu du repertoire ",$hash_conf{repertoire_a_traiter}," :\n";

while ($fic = readdir $dh) {
	if ($fic =~ /^($hash_conf{masque_de_lecture})$/ ) {
		print "fichier retenu pour traitement : $1 \n";
		$hash_fichiers_a_traiter{$1}++; # alimenter la table de hash des fichier à traiter
	}
	
}
closedir $dh;

#####################
############ Traiter les fichiers
#####################
while (($nom_fichier,$indice) = each(%hash_fichiers_a_traiter)) {
	$chemin_et_fichier = $hash_conf{repertoire_a_traiter}.$nom_fichier;
	$appel_prog=$hash_conf{programme} ;
	$appel_prog=~ s/--PARAM--/$chemin_et_fichier/; 
	$appel_prog=~ s/--FIC--/$nom_fichier/; 
	print "$appel_prog \n";
	system "$appel_prog";
}

### SORTIR ICI : NE PAS RENOMMER LES FICHIERS s'il y a dans le fichier de conf : renomme=non

if ($hash_conf{renomme}=~/non/ ) {
	exit(0);
}

#####################
############ Renommer les fichiers traités
#####################
while (($nom_fichier,$indice) = each(%hash_fichiers_a_traiter)) {
	$appel_prog=$hash_conf{programme} ;
	print "RENOMMER $hash_conf{repertoire_a_traiter}$nom_fichier\n";
	system "mv $hash_conf{repertoire_a_traiter}$nom_fichier $hash_conf{repertoire_a_traiter}traite-$nom_fichier";
}

#####################
############ FIN
#####################
print "#########\nFIN.\n";

