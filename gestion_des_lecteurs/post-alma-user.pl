#!/usr/bin/perl -w
##################
# Rachid Aliouat
# Le 26/02/2025 
# -Lire un fichier user xml utf8 contenant une fiche lecteur alma
# -Creer dans Alma la fiche lecteur
#######################
use strict;
use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use MIME::Lite;			# bibliothèque pour envoye un mail (rapport)
use utf8;
use Text::Unidecode;

##### LIRE LE FICHIER DE CONF
my %hash_conf=();
open (CONF, "config.conf") or die "Ouverture fichier de configuration config.conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
my @tab_inter = <CONF>; 
close(CONF);
my $i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	print $tab_inter[$i];
	my @ligne=split('=',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf

}
print "-" x 20, "\n";
##### Lire le fichier XML passé en parametre
my $nom_fic=$ARGV[0];

my $twig=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
);
$twig->parsefile($nom_fic); # ici tout est dans la structure twig

$twig->print; # affichage du contenu
my $fiche = $twig->sprint;
$fiche =~  /<primary_id>(.*)<\/primary_id>/;
my $primary_id = $1; # recupération du primary_id
###########
open(ERR,">>erreurs.txt");

my $reponse=post_user($primary_id,$fiche); # appel de la fonction qui va questionner le webservice POST pour creation de la fiche
if ($reponse !~ /OK/) {
	print ERR $primary_id . " : Echec de la creation\n";

}
close(ERR);
############################
########## Fonctions
############################

########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	
	return $res;
}

############## fonction API POST pour créer la fiche lecteur dans Alma 
sub post_user{
	my ($uid,$fiche_xml)= @_;
	print "AJOUT du lecteur $uid\n";
	print "\n#############################################################################################","\n";
	print "CONTENU DE LA FICHE :\n";
	print "\n######################## FICHE XML #####################################################################","\n";
	print $fiche_xml,"\n";
	print "\n######################## FICHE XML #####################################################################","\n";
	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users?social_authentication=false&send_pin_number_letter=false&apikey=".$hash_conf{api_cle};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(POST => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user}, $hash_conf{api_cle}); # login  + mot de passe => user_api + cle api
	$req->content(unidecode($fiche_xml));
	# Passer requete au user agent et récuperer la response
	my $i=0;
	my $max=1; #maximum de tentative
	my $status="NOK";
	while ($status =~ /NOK/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n#############################################################################################","\n";
			#print $res->content;
			print "Ajout OK pour $uid";
			print "\n#############################################################################################","\n";
			$status="OK";
		} else {
			print "\n#############################################################################################","\n";
			print $res->status_line, "\n";
			
			print "\n#############################################################################################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
		
	return $status;
}