#!/usr/bin/perl -w
##################
# Rachid Aliouat
# Le 28/02/2025 
# -Lire un fichier csv tabulé contenant deux colonne : primary_id et passwd
# -Ajouter pour chaque fichier primary_id.XML, le password présent dans le csv
#######################
use strict;
use warnings;
use XML::Twig;			# bibliothèque pour le XML
use utf8;
use Text::Unidecode;

##### LIRE LE FICHIER txt/csv avec les primary_id et password
my %hash_liste=();
my $primary_id="";
my $passwd="";
my $nom_fic_txt=$ARGV[0];
open (FIC, $nom_fic_txt) or die "Ouverture fichier de la liste primary_id et password impossible , cause : $! \n";
print "lecture du fichier des primary_id et password:\n";
print "-" x 20, "\n";
my @tab_inter = <FIC>; 
close(FIC);
my $i=0;

for ($i=1;$i<=$#tab_inter;$i++) { # on passe la ligne d'entete
    print $tab_inter[$i];
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne
    if (length($ligne[0])>1) {
	    $hash_liste{epure($ligne[0])}=epure($ligne[1]); # alimentation de la table de hash avec les primary_id et password
    }
    
}
print "-" x 20, "\n";

### charger chaque fichier xml et inserer le passwd
while (($primary_id,$passwd) = each(%hash_liste)) {
    my $nom_fic=$primary_id.".xml";

    my $twig=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
    );
    $twig->parsefile($nom_fic); # ici tout est dans la structure twig

    #$twig->print; # affichage du contenu
    my $fiche = $twig->sprint;
    $fiche =~  s/<password><\/password>/<password>$passwd<\/password>/;
    print $fiche;
    ## Remplacer la fiche sur disque
    open (FIC,'>:encoding(UTF-8)' ,"$primary_id".".xml") or die "Ouverture fichier de la liste primary_id impossible , cause : $! \n";
    print FIC $fiche; # affichage du contenu
    close(FIC);
}


############################
########## Fonctions
############################

########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	
	return $res;
}