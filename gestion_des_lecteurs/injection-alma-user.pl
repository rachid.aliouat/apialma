#!/usr/bin/perl -w
##################
# Rachid Aliouat
# Le 18/04/2019 
# -Lire un fichier XML contenant 1 ou plusieurs fiches lecteurs
# -Tester avec l'api GET si l'utilisateur existe déjà
# -S'il existe : mettre à jour l'utilisateur ave l'API PUT
# -S'il existe pas : créer l'utilisateur ave l'API POST 
# Le 02/09/2019 : 
#+ ajouter la balise <external_id>SIS</external_id> si c'est une creation POST
# Le 10/12/2019
#+ préservation des blocages en cas de update
# Le 23/12/2019
#+ préservation des notes en cas de update
#######################
use strict;
use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use MIME::Lite;			# bibliothèque pour envoye un mail (rapport)

##### LIRE LE FICHIER DE CONF
my %hash_conf=();
open (CONF, "config.conf") or die "Ouverture fichier de configuration config.conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
my @tab_inter = <CONF>; 
close(CONF);
my $i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	print $tab_inter[$i];
	my @ligne=split('=',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf

}
print "-" x 20, "\n";

# Lire le fichier XML passé en parametre
my $nom_fic=$ARGV[0];

my $twig=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
);
$twig->parsefile($nom_fic); # ici tout est dans la structure twig

#$twig->print; # affichage du contenu

my $root= $twig->root;  
my @les_lecteurs= $root->children;  ### on récuperer une table de hash de tous les "enfants" du root = les entrées <user> de la racine <users>
$i=0; # compteur generale
my $nb_maj=0;
my $nb_creation=0;
my $nb_echec=0;
my $primary_id ="";
my $nom="";
my $reponse=""; # pour analyse des code retour
my $block_xml="";
my $fiche_origine="";
my $blocage="";
my $note="";
my $rapport="Traitement du fichier : $nom_fic \n\n";
$rapport.="Liste des anomalies import Lecteur exterieur Alma :\n";
$rapport.="Il y a ".scalar @les_lecteurs ." fiche(s) XML.\n";
$rapport.="###########\n";
$rapport.="Liste des anomalies : \n----------------\n";

foreach my $un_lecteur (@les_lecteurs)      # liste chaque LECTEUR de la liste des LECTEURS
 { 
	#$un_lecteur->print;               # afficher contenu xml de un_lecteur
	$fiche_origine="";
	$blocage="";
	$note="";
   print "\n########################\n"; 
   # ################### RECUPÉRATION DU PRIMARY ID
   $primary_id = $un_lecteur->first_child('primary_id')->text();
   $nom = $un_lecteur->first_child('last_name')->text() . " " . $un_lecteur->first_child('first_name')->text();
   print "PRIMARY ID = $primary_id\n";
   print "################\n";
   # ################### QUESTIONER API GET POUR SAVOIR S'IL EXISTE OU PAS
   ($reponse,$fiche_origine)=get_user($primary_id); # appel de la fonction qui va questionner le webservice GET pour savoir si la personne existe déjà
   if ($reponse =~ /OK/) { # la personne existe déjà => on peut faire une mise à jour avec un PUT
		print "Le compte lecteur existe déjà dans Alma\n";
		print "################\n";
		$block_xml = $un_lecteur->sprint;
		print "############## AFFICHAGE FICHE ORIGINE ####################\n";
		print $fiche_origine,"\n";
		print "###########################################################\n";
		#### ATTENTION : PRESERVER LES BLOCAGES S'IL Y EN A
		 
		if ($fiche_origine=~/<user_blocks>/) { 
			print "Preservation des blocages : \n#############\n";## s'il y a des blocages il faut preserver ces blocages
			$fiche_origine=~ /(<user_blocks>.*<\/user_blocks>)/; # on extrait les blocages de la fiche d'origine
			$blocage=$1; # stockage des blocages
			print "$blocage\n";
			$block_xml=~ s/<\/user>/$blocage/; # on remplace la balise de fin de fiche XML par les blocages
			$block_xml.= "\n<\/user>\n"; # on rajoute la balise de fin de  fiche XML enlevée précédement
			print "\n#############\n";
			print "Les blocages seront injectés dans la nouvelle fiche.\n###########\n";
		}
		#### ATTENTION : PRESERVER LES NOTES S'IL Y EN A
		 
		if ($fiche_origine=~/<user_notes>/) { 
			print "Preservation des notes : \n#############\n";## s'il y a des notes il faut preserver ces notes
			$fiche_origine=~ /(<user_notes>.*<\/user_notes>)/; # on extrait les notes de la fiche d'origine
			$note=$1; # stockage des notes
			print "$note\n";
			$block_xml=~ s/<\/user>/$note/; # on remplace la balise de fin de fiche XML par les notes
			$block_xml.= "\n<\/user>\n"; # on rajoute la balise de fin de  fiche XML enlevée précédement
			print "\n#############\n";
			print "Les notes seront injectés dans la nouvelle fiche.\n###########\n";
		}


		############
		print $block_xml,"\n";
		$reponse=put_user($primary_id,$block_xml); # appel de la fonction qui va mettre à jour la fiche lecteur via le webservice PUT 
		if ($reponse =~ /OK/) { # Mise à jour OK
			print "MISE A JOUR OK (core)\n";
			$nb_maj++;
			
		} else {
			print "ECHEC DE LA MISE A JOUR (core)\n";
			$rapport.="ECHEC DE LA MISE A JOUR pour le lecteur : " . $primary_id ." - " .$nom."\n";
			$nb_echec++;
		}
 		
	} else { # la personne n'existe pas => on doit faire une création avec un POST
		print "Le compte lecteur n'existe pas dans Alma\n";
		print "################\n";
		$block_xml = $un_lecteur->sprint;
		$reponse=post_user($primary_id,$block_xml); # appel de la fonction qui va créer la fiche lecteur via le webservice POST 
		if ($reponse =~ /OK/) { # Ajout OK
			print "Ajout OK (core)\n";
			$nb_creation++;
		} else {
			print "ECHEC DE LA CREATION DU COMPTE LECTEUR (core)\n";
			$rapport.="ECHEC DE LA CREATION pour le lecteur : " . $primary_id ." - " .$nom ."\n";
			$nb_echec++;
		}

   }
   ##############################
   #### compteur
   $i++;
 }

print "Il y a $i fiche(s) XML.\n";
$rapport.="--------------------------\n";
$rapport.="$i fiche(s) = $nb_maj mise(s) a jour + $nb_creation creation(s) + $nb_echec echec(s)\n";
print $rapport;
###############
#envoie email 
###############
my $date=date_claire(time);
print $date, "\n";

print  "Envoie email d'informations sur le import lecteurs Alma :\n";
my $email_subject = "[IMPORT-ALMA-LECTEURS-EXTERIEURS]Rapport en date du $date";
my $email_body = $rapport;
my $Message = new MIME::Lite
From =>$hash_conf{from},
To =>$hash_conf{destinataires}, 
subject =>$email_subject,
Type =>'multipart/mixed';
attach $Message
	Type =>'TEXT',
	Data =>$email_body;

my $return=$Message->send_by_smtp($hash_conf{smtp});
print $return==1?"OK\n":"Echec\n";


############################
########## Fonctions
############################

########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	
	return $res;
}

############## fonction API GET pour questionner Alma : savoir si le lecteur existe déjà
sub get_user{
	my ($uid)=@_;  # on récupére l'uid en paramétre
	my $fiche_xml="";
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/".$uid."?user_id_type=all_unique&view=full&expand=none&apikey=".$hash_conf{api_cle};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user}, $hash_conf{api_cle}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la response
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /NOK/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n#############################################################################################","\n";
			$fiche_xml = $res->content;
			#print $fiche_xml;
			print "Existence OK pour $uid";
			print "\n#############################################################################################","\n";
			$status="OK";
		} else {
			print "\n#############################################################################################","\n";
			print $res->status_line, "\n";
			print "\n#############################################################################################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return ($status, $fiche_xml);
}

############## fonction API PUT pour mettre à jour la fiche lecteur dans Alma 
sub put_user{
	my ($uid,$fiche_xml)= @_;
	#my $fiche_a_importer="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" . $fiche_xml ;
	my $fiche_a_importer="<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" . $fiche_xml ;
	# supprimer <external_id>SIS</external_id> dans la fiche XML
	$fiche_a_importer=~s/<external_id>SIS<\/external_id>\n//;
	print "\n#############################################################################################","\n";
	print "MISE A JOUR du lecteur $uid\n";
	print "\n#############################################################################################","\n";
	print "CONTENU DE LA FICHE :\n";
	print "\n######################## FICHE XML #####################################################################","\n";
	print $fiche_a_importer,"\n";
	print "\n######################## FICHE XML #####################################################################","\n";
	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/".$uid."?user_id_type=all_unique&send_pin_number_letter=false&apikey=".$hash_conf{api_cle};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(PUT => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user}, $hash_conf{api_cle}); # login  + mot de passe => user_api + cle api
	$req->content($fiche_a_importer);
	# Passer requete au user agent et récuperer la response
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /NOK/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n#############################################################################################","\n";
			#print $res->content;
			print "Mise à jour OK pour $uid";
			print "\n#############################################################################################","\n";
			$status="OK";
		} else {
			print "\n#############################################################################################","\n";
			print $res->status_line, "\n";
			print $res->content,"\n";
			print "\n#############################################################################################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
		
	return $status;
}

############## fonction API POST pour créer la fiche lecteur dans Alma 
sub post_user{
		my ($uid,$fiche_xml)= @_;
	#my $fiche_a_importer="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" . $fiche_xml ;
	my $fiche_a_importer="<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" . $fiche_xml ;
	# ajouter <external_id>SIS</external_id> dans la fiche XML
	$fiche_a_importer=~s/<\/user>/<external_id>SIS<\/external_id>\n<\/user>\n/;
	print "FONCTION POST : fiche à importer:\n-------------\n".$fiche_a_importer,"\n-------------\n---------------";
	print "\n#############################################################################################","\n";
	print "AJOUT du lecteur $uid\n";
	print "\n#############################################################################################","\n";
	print "CONTENU DE LA FICHE :\n";
	print "\n######################## FICHE XML #####################################################################","\n";
	print $fiche_a_importer,"\n";
	print "\n######################## FICHE XML #####################################################################","\n";
	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users?social_authentication=false&send_pin_number_letter=false&apikey=".$hash_conf{api_cle};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(POST => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user}, $hash_conf{api_cle}); # login  + mot de passe => user_api + cle api
	$req->content($fiche_a_importer);
	# Passer requete au user agent et récuperer la response
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /NOK/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n#############################################################################################","\n";
			#print $res->content;
			print "Ajout OK pour $uid";
			print "\n#############################################################################################","\n";
			$status="OK";
		} else {
			print "\n#############################################################################################","\n";
			print $res->status_line, "\n";
			
			print "\n#############################################################################################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
		
	return $status;
}



############################
sub date_claire { # Passage d'une date et retour d'une date en clair exemple : Lundi 22-09-2008 � 15h00
my @cejour = localtime($_[0]);
my $cejour_jour= $cejour[3];
my $cejour_mois= $cejour[4]+1;
my $cejour_annee= $cejour[5]+1900;
my $cejour_heure= $cejour[2];
my $cejour_minute= $cejour[1];
my $cejour_libelle=("Dimanche","Lundi", "Mardi", "Mercredi" ,"Jeudi", "Vendredi", "Samedi")[$cejour[6]];
#my $texte_date_claire= "$cejour_libelle $cejour_jour-$cejour_mois-$cejour_annee " . chr(133) . " $cejour_heure"."H".(length($cejour_minute)==1?"0" . $cejour_minute:$cejour_minute);
my $texte_date_claire= "$cejour_libelle ".(length($cejour_jour)==1?"0" . $cejour_jour:$cejour_jour)."-".(length($cejour_mois)==1?"0" . $cejour_mois:$cejour_mois)."-$cejour_annee -" . " $cejour_heure"."H".(length($cejour_minute)==1?"0" . $cejour_minute:$cejour_minute);
return  $texte_date_claire;
}
############################
sub date_simple { # Passage d'une date et retour d'une date en clair 
my @cejour = localtime($_[0]);
my $cejour_jour= $cejour[3];
my $cejour_mois= $cejour[4]+1;
my $cejour_annee= $cejour[5]+1900;
my $cejour_heure= $cejour[2];
my $cejour_minute= $cejour[1];
my $cejour_libelle=("Dimanche","Lundi", "Mardi", "Mercredi" ,"Jeudi", "Vendredi", "Samedi")[$cejour[6]];
my $texte_date_claire= (length($cejour_jour)==1?"0" . $cejour_jour:$cejour_jour) . "\/" . (length($cejour_mois)==1?"0" . $cejour_mois:$cejour_mois) . "\/" . $cejour_annee;
return  $texte_date_claire;
}
