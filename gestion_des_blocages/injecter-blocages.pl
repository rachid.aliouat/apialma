#!/usr/bin/perl -w
##################
# Rachid Aliouat
# Le 28/05/2019
# -Lire un fichier des blocages  : user_id(primary_id ou cb), type blocage(lié à un prêt ou messages), cb exemplaire, date fin
# -Creer le blocage
#######################
use strict;
use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use MIME::Lite;			# bibliothèque pour envoye un mail (rapport)

######## variables
my $methode="";
my $nom_fic="";
my @tab_blocages=();
my $uid="";
my $i;
my $twig=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
);
my $fiche_lecteur="";
my $reponse="";
my $blocage="";
my %hash_uid_inconnu=();
my $c="";
my $v=0;
##### LIRE LE FICHIER DE CONF
my %hash_conf=();
open (CONF, "config.conf") or die "Ouverture fichier de configuration config.conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
my @tab_inter = <CONF>; 
close(CONF);
$i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	print $tab_inter[$i];
	my @ligne=split('=',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf

}
print "-" x 20, "\n";

$methode=$ARGV[0];
$nom_fic=$ARGV[1];

#print $methode ,"\n";
#print $nom_fic ,"\n";

if ($methode!~ /ajout|remplace/) {
	print "Erreur dans le passage de paramétre\n";
	print "perl injecter-blocages ajout fichier.csv\nou \nperl injecter-blocages remplace fichier.csv\n";
	exit(0);
}




#### fichier en paramétre csv

open (FIC, $nom_fic) or die "Ouverture fichier des blocages impossible \"$nom_fic\" , cause : $! \n";
@tab_inter = <FIC>; 
close(FIC);
$i=0;
for ($i=1;$i<=$#tab_inter;$i++) {
	print $tab_inter[$i];
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne
	$tab_blocages[$i][0]= epure($ligne[0]); # num lecteur
	$tab_blocages[$i][1]= epure($ligne[1]); # type de blocage
	$tab_blocages[$i][2]= epure($ligne[2]); # message
	$tab_blocages[$i][3]= epure($ligne[3]); # date de fin
	

}
print "-" x 20, "\n";
###### traitements des blocages

for ($i=1;$i<=$#tab_blocages;$i++) {
	$uid=$tab_blocages[$i][0];
	# ################### QUESTIONER API GET POUR recuperer la fiche
   ($reponse,$fiche_lecteur)=get_user($uid); # appel de la fonction qui va questionner le webservice GET pour savoir si la personne existe déjà
   if ($reponse =~ /OK/) { # la personne existe déjà => on peut faire une mise à jour avec un PUT
	   #$twig->parse($fiche_lecteur); # ici tout est dans la structure twig
	   #print $twig->sprint , "\n";
	   print "Ajout blocage pour lecteur $uid\n";
	   ######### ajout d'un bloc blocage
	   $blocage="      <user_block segment_type=\"Internal\">"."\n".
        "<block_type desc=\"General\">GENERAL<\/block_type>"."\n".
        "<block_description >".$tab_blocages[$i][1]."<\/block_description>"."\n".
        "<block_status>ACTIVE<\/block_status>"."\n".
        "<block_note>".$tab_blocages[$i][2]."<\/block_note>"."\n".
        "<expiry_date>".$tab_blocages[$i][3]."<\/expiry_date>"."\n".
      "<\/user_block>"."\n";
      print "Ajout blocage =>\n";
      print $blocage,"\n";
      if ($methode=~ /ajout/) { ### On est en mode ajout

		if ($fiche_lecteur=~/<user_blocks>/) { ## il y a déjà des blocages il faut ajouter ce block
			$blocage.="<\/user_blocks>\n"; # on ajoute une balise de fin de blocks
			$fiche_lecteur=~s/<\/user_blocks>/$blocage/; ### remplacer cette balise par le blocages 
			 
			}else { #il n'y a pas de blocage , on peut ajouter un block user_blocks à la fin
				$fiche_lecteur=~ s/<\/user>/<user_blocks>/;
				$fiche_lecteur.= $blocage."\n"."<\/user_blocks>\n<\/user>\n";
			}
		} else { ### On est en mode remplace
			$fiche_lecteur=~s/<user_blocks>.*<\/user_blocks>//; ### on supprime le block complet s'il existe
		
		 #il n'y a plus de blocage , on peut ajouter un block user_blocks à la fin
			$fiche_lecteur=~ s/<\/user>/<user_blocks>/;
			$fiche_lecteur.= $blocage."\n"."<\/user_blocks>\n<\/user>\n";
		}
	  
  
	  
	  #print $fiche_lecteur;
	  $twig->parse($fiche_lecteur); # ici tout est dans la structure twig
	  print $twig->sprint , "\n";
	  #### re-injecter la fiche
	  $reponse=put_user($uid,$fiche_lecteur); # appel de la fonction qui va mettre à jour la fiche lecteur via le webservice PUT 
	  if ($reponse =~ /OK/) { # Mise à jour OK
			print "MISE A JOUR $uid OK \n";
			
			
		} else {
			print "ECHEC DE LA MISE A JOUR pour $uid \n";
			
			
		}

   } else {
	   print "################\n###\nAPI GET ECHEC : $uid USAGER INCONNU###\n################\n";
	   $hash_uid_inconnu{$uid}++;
	   
   }
	
}
# fichier rapport
open (RAP, ">rapport.txt") or die "Ouverture fichier des rapport impossible , cause : $! \n";

print RAP "Liste des uid inconnus pour lesquels des blocages n'ont pas été injectes(uid,nombre de blocage):\n";
print "Liste des uid inconnus pour lesquels des blocages n'ont pas été injectes(uid,nombre de blocage):\n";
while (($c,$v) = each(%hash_uid_inconnu)) {
	print RAP "$c,$v\n";
	print "$c,$v\n";
}

close(RAP);


############################
########## Fonctions
############################

########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	
	return $res;
}

############## fonction API GET pour questionner Alma : savoir si le lecteur existe déjà
sub get_user{
	my ($uid)=@_;  # on récupére l'uid en paramétre
	my $block="";
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/".$uid."?user_id_type=all_unique&view=full&expand=none&apikey=".$hash_conf{api_cle};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user}, $hash_conf{api_cle}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la response
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /NOK/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n#############################################################################################","\n";
			#print $res->content;
			$block=$res->content;
			print "Existence OK pour $uid";
			print "\n#############################################################################################","\n";
			$status="OK";
		} else {
			print "\n#############################################################################################","\n";
			print $res->status_line, "\n";
			print "\n#############################################################################################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return ($status,$block);
}

############## fonction API PUT pour mettre à jour la fiche lecteur dans Alma 
sub put_user{
	my ($uid,$fiche_xml)= @_;
	#my $fiche_a_importer="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" . $fiche_xml ;
	my $fiche_a_importer=$fiche_xml ;
	print "\n#############################################################################################","\n";
	print "MISE A JOUR du lecteur $uid\n";
	print "\n#############################################################################################","\n";
	print "CONTENU DE LA FICHE :\n";
	print "\n######################## FICHE XML #####################################################################","\n";
	print $fiche_a_importer,"\n";
	print "\n######################## FICHE XML #####################################################################","\n";
	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/".$uid."?user_id_type=all_unique&send_pin_number_letter=false&apikey=".$hash_conf{api_cle};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(PUT => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user}, $hash_conf{api_cle}); # login  + mot de passe => user_api + cle api
	$req->content($fiche_a_importer);
	# Passer requete au user agent et récuperer la response
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /NOK/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n#############################################################################################","\n";
			#print $res->content;
			print "Mise à jour OK pour $uid";
			print "\n#############################################################################################","\n";
			$status="OK";
		} else {
			print "\n#############################################################################################","\n";
			print $res->status_line, "\n";
			print $res->content,"\n";
			print "\n#############################################################################################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
		
	return $status;
}





############################
sub date_claire { # Passage d'une date et retour d'une date en clair exemple : Lundi 22-09-2008 � 15h00
my @cejour = localtime($_[0]);
my $cejour_jour= $cejour[3];
my $cejour_mois= $cejour[4]+1;
my $cejour_annee= $cejour[5]+1900;
my $cejour_heure= $cejour[2];
my $cejour_minute= $cejour[1];
my $cejour_libelle=("Dimanche","Lundi", "Mardi", "Mercredi" ,"Jeudi", "Vendredi", "Samedi")[$cejour[6]];
#my $texte_date_claire= "$cejour_libelle $cejour_jour-$cejour_mois-$cejour_annee " . chr(133) . " $cejour_heure"."H".(length($cejour_minute)==1?"0" . $cejour_minute:$cejour_minute);
my $texte_date_claire= "$cejour_libelle ".(length($cejour_jour)==1?"0" . $cejour_jour:$cejour_jour)."-".(length($cejour_mois)==1?"0" . $cejour_mois:$cejour_mois)."-$cejour_annee -" . " $cejour_heure"."H".(length($cejour_minute)==1?"0" . $cejour_minute:$cejour_minute);
return  $texte_date_claire;
}
############################
sub date_simple { # Passage d'une date et retour d'une date en clair 
my @cejour = localtime($_[0]);
my $cejour_jour= $cejour[3];
my $cejour_mois= $cejour[4]+1;
my $cejour_annee= $cejour[5]+1900;
my $cejour_heure= $cejour[2];
my $cejour_minute= $cejour[1];
my $cejour_libelle=("Dimanche","Lundi", "Mardi", "Mercredi" ,"Jeudi", "Vendredi", "Samedi")[$cejour[6]];
my $texte_date_claire= (length($cejour_jour)==1?"0" . $cejour_jour:$cejour_jour) . "\/" . (length($cejour_mois)==1?"0" . $cejour_mois:$cejour_mois) . "\/" . $cejour_annee;
return  $texte_date_claire;
}
