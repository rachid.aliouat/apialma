#!/usr/bin/perl
##############################
# auteur : Rachid
# date : 01-12-2022
# objet : mettre à jour des données holding dans ALma via API. 
# Source de données : un fichier CSV tabulé UTF8 : mms_id holding_id / item_pid / champ et sous champs à  mettre a jour
#colonnage :
# mms_id	holding_id	852_z
# ==> exemple pour remplacer le contenu de la zone 852 $z de la holding
# ==> si la zone n'existe pas, elle est ajoutée
##############################

use strict;
#use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use URI::Encode qw(uri_encode uri_decode);; #encoder decoder les URL   - requiert la bibliotheque liburi-encode-perl

my $nom_fic_csv="";
my %hash_conf=();
my $nom_fic_conf="config.conf";
my @tab_inter = ();
my $i=0;
my $chaine_a_capturer="";
my $chaine_a_remplacer="";
my $chaine_de_remplacement="";
my $retour="";
my %hash_holding_mms_id=(); # holding_id => mms_id de la notice

my $champ="";
my $sous_champ="";

if (!(exists $ARGV[0])) {print "Manque le fichier des holding en parametre.\n";exit(0)};
$nom_fic_csv=$ARGV[0];
##### LIRE LE FICHIER DE CONF

open (CONF, $nom_fic_conf) or die "Ouverture fichier de configuration $nom_fic_conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
@tab_inter = <CONF>; 
close(CONF);
$i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	#print $tab_inter[$i];
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf
	

}
print "-" x 20, "\n";


################ TRAITEMENT DU CSV LIGNE A LIGNE
open (CSV,'<:encoding(UTF-8)',$nom_fic_csv) or die "Ouverture fichier de CSV $nom_fic_csv impossible , cause : $! \n";
print "lecture des holding:\n";
print "-" x 20, "\n";
@tab_inter = <CSV>; 
close(CSV);

print "Nombre de ligne lues : $#tab_inter\n";

### récupération du nom de la sous zone sur laquelle travailler
my @ligne=split('\t',$tab_inter[0]); # decoupage ligne
my $champ_sous_champ=epure($ligne [2]);

if ($champ_sous_champ=~ /(.*)_(.*)/) {
	$champ=$1;
	$sous_champ=$2;
} 	else  {
		print "Mauvaise définition du champ et sous champ $champ_sous_champ\n";
		exit(0);
	}

if (length($champ)>0 & length($sous_champ)>0) {
	print "Mise à jour du champ $champ sous champ $sous_champ\n";
	}
	else  {
		print "Mauvaise définition du champ et sous champ $champ_sous_champ\n";
		exit(0);
	}
######## Ouvrir le rapport d'errreur des lignes non traité : par exemple PPN inconnu
open (ERR, ">erreurs.txt") or die "Ouverture fichier de erreurs.txt impossible , cause : $! \n";
print ERR "Motif_rejet\t$tab_inter[0]"; # on récupére l'entête , on ajoute une colone avant pour renseigner ensuite le motif du rejet
close (ERR);

####################

########### Appel de l'API analytics 
my $block_xml="";
my $block_xml_apres="";
my $conv = XML::Twig::encode_convert( 'utf8');
my $twig_rapport=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
        output_filter => $conv
);
######## Traitement des données
$i=1; # on passe la ligne d'entete => on commence à 1
for ($i;$i<=$#tab_inter;$i++) {
	@ligne=split('\t',$tab_inter[$i]); # decoupage ligne
	my $mms_id=epure($ligne[0]); # récupération du mmsid
	my $holding_id=epure($ligne[1]);
	my $valeur_sous_zone=epure($ligne[2]);
	if (length($mms_id) >10) { # ne pas traiter les lignes vides
		
		print $mms_id ."-->". $holding_id ."\n";
		### Recupération de la notice holding
		print "Requete API sur le MMS_ID - HOLDING_ID \n";
		$block_xml=get_holding_from_mms_id($mms_id,$holding_id); # Appel de l'API sur toutes les bibs pour recuperer les mms_id
		print "\n#############\n$block_xml\n###########\n";
		if ($block_xml!~/^MAX$/) # Ok pour la récupération de la holding
		{
			$twig_rapport->parse($block_xml); # ici tout est dans la structure twig
			print $twig_rapport->sprint; # affichage du contenu
			#print $block_xml."\n--------\n";
			my $root= $twig_rapport->get_xpath('//holding/record/',0);
		
			my @les_datafield= $root->children('datafield');  ### on récuperer une table de hash de tous les "enfants"
		
			foreach my $un_datafield (@les_datafield)      # liste chaque datafield de la liste
			{ 
				
				my $tag = $un_datafield->{'att'}->{'tag'};
				if ($tag==$champ) { # on est dans le bon datafield
					print "\n############\n";
					print $un_datafield->sprint;
					print "\n############\n";
					my @les_subfield= $un_datafield->children('subfield');  ### on récuperer une table de hash de tous les "enfants"
					foreach my $un_subfield (@les_subfield)      # liste chaque datafield de la liste
						{
						my $code = $un_subfield->{'att'}->{'code'};
						#print $code,"\n";
						if ($code=~ /^$sous_champ$/) { # on est dans le bon subfield
							print $un_subfield->sprint;
							$chaine_a_remplacer=$un_subfield->text();
							print "\nAncienne valeur du $champ \$$sous_champ = [$chaine_a_remplacer]\n"; 
							print "\nNouvelle valeur du $champ \$$sous_champ = [$valeur_sous_zone]\n"; 
							$un_subfield->set_text($valeur_sous_zone);
						}
					}
				}
			}

		# Apres modif eventuellement
			print "\n############# Apres Modif ############\n";
			print $twig_rapport->sprint; # affichage du contenu
			### Mettre à jour la holding
			$block_xml_apres=$twig_rapport->sprint;
			$retour=put_maj_holding($mms_id,$holding_id,$block_xml_apres);
			if ($retour=~/false/) # echec de la mise à jour 
				{
				ecrire_log("echec maj holding",$mms_id."---".$holding_id."\n");
				}
			#print "\n###########\n".$block_xml."\n";
			$twig_rapport->purge; #liberer la mémoire
		}
	}
	
}

########################################
######### FONCTIONS

########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	#$res=~ s/\"//g;
	
	return $res;
}

#### Fonction erreur : ecrire une ligne dans le rapport d'erreur
sub ecrire_log() {
	my ($motif,$ligne)=@_;

open (ERR, ">>erreurs.txt") or die "Ouverture fichier de erreurs.txt impossible , cause : $! \n";
print ERR "$motif\t$ligne";
close (ERR);

}


############## fonction API GET pour questionner Alma : Recuperer la HOLDING sur la base du MMS_id / holding
sub get_holding_from_mms_id{
	my ($mms_id,$holding_id)=@_;  # on récupére le mms_id en paramétre
	
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id/holdings/$holding_id?apikey=".$hash_conf{api_cle_create_hold_item};

	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=1; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n##############################","\n";
			print $res->content;
			print "\nRetour appel API OK pour $mms_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print $res->content,"\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
			}
		}	
	}
	
	return $status;
}


############## fonction API PUT pour mettre à jour une holding

sub put_maj_holding {
	my($mms_id,$holding_id,$block_xml)=@_;
	
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	#exemple : 	https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/990000667070205601/holdings/22184992810005601/items/23184992800005601?apikey=#######
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id/holdings/$holding_id?apikey=".$hash_conf{api_cle_create_hold_item};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(PUT => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	$req->content($block_xml);
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=1; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			print $res->content;
			$block_xml=$res->content; # block retour suite appel api post : on est censé avoir un block item XML 
			print "\nRetour appel API OK pour mms_id = $mms_id et holding_id = $holding_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print $res->content,"\n";

			print "############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
		}
	}
	if ($status !~ /MAX|NOK/) {
	#######Analyse du block XML retourné et alimentation des tables de hash globales
		return "true"; # maj de holding ok
	} else {
		return "false"; # echec de la maj de la holding
	}
	
}
