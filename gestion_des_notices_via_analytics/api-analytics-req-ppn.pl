#!/usr/bin/perl -w
##################
# Rachid Aliouat
# Le 15/06/2020
# Test API analytics avec un rapport statique sur les nouveautes
# 16/11/2020 : ajout de 3 colonnes supplémentaire dans le rapport analytics :  Location Name / Internal Note 3 / Reporting Code Secondary
# dans le prompt remplacer PPNS par autant de bloc <sawx:expr xsi:type="xsd:string">041019342</sawx:expr> contenant un PPN (attention max octet dans une URL et max PPn dans une requete = 9999)
#################
use strict;
use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use URI::Encode qw(uri_encode uri_decode);; #encoder decoder les URL   - requiert la bibliotheque liburi-encode-perl

my $token="";
my @tab_nouveautes=();
my @tab_ppn=();
my $indice=0;
my $j=0;
my $nom_fic_conf="";
my $nom_fic_ppn="";
my %hash_entete=(); # pour retrouver plus facilement une valeur
my @tab_entete=(); # pour parcourir dans l'ordre les colonnes
my $ligne_entete_csv="";
my $nbr_colonne=0;
my $entete_csv_deja_ecrit=0;
my $max_ppn_url = 100; # nombre max de bloc ppn dans l'url : attention une url est limitée en nombre d'octect : pas plus de 100 PPN car c'est encapsulé dans des balises XML verbeuses...
my $compteur_ppn=0;

$nom_fic_conf=$ARGV[0];
if (!(exists $ARGV[1])) {print "Manque le fichier de ppn en parametre.\n";exit(0)};
$nom_fic_ppn=$ARGV[1];
##### LIRE LE FICHIER DE CONF
my %hash_conf=();
open (CONF, $nom_fic_conf) or die "Ouverture fichier de configuration $nom_fic_conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
my @tab_inter = <CONF>; 
close(CONF);
my $i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	print $tab_inter[$i];
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf

}
print "-" x 20, "\n";
#################### Mise en place du filtre sur les PPN
##### LIRE LE FICHIER DES PPN
print "traitement du fichier $nom_fic_ppn\n";

####
#####
open (FIC, $nom_fic_ppn) or die "Ouverture fichier des demandes impossible , cause : $! \n";
print "lecture du fichier des PPN:\n";
print "-" x 20, "\n";
@tab_inter = <FIC>; 
close(FIC);
$j=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	$tab_inter[$i]=~ s/\n//g; # on enléve le retour chariot
	$tab_inter[$i]=~ s/\(PPN\)//g; # s'il y a un prefixe (PPN) , on l'enleve
	if (length($tab_inter[$i]) > 5) { # si la donnée est trop courte, on ne la prend pas
		$tab_ppn[$j]=$tab_inter[$i];
		print $tab_ppn[$j],"\n";
		$j++;
	}
}

print "Il y a ".($#tab_ppn+1)." PPN a traiter.","\n";

########### Appel de l'API analytics 

my $twig_rapport=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
);

while ($compteur_ppn<=$#tab_ppn) { #On parcourt la table des PPN et on va faire des requete par bloc de X PPN
	my $prompt_filtre = $hash_conf{prompt};
	my $ppn="";
	# alimenter la liste de ppn à transmettre lors de l'appel API
	my $cpt_max_ppn_url=0;
	while (($cpt_max_ppn_url<=$max_ppn_url) && ($compteur_ppn<=$#tab_ppn)) {
		$ppn .= "<sawx:expr xsi:type=\"xsd:string\">".$tab_ppn[$compteur_ppn]."</sawx:expr>";
		#print "REQUETE PPN: [".$ppn."]\n";
		$compteur_ppn++;
		$cpt_max_ppn_url++;
		
	}
	print "REQUETE PPN: [".$ppn."]\n";
	$prompt_filtre=~ s/PPNS/$ppn/g; # on y met les PPN
	print $prompt_filtre,"\n";

	my $block_xml=get_alma_analytics_by_name($hash_conf{rapport_prompt},$prompt_filtre,""); # Appel de l'API analytics sur le nom d'un rapport dans le fichier de conf
	$block_xml =~/<ResumptionToken>(.*)<\/ResumptionToken>/;
	$token =$1;
	$twig_rapport->parse($block_xml); # ici tout est dans la structure twig
	print $twig_rapport->sprint; # affichage du contenu

	if ($entete_csv_deja_ecrit==0) {
		aliment_tab_indices ($twig_rapport);
		#exit();
	}

	aliment_tab_nouveautes($twig_rapport);


	# si <IsFinished>false</IsFinished> existe. Alors c'est que l'on a pas tout la liste. Il faut rappeler la requete Analytics en mettant le token à la place du chemin
	while ($block_xml =~/<IsFinished>false<\/IsFinished>/) {

		$block_xml=get_alma_analytics_by_name("","",$token); # Appel de l'API analytics sur le token
		$twig_rapport->parse($block_xml); # ici tout est dans la structure twig
		#print $twig_rapport->sprint; # affichage du contenu
		aliment_tab_nouveautes($twig_rapport);

	}
	print "\n";

	#### Ecriture du fichier CSV en mode Ajout

	open (FIC, ">>".$hash_conf{code_bib}."-liste-exemplaire.csv") or die "Ouverture fichier de liste exemplaire CSV impossible , cause : $! \n";
	binmode(FIC, ":utf8");

	if ($entete_csv_deja_ecrit==0) {# on écrit l'entete CSV une seule fois : donc on le mémorise dans entete_deja_ecrit qui passe à 1
		print $ligne_entete_csv,"\n";
		print FIC $ligne_entete_csv,"\n";
		$entete_csv_deja_ecrit=1;
		}
	for ($i=0;$i<=$#tab_nouveautes;$i++) #on écrit toutes les lignes en mémoires
		{
		for ($j=0;$j<=$nbr_colonne;$j++) 
			{
			print $tab_nouveautes[$i][$j],"\t";
			print FIC $tab_nouveautes[$i][$j],"\t";
			}
	print "\n";
	print FIC "\n"; 
	}
	close (FIC);
	# on purge le tableau des lignes pour l'appel api suivant
	@tab_nouveautes=();
	#$compteur_ppn++;
	$indice=0;

}

################ Fonctions
######## Fonction alimente tableau indice de colonne pour chaque libellé de colonne car l'ordre des colonnes peut changer si on modifie le rapport analytics

sub aliment_tab_indices{
		my ($twig_rapport)=@_;
		my $z=0;
		
	my $root= $twig_rapport->get_xpath('//rowset/xsd:schema/xsd:complexType/xsd:sequence',0); 
	#my $entete=defined $root->first_child('xsd:sequence')?$root->first_child('xsd:sequence')->text():"";
	#print $root->sprint; # affichage du contenu
	print "\n";
	
	my @les_colonnes= $root->children('xsd:element');  ### on récuperer une table de hash de tous les "enfants" du root 

	foreach my $une_colonne (@les_colonnes)      # liste chaque nouveaute de la liste des nouveautes
		{
			#print $une_colonne->sprint;
			#print "\n";
			my $numero_col= $une_colonne->{'att'}->{'name'}; # get the attribute
			my $nom_col= $une_colonne->{'att'}->{'saw-sql:columnHeading'}; # get the attribute
			#print "\nNom de colonne : $nom_col\n";
			$hash_entete{$nom_col} = $numero_col;
			print $hash_entete{$nom_col} . "-> ".$nom_col."\n";
			$ligne_entete_csv.=$nom_col."\t";
			$tab_entete[$z]=$nom_col;
			$z++;
		}
		$nbr_colonne=$z-1;

}


######## Fonction alimente tableau des nouveautes
sub aliment_tab_nouveautes{
	my ($twig_rapport)=@_;
	#print $twig_rapport->sprint;
	#print "----fonction aliment_tab_nouveautes----\n";
	#my $root= $twig_rapport->root->first_child('ResultXml')->first_child('rowset');  
	my $root= $twig_rapport->get_xpath('//rowset',0); 
	my @les_nouveautes= $root->children('Row');  ### on récuperer une table de hash de tous les "enfants" du root = les entrées <Row> de la racine <rowset>
	#print "#########################\n";
	#print "####".$root->first_child('Row')->first_child('Column12')->text()."####\n";
	foreach my $une_nouveaute (@les_nouveautes)      # liste chaque nouveaute de la liste des nouveautes
		{ 
				#print "############\n";
				
				for ($i=0;$i<=$#tab_entete;$i++) {
					$tab_nouveautes[$indice][$i]=defined $une_nouveaute->first_child($hash_entete{$tab_entete[$i]})?$une_nouveaute->first_child($hash_entete{$tab_entete[$i]})->text():"";
				}
			$indice++;
		}
	
}

############## fonction API GET pour questionner Alma API Analytics: Recuperer les données sur la base du nom du rapport
sub get_alma_analytics_by_name{
	my ($nom_rapport,$prompt_filtre,$token)=@_;  # on récupére le nom du rapport ou le token en paramétre
	my $uri     = URI::Encode->new( { encode_reserved => 0 } );
	my $nom_rapport_encoded = $uri->encode($nom_rapport);
	my $prompt_encoded=$uri->encode($prompt_filtre);

	#my $nom_rapport_encoded = uri_encoded($nom_rapport);
	print "$nom_rapport\n$nom_rapport_encoded\n";
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="";
	if (length($nom_rapport)>0) {
		print "-----appel initiale----\n";
		#$chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?path=$nom_rapport_encoded&limit=25&col_names=true&apikey=".$hash_conf{api_cle_analytics};
		$chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?path=$nom_rapport_encoded&limit=25&col_names=true&apikey=".$hash_conf{api_cle_analytics}."&filter=".$prompt_encoded;
	}
	else { # sinon, ce n'est pas l'appel initial, mais un appel qui demande la suite de la liste via le token
		print "-----appels secondaires suite de la liste----\n";
		$chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?token=$token&limit=25&col_names=true&apikey=".$hash_conf{api_cle_analytics};

	}
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_analytics}, $hash_conf{api_cle_analytics}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			print $res->content;
			print "Existence notice OK pour $nom_rapport\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print $res->content;
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}


########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	#$res=~ s/\"//g;
	
	return $res;
}

