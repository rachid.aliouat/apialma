#!/usr/bin/perl -w
##################
# Rachid Aliouat
# Le 18/04/2019
# -script pour tester API item : passage du code barre en parametre
#######################
use strict;
use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use MIME::Lite;			# bibliothèque pour envoye un mail (rapport)
##### LIRE LE FICHIER DE CONF
my %hash_conf=();
open (CONF, "config.conf") or die "Ouverture fichier de configuration config.conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
my @tab_inter = <CONF>; 
close(CONF);
my $i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	print $tab_inter[$i];
	my @ligne=split('=',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf

}
print "-" x 20, "\n";

my $twig=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
);


my $code_barre=$ARGV[0];
my $block_xml=get_item($code_barre);
#print $block_xml;
if ($block_xml =~ /item/) { ## item ok 
	$twig->parse($block_xml); # ici tout est dans la structure twig
	print $twig->sprint; # affichage du contenu
	
	my $root= $twig->root->first_child('item_data');  
	my $item_pid = $root->first_child('pid')->text;
	my $item_cb = $root->first_child('barcode')->text;
	my $item_type = $root->first_child('physical_material_type')->text;
	my $item_policy = $root->first_child('policy')->text;
	my $item_process_type = $root->first_child('process_type')->text;
	my $item_library = $root->first_child('library')->text;
	my $item_location = $root->first_child('location')->text;
	my $item_titre_auteur = $root->first_child('title_abcnph')->text;
	
	print "\npid : $item_pid\n";
	print "code barre : $item_cb\n";
	print "type materiel : $item_type\n";
	print "droit de pret : $item_policy\n";
	print "situation actuelle : $item_process_type\n";
	print "bibliotheque : $item_library\n";
	print "localisation : $item_location\n";
	print "titre / auteur : $item_titre_auteur\n";
	
	my $root= $twig->root->first_child('holding_data');  
	my $holding_call = $root->first_child('call_number')->text;
	print "cote : $holding_call\n";
}
############################
########## Fonctions
############################

############## fonction API GET pour questionner Alma : Recuperer toutes les information d'un exemplaires
sub get_item{
	my ($cb)=@_;  # on récupére l'uid en paramétre
	
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/items?view=label&item_barcode=$cb&apikey=".$hash_conf{api_cle};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user}, $hash_conf{api_cle}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n#############################################################################################","\n";
			print $res->content;
			print "\nExistence exemplaire OK pour $cb";
			print "\n#############################################################################################","\n";
			$status=$res->content;
		} else {
			print "\n#############################################################################################","\n";
			print $res->status_line, "\n";
			print "\n#############################################################################################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}





########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	
	return $res;
}



############################
sub date_claire { # Passage d'une date et retour d'une date en clair exemple : Lundi 22-09-2008 � 15h00
my @cejour = localtime($_[0]);
my $cejour_jour= $cejour[3];
my $cejour_mois= $cejour[4]+1;
my $cejour_annee= $cejour[5]+1900;
my $cejour_heure= $cejour[2];
my $cejour_minute= $cejour[1];
my $cejour_libelle=("Dimanche","Lundi", "Mardi", "Mercredi" ,"Jeudi", "Vendredi", "Samedi")[$cejour[6]];
#my $texte_date_claire= "$cejour_libelle $cejour_jour-$cejour_mois-$cejour_annee " . chr(133) . " $cejour_heure"."H".(length($cejour_minute)==1?"0" . $cejour_minute:$cejour_minute);
my $texte_date_claire= "$cejour_libelle ".(length($cejour_jour)==1?"0" . $cejour_jour:$cejour_jour)."-".(length($cejour_mois)==1?"0" . $cejour_mois:$cejour_mois)."-$cejour_annee -" . " $cejour_heure"."H".(length($cejour_minute)==1?"0" . $cejour_minute:$cejour_minute);
return  $texte_date_claire;
}
############################
sub date_simple { # Passage d'une date et retour d'une date en clair 
my @cejour = localtime($_[0]);
my $cejour_jour= $cejour[3];
my $cejour_mois= $cejour[4]+1;
my $cejour_annee= $cejour[5]+1900;
my $cejour_heure= $cejour[2];
my $cejour_minute= $cejour[1];
my $cejour_libelle=("Dimanche","Lundi", "Mardi", "Mercredi" ,"Jeudi", "Vendredi", "Samedi")[$cejour[6]];
my $texte_date_claire= (length($cejour_jour)==1?"0" . $cejour_jour:$cejour_jour) . "\/" . (length($cejour_mois)==1?"0" . $cejour_mois:$cejour_mois) . "\/" . $cejour_annee;
return  $texte_date_claire;
}


