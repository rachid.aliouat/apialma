#!/usr/bin/perl -w
##################
# Rachid Aliouat
# Le 04/01/2024
# convertisseur TSB (csv tabulé) vers xls
# param 1 = nom du fichier csv/tsb
# param 2 = nom du fichier xls
##############
use strict;
use warnings;
use utf8;
use Spreadsheet::WriteExcel;
use URI::Encode qw(uri_encode uri_decode);; #encoder decoder les URL   - requiert la bibliotheque liburi-encode-perl
use utf8;
use Encode;

my $nom_fic_csv="";
my $nom_fic_xls="";
my %hash_csv=();
my @ligne=();
my $nbr_ligne=0;

# tester nbr paramétre
if ($#ARGV<1) {
	print "Pas assez de paramétre : \n\tparam 1 = nom du fichier csv/tsb \n\tparam 2 = nom du fichier xls\n";
	print "Exemple : perl convtsbxls.pl liste.csv liste.xls\n";
	exit(0);
} else {
	$nom_fic_csv=$ARGV[0];
	$nom_fic_xls=$ARGV[1];
	print "convertir $nom_fic_csv en $nom_fic_xls ...\n====================================\n";
}

##############################
# Lire le fichier csv
open (CSV, '<:encoding(utf8)',$nom_fic_csv) or die "Ouverture fichier de configuration $nom_fic_csv impossible , cause : $! \n";
my @tab_inter = <CSV>; 
close(CSV);
my $i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	print $tab_inter[$i];
	#@ligne=split('\t',$tab_inter[$i]); # decoupage ligne
	$hash_csv{$i}=epure($tab_inter[$i]); # alimentation de la table de hash avec le csv
}

$nbr_ligne=$i;
print "====================================\n$nbr_ligne ligne(s)\n";
##############################
# Ecrire le fichier XLS
# Creer un fichier Excel
my $workbook  = Spreadsheet::WriteExcel->new( $nom_fic_xls );
my $worksheet = $workbook->add_worksheet();

my $col = 0;
my $row = 0;
$i=0;
my $j=0;

my $format = $workbook->add_format();
$format->set_bold();
$format->set_color('red');
$format->set_align('center');

### Ecrire la ligne d'entête
@ligne=split('\t',$hash_csv{0});
for ($i=0;$i<=$#ligne;$i++) {
	$worksheet->write_string( 0, $i, $ligne[$i] ,$format);
}

### Ecrire les valeurs dans chaque cellule
for ($i=1;$i<=($nbr_ligne-1);$i++) {
	@ligne=split('\t',$hash_csv{$i});
	for ($j=0;$j<=$#ligne;$j++) {
		$worksheet->write_string( $i, $j, $ligne[$j]); # $i = ligne / $j = colonne
	}
}


#### Fermeture du fichier XLS
$workbook->close() or die "Error closing file: $!";

##############################
#Fonctions
##############################
### nettoyer les fins de lignes
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	return $res;
}

