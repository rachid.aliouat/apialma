#!/usr/bin/perl -w
##################
# Rachid Aliouat
# Le 30/11/2020
# API analytics recuperation d'un rapport
# 14-12-2022 : ajout option horodatage du nom de fichier
# 24-01-2023 : ignorer la colonne 0
# 24-01-2023 : supprimer la derniere tabulation avant retour à la ligne dans le fichier final
#################
use strict;
use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use URI::Encode qw(uri_encode uri_decode);; #encoder decoder les URL   - requiert la bibliotheque liburi-encode-perl

my $token="";
my @tab_nouveautes=();
my @tab_ppn=();
my $indice=0;
my $j=0;
my $nom_fic_conf="";
my $nom_fic_csv="";
my %hash_entete=(); # pour retrouver plus facilement une valeur
my @tab_entete=(); # pour parcourir dans l'ordre les colonnes
my $ligne_entete_csv="";
my $nbr_colonne=0;
my $entete_csv_deja_ecrit=0;
#my $max_ppn_url = 100; # nombre max de bloc ppn dans l'url : attention une url est limitée en nombre d'octect : pas plus de 100 PPN car c'est encapsulé dans des balises XML verbeuses...
#my $compteur_ppn=0;

$nom_fic_conf=$ARGV[0];

##### LIRE LE FICHIER DE CONF
my %hash_conf=();
open (CONF, $nom_fic_conf) or die "Ouverture fichier de configuration $nom_fic_conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
my @tab_inter = <CONF>; 
close(CONF);
my $i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	print $tab_inter[$i];
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf

}
print "-" x 20, "\n";
##### si horodatage du nom de fichier est "yes"
if ($hash_conf{'horodatage'}=~ /yes/){
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
	#printf("%02d:%02d:%02d", $hour, $min, $sec);
	#my $date_jour=sprintf("%02d-%02d-%04d", $mday,  ($mon+1), (1900+$year));
	my $date_jour=sprintf("%04d-%02d-%02d", (1900+$year),($mon+1),$mday);
	print "Horodatage : $date_jour\n";
	$hash_conf{'fic_sortie'}=$date_jour."-".$hash_conf{'fic_sortie'};
}


########### Appel de l'API analytics 

my $twig_rapport=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
);


	my $block_xml=get_alma_analytics_by_name($hash_conf{rapport_prompt},$hash_conf{prompt},""); # Appel de l'API analytics sur le nom d'un rapport dans le fichier de conf
	$block_xml =~/<ResumptionToken>(.*)<\/ResumptionToken>/;
	$token =$1;
	$twig_rapport->parse($block_xml); # ici tout est dans la structure twig
	print $twig_rapport->sprint; # affichage du contenu

	if ($entete_csv_deja_ecrit==0) {
		aliment_tab_indices ($twig_rapport);
		#exit();
	}

	aliment_tab_nouveautes($twig_rapport);


	# si <IsFinished>false</IsFinished> existe. Alors c'est que l'on a pas tout la liste. Il faut rappeler la requete Analytics en mettant le token à la place du chemin
	while ($block_xml =~/<IsFinished>false<\/IsFinished>/) {

		$block_xml=get_alma_analytics_by_name("","",$token); # Appel de l'API analytics sur le token
		$twig_rapport->parse($block_xml); # ici tout est dans la structure twig
		#print $twig_rapport->sprint; # affichage du contenu
		aliment_tab_nouveautes($twig_rapport);

	}
	print "\n";

	#### Ecriture du fichier CSV en mode Ajout

	open (FIC, ">".$hash_conf{fic_sortie}) or die "Ouverture fichier de liste exemplaire CSV impossible , cause : $! \n";
	binmode(FIC, ":utf8");

	
	# dans le fichier ne pas écrire la colonne "0"
		print $ligne_entete_csv,"\n";
		$ligne_entete_csv=~ /^0\t(.*)/;
		$ligne_entete_csv=$1;
		print FIC $ligne_entete_csv,"\n";
	
	
	for ($i=0;$i<=$#tab_nouveautes;$i++) #on écrit toutes les lignes en mémoires
		{
		for ($j=1;$j<=$nbr_colonne;$j++) # on passe la colonne 0 toujours vide
			{
			print $tab_nouveautes[$i][$j],"\t";
			print FIC $tab_nouveautes[$i][$j];
			if ($j<$nbr_colonne) {print FIC "\t"} # on met une tabulation sauf après la derniere colonne
			}
	print "\n";
	print FIC "\n"; 
	}
	close (FIC);


################ Fonctions
######## Fonction alimente tableau indice de colonne pour chaque libellé de colonne car l'ordre des colonnes peut changer si on modifie le rapport analytics

sub aliment_tab_indices{
		my ($twig_rapport)=@_;
		my $z=0;
		
	my $root= $twig_rapport->get_xpath('//rowset/xsd:schema/xsd:complexType/xsd:sequence',0); 
	#my $entete=defined $root->first_child('xsd:sequence')?$root->first_child('xsd:sequence')->text():"";
	#print $root->sprint; # affichage du contenu
	print "\n";
	
	my @les_colonnes= $root->children('xsd:element');  ### on récuperer une table de hash de tous les "enfants" du root 

	foreach my $une_colonne (@les_colonnes)      # liste chaque nouveaute de la liste des nouveautes
		{
			#print $une_colonne->sprint;
			#print "\n";
			my $numero_col= $une_colonne->{'att'}->{'name'}; # get the attribute
			my $nom_col= $une_colonne->{'att'}->{'saw-sql:columnHeading'}; # get the attribute
			#print "\nNom de colonne : $nom_col\n";
			$hash_entete{$nom_col} = $numero_col;
			print $hash_entete{$nom_col} . "-> ".$nom_col."\n";
			$ligne_entete_csv.=$nom_col."\t";
			$tab_entete[$z]=$nom_col;
			$z++;
		}
		$nbr_colonne=$z-1;

}


######## Fonction alimente tableau des nouveautes
sub aliment_tab_nouveautes{
	my ($twig_rapport)=@_;
	#print $twig_rapport->sprint;
	#print "----fonction aliment_tab_nouveautes----\n";
	#my $root= $twig_rapport->root->first_child('ResultXml')->first_child('rowset');  
	my $root= $twig_rapport->get_xpath('//rowset',0); 
	my @les_nouveautes= $root->children('Row');  ### on récuperer une table de hash de tous les "enfants" du root = les entrées <Row> de la racine <rowset>
	#print "#########################\n";
	#print "####".$root->first_child('Row')->first_child('Column12')->text()."####\n";
	foreach my $une_nouveaute (@les_nouveautes)      # liste chaque nouveaute de la liste des nouveautes
		{ 
				#print "############\n";
				
				for ($i=0;$i<=$#tab_entete;$i++) {
					$tab_nouveautes[$indice][$i]=defined $une_nouveaute->first_child($hash_entete{$tab_entete[$i]})?$une_nouveaute->first_child($hash_entete{$tab_entete[$i]})->text():"";
				}
			$indice++;
		}
	
}

############## fonction API GET pour questionner Alma API Analytics: Recuperer les données sur la base du nom du rapport
sub get_alma_analytics_by_name{
	
	my ($nom_rapport,$prompt_filtre,$token)=@_;  # on récupére le nom du rapport ou le token en paramétre
	my $uri     = URI::Encode->new( { encode_reserved => 0 } );
	my $nom_rapport_encoded = $uri->encode($nom_rapport);
	my $prompt_encoded=$uri->encode($prompt_filtre);
	print "Appel fonction GET\n ----\n$nom_rapport\n$prompt_filtre\n$token\n";

	#my $nom_rapport_encoded = uri_encoded($nom_rapport);
	print "$nom_rapport\n$nom_rapport_encoded\n";
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="";
	if (length($nom_rapport)>0) {
		print "-----appel initiale----\n";
		#$chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?path=$nom_rapport_encoded&limit=25&col_names=true&apikey=".$hash_conf{api_cle_analytics};
		$chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?path=$nom_rapport_encoded&limit=25&col_names=true&apikey=".$hash_conf{api_cle_analytics}."&filter=".$prompt_encoded;
		print $chaine_rq,"\n";
	}
	else { # sinon, ce n'est pas l'appel initial, mais un appel qui demande la suite de la liste via le token
		print "-----appels secondaires suite de la liste----\n";
		$chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?token=$token&limit=25&col_names=true&apikey=".$hash_conf{api_cle_analytics};
		print $chaine_rq,"\n";

	}
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_analytics}, $hash_conf{api_cle_analytics}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			print $res->content;
			print "Existence notice OK pour $nom_rapport\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print $res->content;
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}


########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	#$res=~ s/\"//g;
	
	return $res;
}

