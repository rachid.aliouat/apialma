#!/usr/bin/perl -w
##################
# Rachid Aliouat
# Le 30/03/2020
# Question les API Alma pour sortir une liste de bib/holdings/items sur la base d'un csv contenant des PPN
#################
=pod
###############
#  Algorythme #
###############
=pod
Lire un PPN
	questionner l'"api sur PPN" (etape1)
	=>Recupérer tous les MMS_ID des notices bib renvoyées
	si retour ok
		alors lire "api holding sur mms_id de la bib" (etape 2)
		recuperer tous les mms_id des holding
		si pas de holding
			alors ecrire une ligne d'exemplaire avec le PPN et le mms_id de la bib +l'information "holding non trouvée"
		sinon
			Pour chaque mms_id de holding
				lire "api item sur le mms_id de la holding" (etape 3)
				si pas de item alors
					ecrire une ligne avec bib + holding + information "pas d'exemplaire"
				sinon 
					ecrire une ligne pour chaque exemplaire
				fin si
			fin pour
		fin si
	sinon
		ecrire une ligne d'exemplaire avec l'information "PPN non trouvé"
	fin si
=cut
#
####################
#######################
use strict;
use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use URI::Encode qw(uri_encode uri_decode);; #encoder decoder les URL   - requiert la bibliotheque liburi-encode-perl

my $ppn="";
my @tab_ppn=();
my %hash_mms_id_ppn=();#table de correspondance bib(mms_id)=>ppn(network_number)
my %hash_ppn_inconnu=();
my %hash_mms_id_data_bib=();
my %hash_holding_mms_id=();#table de correspondance holding(holding_id)=>bib(mms_id)
my %hash_holding_data_hold=();
my %hash_bib_sans_holding=();
my %hash_holding_sans_item=();
my %hash_item_holding=(); # table de correspondance item(pid)=>holding(holding_id)
my %hash_item_data=();

my $root= "";
my $bib_mms_id= "";
my $bib_ppn= "";
my $bib_title = "";
my $bib_author = "";
my $bib_date_publication = "";
my $bib_date_debut_publication ="";
my $bib_date_fin_publication ="";
my $bib_lieu_publication = "";
my $bib_editeur = "";
my $bib_date_creation = "";
my $bib_date_modification = "";
my $bib_suppr_deladecouverte = "";
my $bib_type_notice ="";
my $bib_localisation_930="";

my $holding_id ="";
my $holding_library=""; 
my $holding_library_desc=""; 
my $holding_location=""; 
my $holding_location_desc="";
my $holding_call_number="";
my $holding_suppress_from_publishing="";
my $holding_isbn="";
my $holding_issn="";

my $item_pid=""; 
my $item_barcode=""; 
my $item_complete_edition="";
my $item_call_number_type="";
my $item_alternative_call_number="";
my $item_description="";
my $item_inventory_number="";
my $item_policy="";
my $item_public_note="";
my $item_fulfillment_note="";
my $item_internal_note_1="";
my $item_internal_note_2="";
my $item_internal_note_3="";
my $item_statistics_note_1="";
my $item_statistics_note_2="";
my $item_statistics_note_3="";
my $item_provenance="";
my $tem_physical_material_type="";
my $item_base_status_desc="";
my $item_arrival_date="";
my $item_in_temp_location="";
my $item_temp_library="";
my $item_temp_location="";
my $item_temp_call_number="";
my $item_temp_policy="";
my $item_inventory_date="";
my $item_creation_date="";
my $item_modification_date="";
my $item_nbr_loan=""; 
my $item_pieces="";

my $prompt_filtre="";
my $cpt=0; #compteur exemplaire

######### Creation des structures TWIG XML

my $twig_bib=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
);

my $twig_hold=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
);

my $twig_item=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML pour les exemplaires
		
        pretty_print  => 'indented',
);

my $twig_rapport=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML pour api analytics
		
        pretty_print  => 'indented',
);


##### LIRE LE FICHIER DE CONF
my %hash_conf=();
open (CONF, "config.conf") or die "Ouverture fichier de configuration config.conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
my @tab_inter = <CONF>; 
close(CONF);
my $i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	print $tab_inter[$i];
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure_conf($ligne[1]); # alimentation de la table de hash avec la conf

}
print "-" x 20, "\n";
####################

##### LIRE LE FICHIER DES PPN

if (!(exists $ARGV[0])) {print "Manque le fichier de ppn en parametre.\n";exit(0)};

my $nom_fic=$ARGV[0];

print "traitement du fichier $nom_fic\n";

####
#####
open (FIC, $nom_fic) or die "Ouverture fichier des demandes impossible , cause : $! \n";
print "lecture du fichier des PPN:\n";
print "-" x 20, "\n";
@tab_inter = <FIC>; 
close(FIC);
my $j=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	if (length($tab_inter[$i]) > 5) { # si la donnée est trop courte, on ne la prend pas
		$tab_ppn[$j]=$tab_inter[$i];
		$j++;
	}
}


print "Il y a ".($#tab_ppn+1)." PPN a traiter.","\n";

########### creer entete fichier resultat
print "#####################\nEcriture du fichier CSV...\n";
open (FIC, ">liste-exemplaire.csv") or die "Ouverture fichier de liste exemplaire CSV impossible , cause : $! \n";
binmode(FIC, ":utf8");
#entete
print FIC "bib_mms_id\tbib_ppn\tbib_titre\tbib_auteur\tbib_date_publication\tbib_date_debut_publication\tbib_date_fin_publication\tbib_lieu_publication\tbib_editeur\tbib_date_creation\tbib_date_modification\tbib_suppr_deladecouverte\tbib_type_notice\tbib_localisation_930"; 
print FIC "\tholding_id\tholding_code_bib\tholding_libelle_bib\tholding_code_localisation\tholding_libelle_localisation\tholding_cote\tholding_suppr_de_la_decouverte\tholding_isbn\tholding_issn";
print FIC "\titem_pid\titem_barcode\titem_mention_edition\titem_type_de_cote\titem_cote_exemplaire_alternative\titem_description\titem_numero_inventaire\titem_exception_de_pret_policy\titem_note_publique\titem_note_service_aux_usagers\titem_note__interne_1\titem_note_interne_2\titem_note_interne_3\titem_note_stat_1\titem_note_stat_2\titem_note_stat_3\titem_provenance\titem_type_de_materiel\titem_statut\titem_date_reception\titem_dans_localisation_temporaire\titem_code_bib_temporaire\titem_localisation_temporaire\titem_cote_temporaire\titem_exception_temporaire\titem_date_inventaire\titem_date_creation\titem_date_modification\titem_nombre_elements\titem_nombre_de_prets";
print FIC "\n";
#close (FIC);


########### Appel de l'API bib pour chaque PPN

print "Récupératon de tous les MMS_ID des données bib sur la base des PPN :\n------\n";
for ($i=0;$i<=$#tab_ppn;$i++) {
	$ppn=epure($tab_ppn[$i]);
	print"##################-------------####################\n";
	print $ppn,"\n";
	my $block_xml=get_bib_mms_id_from_network_number($ppn); # Appel de l'API sur toutes les bibs pour recuperer les mms_id
	if ($block_xml !~ /total_record_count=\"0\"/) { ## notice bib ok => retour OK  ===> doit être différent de bibs total_record_count="0" 
		$twig_bib->parse($block_xml); # ici tout est dans la structure twig
		#print $twig_bib->sprint; # affichage du contenu
		print "1ere notice renvoyee:\n";
		my $root= $twig_bib->root->first_child('bib');  
		my $bib_mms_id = defined $root->first_child('mms_id')?$root->first_child('mms_id')->text:"";
		my $bib_mms_title =  defined $root->first_child('title')?$root->first_child('title')->text:"";
		my $bib_mms_author = defined $root->first_child('author')?$root->first_child('author')->text:"";
	
		print "\nmms_id : $bib_mms_id\n";
		print "Titre: $bib_mms_title\n";
		print "Auteur : $bib_mms_author\n";
		
		print "=============> Recuperation de tous les mms_id :\n";
		my @tab_mms_id = $twig_bib->root->children('bib');  
		foreach my $un_mms_id (@tab_mms_id) {
			my $mms_id=$un_mms_id->first_child('mms_id')->text ;
			$hash_mms_id_ppn{$mms_id}=$ppn;
			print $mms_id."->".$hash_mms_id_ppn{$mms_id},"\n";
			
			############## APPEL TRAITEMENT POUR CHAQUE MMS_ID
			my $k=$mms_id;
			my $v=$ppn;
			print "MMS_ID=$k PPN=$v\n";
			my $block_xml=get_bib_mms_id($k); # Appel de l'API bib avec le mms_id
			print $block_xml,"\n";
			print "$v\n";
			if ($block_xml =~ /\Q$v/ ){
			## notice bib ok => retour OK et elle contient le bon ppn 
			#(suite au problème de l'indexer alma qui isole les chiffres du ppn et peut renvoyer un issn ou un numero oclc avec cette suite de chiffre
			$twig_bib->parse($block_xml); # ici tout est dans la structure twig
			#print $twig_bib->sprint; # affichage du contenu
			### stockage des données bib
			alimente_bib_mms_id ($twig_bib,$k,$v);

			}
			else { #problème avec la notice bib : à ce stade situation impossible sauf crash server api entre deux
				# ou autre probleme : une seule notice retournee et elle n'a pas le PPN attendu
				$bib_mms_id = $k;
				$bib_ppn=$v;
				print "PPN non trouve dans notice bib : [$bib_mms_id][$bib_ppn] \n";
				$bib_title = "Probleme retour erreur api";
				$bib_author = $bib_date_publication = $bib_date_debut_publication = $bib_date_fin_publication = $bib_lieu_publication = $bib_editeur = $bib_date_creation = $bib_date_modification = $bib_suppr_deladecouverte=$bib_type_notice="";
			}
		print "[$bib_mms_id][$bib_ppn][$bib_title][$bib_author][$bib_date_publication][$bib_date_debut_publication][$bib_date_fin_publication][$bib_lieu_publication][$bib_editeur][$bib_date_creation][$bib_date_modification][$bib_suppr_deladecouverte][$bib_type_notice][$bib_localisation_930]\n";
		$hash_mms_id_data_bib{$k}="$bib_mms_id\t$bib_ppn\t$bib_title\t$bib_author\t$bib_date_publication\t$bib_date_debut_publication\t$bib_date_fin_publication\t$bib_lieu_publication\t$bib_editeur\t$bib_date_creation\t$bib_date_modification\t$bib_suppr_deladecouverte\t$bib_type_notice\t$bib_localisation_930";
		#print "Stockage en memoire:".$hash_mms_id_data_bib{$k},"\n";
	
		############## APPEL TRAITEMENT sur holding POUR CHAQUE MMSID
		$k=$bib_mms_id;
		$v=$bib_ppn;
		print "MMS_ID=$k PPN=$v\n";
		$block_xml=get_holding_mms_id($k); # Appel de l'API holding avec le mms_id
		if ($block_xml !~ /total_record_count=\"0\"/) { ## notice holdings ok => retour OK
			print "Il y a au moins 1 holding \n";
			##parcourir toutes les holding filles
			$twig_hold->parse($block_xml); # ici tout est dans la structure twig
			$root= $twig_hold->get_xpath('//bib_data',0);  
			$holding_isbn=defined $root->first_child('isbn')?$root->first_child('isbn')->text:"";
			$holding_issn=defined $root->first_child('issn')?$root->first_child('issn')->text:"";
			print "isbn = $holding_isbn\n";
			print "issn = $holding_issn\n";

			#print $twig_hold->sprint; # affichage du contenu
			$block_xml=~ /<holdings total_record_count=\"(\d+)\">/;
			my $nbr_holding=$1;
			print "Nombre de holding = $nbr_holding\n";
			my $root= $twig_hold->get_xpath('//holdings',0);  
			#my $root= $twig_hold->('holdings');  
			#print $root->sprint; # affichage du contenu
			my @holding= $root->children( 'holding');   # recuperer toutes les holdings
		
			foreach my $holding (@holding)
				{ 
				$v=alimente_holding($holding,$k);
			
				############## APPEL TRAITEMENT sur ITEM
				print "HOLDING_ID=$v MMS_ID=$k\n";
				#$k est le MMS_ID
				#$v est le Holding_id courant
				if ($hash_mms_id_data_bib{$hash_holding_mms_id{$v}} !~ /\tProbleme retour erreur api\t/) { # ne pas ecrire les lignes qui ne correspondent pas : cause probleme network_number
					my $nbr_item = recup_item($v,$k); ### Cette fonction ecrit la ligne dans le fichier CSV ####
					print "Nombre exemplaire pour cette holding = $nbr_item\n";
				}

			}
		}
		else { # pas de holding pour cette notice bib
			$hash_bib_sans_holding{$k}++;
			}
	}
		

	} # fin (s'il y une notice bib)
	else {#sinon, il n'y a pas de notice bib pour ce ppn
	print "$ppn Inconnu.\n";
	$hash_ppn_inconnu{$ppn}++;
	}
} 


######################################################
################# Generation du fichier exemplaire csv
######################################################
close(FIC);
########### Generation du rapport final
print "Ecriture du rapport final.\n";
open (FIC, ">rapport.txt") or die "Ouverture fichier de rapport impossible , cause : $! \n";
print FIC "Nombre d'exemplaire genere : $cpt\n";
print FIC "Liste des PPN introuvables : \n";
while( my ($k,$v) = each(%hash_ppn_inconnu) ) { 
	print FIC "$k\n";
}
print FIC "------------------------\n";
print FIC "Liste des notices bib sans holding : \n";
while( my ($k,$v) = each(%hash_bib_sans_holding) ) { 
	print FIC "$k\t". $hash_mms_id_ppn{$k}."\n";
}
print FIC "------------------------\n";
print FIC "Liste des holding sans exemplaire: \n";
while( my ($k,$v) = each(%hash_holding_sans_item) ) { 
	print FIC "$k\n";
}

close(FIC);
print "#####################\nFin.\n";


############################
########## Fonctions
############################
#Fonction pour aller chercher les exemplaires
sub recup_item {
	my ($v,$k)=@_;  # on récupére la notice bib courante et les cle/valeur courante
	#$k est le MMS_ID
	#$v est le Holding_id courant
	my $nbr_item=0;
	my $item_pid="";

	my $block_xml=get_item_holding_id($v,$k); # Appel de l'API holding avec le mms_id
	if ($block_xml =~ /item/) { ## notice holdings ok => retour OK
		##parcourir tous les exemplaires 
		$twig_item->parse($block_xml); # ici tout est dans la structure twig
		#print $twig_item->sprint; # affichage du contenu
		$block_xml=~ /<items total_record_count=\"(\d+)\">/;
		$nbr_item=$1;
		print "Nombre de item(exemplaire) = $nbr_item\n";
		my $root= $twig_item->get_xpath('//items',0);  
		#print $root->sprint; # affichage du contenu
		my @item= $root->children( 'item');   # recuperer toutes les exemplaire
		foreach my $item (@item)
			{ 
			$item_pid=alimente_item($item,$v);
			## ECRIRE DANS LE FICHIER DES EXEMPLAIRES
			
				print FIC $hash_mms_id_data_bib{$hash_holding_mms_id{$v}}. $hash_holding_data_hold{$v}. $hash_item_data{$item_pid} ."\n";
				$cpt++;
				
			}
		}
		else { # pas de item pour cette notice holding
			$hash_holding_sans_item{$k}++;
			}
	return ($nbr_item);
}
	
	


############ fonction qui va formater les données des notice bib sur la basse MMS_ID
sub alimente_bib_mms_id{
	my ($twig_bib,$k,$v)=@_;  # on récupére la notice bib courante et les cle/valeur courante

	
	$root= $twig_bib->root;
	$bib_mms_id = $k;
	$bib_ppn=$v;
	$bib_title = defined $root->first_child('title')?epure($root->first_child('title')->text):"";
	$bib_author = defined $root->first_child('author')?epure($root->first_child('author')->text):"";
	$bib_date_publication = defined $root->first_child('date_of_publication')?epure($root->first_child('date_of_publication')->text):"";
	$bib_date_debut_publication ="";
	$bib_date_fin_publication ="";
	$bib_lieu_publication =defined $root->first_child('place_of_publication')?epure($root->first_child('place_of_publication')->text):"";
	$bib_editeur =defined $root->first_child('publisher_const')?epure($root->first_child('publisher_const')->text):"";
	$bib_date_creation =defined $root->first_child('created_date')?epure($root->first_child('created_date')->text):"";
	$bib_date_creation=date_us_vers_fr($bib_date_creation);
	$bib_date_modification =defined $root->first_child('last_modified_date')?epure($root->first_child('last_modified_date')->text):"";
	$bib_date_modification = date_us_vers_fr($bib_date_modification);
	$bib_suppr_deladecouverte =defined $root->first_child('suppress_from_publishing')?epure($root->first_child('suppress_from_publishing')->text):"";
	# recuperation du champ 181 : record/ <datafield ind1=" " ind2=" " tag="181"> / <subfield code="c">txt</subfield>
	$bib_type_notice = defined $root->get_xpath('//datafield[@ind1=" " and @ind2=" " and @tag="181"]/subfield[@code="c"]',0)?epure($root->get_xpath('//datafield[@ind1=" " and @ind2=" " and @tag="181"]/subfield[@code="c"]',0)->text):"";
	# recuperation du champ 930 $5 $b $e $j : <datafield ind1=" " ind2=" " tag="930"><subfield code="5">590092102:043151116</subfield><subfield code="b">590092102</subfield><subfield code="e">R BRO REG E</subfield><subfield code="j">u</subfield></datafield>
	$bib_localisation_930 = defined $root->get_xpath('//datafield[@ind1=" " and @ind2=" " and @tag="930"]/subfield[@code="5"]',0)?epure($root->get_xpath('//datafield[@ind1=" " and @ind2=" " and @tag="930"]/subfield[@code="5"]',0)->text):"";
	$bib_localisation_930 .=" ";
	$bib_localisation_930 .=defined $root->get_xpath('//datafield[@ind1=" " and @ind2=" " and @tag="930"]/subfield[@code="b"]',0)?epure($root->get_xpath('//datafield[@ind1=" " and @ind2=" " and @tag="930"]/subfield[@code="b"]',0)->text):"";
	$bib_localisation_930 .=" ";
	$bib_localisation_930 .=defined $root->get_xpath('//datafield[@ind1=" " and @ind2=" " and @tag="930"]/subfield[@code="e"]',0)?epure($root->get_xpath('//datafield[@ind1=" " and @ind2=" " and @tag="930"]/subfield[@code="e"]',0)->text):"";
	$bib_localisation_930 .=" ";
	$bib_localisation_930 .=defined $root->get_xpath('//datafield[@ind1=" " and @ind2=" " and @tag="930"]/subfield[@code="j"]',0)?epure($root->get_xpath('//datafield[@ind1=" " and @ind2=" " and @tag="930"]/subfield[@code="j"]',0)->text):"";
	}

############ fonction qui va formater les données des items lié à une holding
sub alimente_holding {
	my ($holding,$bib)=@_;  # on récupére la holding courante et le numero de bib liée

	#$holding->sprint;
	$holding_id=$holding->first_child('holding_id')->text; 
	$holding_library=defined $holding->first_child('library')?$holding->first_child('library')->text:""; 
	$holding_library_desc=defined $holding->first_child('library')?$holding->first_child('library')->att('desc'):""; 
	$holding_location=defined $holding->first_child('location')?$holding->first_child('location')->text:""; 
	$holding_location_desc=defined $holding->first_child('location')?$holding->first_child('location')->att('desc'):""; 
	$holding_call_number=defined $holding->first_child('call_number')?$holding->first_child('call_number')->text:""; 
	$holding_suppress_from_publishing=defined $holding->first_child('suppress_from_publishing')?$holding->first_child('suppress_from_publishing')->text:""; 
	
	$hash_holding_mms_id{$holding_id}=$bib; # stocker le tuple holding_id=>mms_id de sa bib mere
	print "[$bib][$holding_id][$holding_library][$holding_library_desc][$holding_location][$holding_location_desc][$holding_call_number][$holding_suppress_from_publishing][$holding_isbn][$holding_issn]\n";
	$hash_holding_data_hold{$holding_id}="\t$holding_id\t$holding_library\t$holding_library_desc\t$holding_location\t$holding_location_desc\t$holding_call_number\t$holding_suppress_from_publishing\t$holding_isbn\t$holding_issn";
	print "Stockage en memoire:".$hash_holding_data_hold{$holding_id},"\n";
	
	return ($holding_id);
	
}

############ fonction qui va formater les données des items lié à un exemlaire
sub alimente_item {
	my ($item,$holding)=@_;  # on récupére le item courant et le numéro de holding lié
	$item_pid=$item->first_child('item_data')->first_child('pid')->text; 
	$item_barcode=defined $item->first_child('item_data')->first_child('barcode')?$item->first_child('item_data')->first_child('barcode')->text:""; 
	$item_complete_edition=defined $item->first_child('bib_data')->first_child('complete_edition')?$item->first_child('bib_data')->first_child('complete_edition')->text:""; 
	$item_call_number_type=defined $item->first_child('holding_data')->first_child('call_number_type')?$item->first_child('holding_data')->first_child('call_number_type')->att('desc'):""; 
	$item_alternative_call_number=defined $item->first_child('item_data')->first_child('alternative_call_number')?$item->first_child('item_data')->first_child('alternative_call_number')->text:""; 
	$item_description=defined $item->first_child('item_data')->first_child('description')?$item->first_child('item_data')->first_child('description')->text:""; 
	$item_inventory_number=defined $item->first_child('item_data')->first_child('inventory_number')?$item->first_child('item_data')->first_child('inventory_number')->text:""; 
	$item_policy=defined $item->first_child('item_data')->first_child('policy')?$item->first_child('item_data')->first_child('policy')->text:""; 
	$item_public_note=defined $item->first_child('item_data')->first_child('public_note')?$item->first_child('item_data')->first_child('public_note')->text:""; 
	$item_fulfillment_note=defined $item->first_child('item_data')->first_child('fulfillment_note')?$item->first_child('item_data')->first_child('fulfillment_note')->text:""; 
	$item_internal_note_1=defined $item->first_child('item_data')->first_child('internal_note_1')?$item->first_child('item_data')->first_child('internal_note_1')->text:""; 
	$item_internal_note_2=defined $item->first_child('item_data')->first_child('internal_note_2')?$item->first_child('item_data')->first_child('internal_note_2')->text:""; 
	$item_internal_note_3=defined $item->first_child('item_data')->first_child('internal_note_3')?$item->first_child('item_data')->first_child('internal_note_3')->text:""; 
	$item_statistics_note_1=defined $item->first_child('item_data')->first_child('statistics_note_1')?$item->first_child('item_data')->first_child('statistics_note_1')->text:""; 
	$item_statistics_note_2=defined $item->first_child('item_data')->first_child('statistics_note_2')?$item->first_child('item_data')->first_child('statistics_note_2')->text:""; 
	$item_statistics_note_3=defined $item->first_child('item_data')->first_child('statistics_note_3')?$item->first_child('item_data')->first_child('statistics_note_3')->text:""; 
	$item_provenance=defined $item->first_child('item_data')->first_child('provenance')?$item->first_child('item_data')->first_child('provenance')->text:""; 
	$tem_physical_material_type=defined $item->first_child('item_data')->first_child('physical_material_type')?$item->first_child('item_data')->first_child('physical_material_type')->text:""; 
	$item_base_status_desc=defined $item->first_child('item_data')->first_child('base_status')?$item->first_child('item_data')->first_child('base_status')->att('desc'):""; 
	$item_arrival_date=defined $item->first_child('item_data')->first_child('arrival_date')?$item->first_child('item_data')->first_child('arrival_date')->text:""; 
	$item_arrival_date=date_us_vers_fr($item_arrival_date);
	$item_in_temp_location=defined $item->first_child('holding_data')->first_child('in_temp_location')?$item->first_child('holding_data')->first_child('in_temp_location')->text:""; 
	$item_temp_library=defined $item->first_child('holding_data')->first_child('temp_library')?$item->first_child('holding_data')->first_child('temp_library')->text:""; 
	$item_temp_location=defined $item->first_child('holding_data')->first_child('temp_location')?$item->first_child('holding_data')->first_child('temp_location')->text:""; 
	$item_temp_call_number=defined $item->first_child('holding_data')->first_child('temp_call_number')?$item->first_child('holding_data')->first_child('temp_call_number')->text:""; 
	$item_temp_policy=defined $item->first_child('holding_data')->first_child('temp_policy')?$item->first_child('holding_data')->first_child('temp_policy')->text:""; 
	$item_inventory_date=defined $item->first_child('item_data')->first_child('inventory_date')?$item->first_child('item_data')->first_child('inventory_date')->text:""; 
	$item_inventory_date=date_us_vers_fr($item_inventory_date);
	$item_creation_date=defined $item->first_child('item_data')->first_child('creation_date')?$item->first_child('item_data')->first_child('creation_date')->text:""; 
	$item_creation_date=date_us_vers_fr($item_creation_date);
	$item_modification_date=defined $item->first_child('item_data')->first_child('modification_date')?$item->first_child('item_data')->first_child('modification_date')->text:""; 
	$item_modification_date=date_us_vers_fr($item_modification_date);
	$item_pieces=defined $item->first_child('item_data')->first_child('pieces')?$item->first_child('item_data')->first_child('pieces')->text:""; 
	print "ITEM PID = $item_pid\n";
	print "BARCODE = $item_barcode \n";
	$hash_item_holding{$item_pid}=$holding; # stocker le tuple item_pid=>holding_id de sa holding mere
			
	####### récupérer le nombre de prêts avec l'API Analytics => à mettre dans $item_nbr_loan
			
	#### changer le filtre
	if ($hash_conf{avec_nbr_pret}=~/oui/) {
		$prompt_filtre = $hash_conf{prompt}; # on récupere le filtre vierge
		$prompt_filtre=~ s/ITEM_PID/$item_pid/g; # on y met le PID
		my $block_xml=get_alma_analytics_with_prompt($hash_conf{rapport_prompt},$prompt_filtre); # Appel de l'API analytics sur le nom d'un rapport dans le fichier de conf
		$twig_rapport->parse($block_xml); # ici tout est dans la structure twig
		#print $twig_rapport->sprint; # affichage du contenu
		#$twig_rapport=$root->children( 'item');
		$item_nbr_loan= defined $twig_rapport->get_xpath('//Row/Column2',0)?$twig_rapport->get_xpath('//Row/Column2',0)->text:"0"; 
	} else
	{
		$item_nbr_loan="";
	}
	###############"
			
	print "[$item_pid][$item_barcode][$item_complete_edition][$item_call_number_type][$item_alternative_call_number][$item_description][$item_inventory_number][$item_policy][$item_public_note][$item_fulfillment_note][$item_internal_note_1][$item_internal_note_2][$item_internal_note_3][$item_statistics_note_1][$item_statistics_note_2][$item_statistics_note_3][$item_provenance][$tem_physical_material_type][$item_base_status_desc][$item_arrival_date][$item_in_temp_location][$item_temp_library][$item_temp_location][$item_temp_call_number][$item_temp_policy][$item_inventory_date][$item_creation_date][$item_modification_date][$item_pieces][$item_nbr_loan]\n";
	$hash_item_data{$item_pid}="\t$item_pid\t$item_barcode\t$item_complete_edition\t$item_call_number_type\t$item_alternative_call_number\t$item_description\t$item_inventory_number\t$item_policy\t$item_public_note\t$item_fulfillment_note\t$item_internal_note_1\t$item_internal_note_2\t$item_internal_note_3\t$item_statistics_note_1\t$item_statistics_note_2\t$item_statistics_note_3\t$item_provenance\t$tem_physical_material_type\t$item_base_status_desc\t$item_arrival_date\t$item_in_temp_location\t$item_temp_library\t$item_temp_location\t$item_temp_call_number\t$item_temp_policy\t$item_inventory_date\t$item_creation_date\t$item_modification_date\t$item_pieces\t$item_nbr_loan";
	print "Stockage en memoire:".$hash_item_data{$item_pid},"\n";
	
	return ($item_pid);
}



############## fonction API GET pour questionner Alma : Recuperer les exemplaires(item) sur la base du holding_id et MMS_ID de la notice
sub get_item_holding_id{
	my ($holding_id,$mms_id)=@_;  # on récupére le holding_id et le mms_id en paramétre
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id/holdings/$holding_id/items?limit=100&offset=0&order_by=none&direction=desc&apikey=".$hash_conf{api_cle_bibs};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			#print $res->content;
			print "Existence notice exemplaires OK pour mms_id = $mms_id et holding_id = $holding_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
	
}


############## fonction API GET pour questionner Alma : Recuperer les holdings sur la base du MMS_ID de la notice
sub get_holding_mms_id{
	my ($mms_id)=@_;  # on récupére le mms_id en paramétre
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id/holdings?apikey=".$hash_conf{api_cle_bibs};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			#print $res->content;
			print "Retour requete API pour $mms_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
	
}

############## fonction API GET pour questionner Alma : Recuperer les MMS_ID sur la base du PPN
sub get_bib_mms_id{
	my ($mms_id)=@_;  # on récupére le mms_id en paramétre
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id?view=full&expand=None&apikey=".$hash_conf{api_cle_bibs};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			#print $res->content;
			print "Existence notice bib OK pour $mms_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
	
}




############## fonction API GET pour questionner Alma : Recuperer les MMS_ID sur la base du PPN
sub get_bib_mms_id_from_network_number{
	my ($network_number)=@_;  # on récupére le ppn (ou autre network_number) en paramétre
	
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs?view=full&expand=None&view=brief&other_system_id=$network_number&apikey=".$hash_conf{api_cle_bibs};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			#print $res->content;
			print "Existence notice bib OK pour $network_number\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}

############## fonction API GET pour questionner Alma API Analytics: Recuperer les données sur la base du nom du rapport
sub get_alma_analytics_with_prompt{
	my ($nom_rapport,$prompt)=@_;  # on récupére le nom du rapport et le filtre(prompt) en paramétre
	my $uri     = URI::Encode->new( { encode_reserved => 0 } );
	my $nom_rapport_encoded = $uri->encode($nom_rapport);
	my $prompt_encoded=$uri->encode($prompt);
	#my $nom_rapport_encoded = uri_encoded($nom_rapport);
	#print "$nom_rapport\n$nom_rapport_encoded\n";
	#print "$prompt\n$prompt_encoded\n";
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?path=$nom_rapport_encoded&limit=25&col_names=true&apikey=".$hash_conf{api_cle_analytics}."&filter=".$prompt_encoded;
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_analytics}, $hash_conf{api_cle_analytics}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			#print $res->content;
			print "Existence notice OK pour $nom_rapport\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print $res->content;
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}



########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	$res=~ s/\t//g;
	
	return $res;
}

sub epure_conf { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	
	return $res;
}
### Transforme une date us en date fr 2019-08-24Z => 24/08/2019

sub date_us_vers_fr {
	my($date)=@_;
	if ($date=~ /^(\d\d\d\d)-(\d\d)-(\d\d)Z$/) { #si la date est comme attendu ok, sinon on ne fait rien
	#$date=~ /^(\d\d\d\d)-(\d\d)-(\d\d)Z$/;
		$date = "$3"."/".$2."/".$1;
	}
	
	return $date;
}
