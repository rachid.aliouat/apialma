#!/usr/bin/perl
##############################
# auteur : Rachid
# date : 26-01-2022
# objet : activer des porfolios dans ALma via API. Source de données : un fichier CSV
#		on active les portfolios ayant une url avec pepite-depot
#	24/01/2023 : correction des commentaires faisant alusion à pepite-depot au lieu de motif_url
#	09-02-2023 : ajout cas csv multicolonne : le fichier csv peut avoir plusieurs colonne , c'est la 1er qui contient le mms_id
#	31-03-2023 : ajout activation du proxy si la variable proxy est alimentée dans la conf
#	18-07-2023 : modif du code : tri sur la clé portofolio_id lors du traitement pour avoir un fichier journal plus linéaire
#	29/11/2024 : passage du fichier de conf en parametre
###########
#	passage de parametre -> active_portfolios.pl	fichier-conf.conf	liste.csv
##############################
##############################
use strict;
#use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use URI::Encode qw(uri_encode uri_decode);; #encoder decoder les URL   - requiert la bibliotheque liburi-encode-perl
use MIME::Lite;

my $nom_fic_csv="";
my %hash_conf=();
my $nom_fic_conf="";
my @tab_inter = ();
my $i=0;
my $mms_id="";
my %hash_mms_id=(); # MMS_ID=> PPN
my $block_xml="";
my $twig_portfolios="";
my %hash_portfolios=();
my $balise_available="<availability desc=\"Available\">11</availability>";
my $balise_not_available="<availability desc=\"Not Available\">10</availability>";
my $balise_proxy_enable="<proxy_enabled desc=\"Yes\">true</proxy_enabled>";
my $balise_proxy="";



if (!(exists $ARGV[0])) {print "Manque le fichier de conf en parametre.\n";exit(0)};
$nom_fic_conf=$ARGV[0];
if (!(exists $ARGV[1])) {print "Manque le fichier des MMS_ID en parametre.\n";exit(0)};
$nom_fic_csv=$ARGV[1];

##### LIRE LE FICHIER DE CONF

open (CONF, $nom_fic_conf) or die "Ouverture fichier de configuration $nom_fic_conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
@tab_inter = <CONF>; 
close(CONF);
$i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	#print $tab_inter[$i];
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf
	

}
print "-" x 20, "\n";

print "-" x 20, "\n";
#### Definir la date de travail 
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
my $date_jour_rapport=sprintf("%04d-%02d-%02d", (1900+$year), ($mon+1),$mday);
my $date_jour=sprintf("%02d%02d%02d", ($year-100), ($mon+1),$mday);

print "Travail sur la date du jour suivante : $date_jour_rapport\n";

################ TRAITEMENT DU CSV LIGNE A LIGNE
open (CSV, $nom_fic_csv) or die "Ouverture fichier de CSV $nom_fic_csv impossible , cause : $! \n";
print "lecture des MMS_ID:\n";
print "-" x 20, "\n";
@tab_inter = <CSV>; 
close(CSV);
######## Ouvrir le rapport d'errreur des lignes non traité : par exemple PPN inconnu
open (ERR, ">journal-activer-portfolios-$date_jour_rapport.txt") or die "Ouverture fichier de journal impossible , cause : $! \n";
print ERR "Motif\tCommentaire\n"; # on récupére l'entête , on ajoute une colone avant pour renseigner ensuite le motif du rejet
close (ERR);



##### Traitement principale
######### Recuperer tous les portfolios
$i=1; # on passe la ligne d'entete => on commence à 1
for ($i;$i<=$#tab_inter;$i++) {
	print "############ Traitement de la ligne : ################\n$tab_inter[$i]#########################\n";
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne dès que le CSV comporte plusieurs colonne
	#$mms_id=epure($tab_inter[$i]);
	$mms_id=epure($ligne[0]);
	print "############ Traitement du MMS_ID : ################\n$mms_id\n#########################\n";
	## passer les ligne vide
	$twig_portfolios=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
	);
	$block_xml=get_portfolios_from_mms_id($mms_id); # Appel de l'API sur le mms_id pour récuperer toutes les portfolios
	#print $block_xml,"\n";
	if  ($block_xml !~ /total_record_count="0"/) { # s'il y a un portfolos
		$twig_portfolios->parse($block_xml); # ici tout est dans la structure twig
		my $root= $twig_portfolios->root->first_child('portfolios');  
		print "=============> Recuperation de tous les id de portfolios :\n";
		my @tab_portfolios_id = $twig_portfolios->root->children('portfolio'); 
		foreach my $un_portfolio_id (@tab_portfolios_id) {
			my $portfolio_id=$un_portfolio_id->first_child('id')->text ; #portfolio_id
			print $portfolio_id,"\n";
			$hash_portfolios{$portfolio_id}=$mms_id;
			
			}
	} else { # log erreur
		print "Aucun portfolios pour $mms_id\n";
		ecrire_log("Aucun portfolio",$mms_id);

	}

}

########### Activer tous les portfolios pour lesquels l'url contient le motif (motif_url)
print "Liste des portoflios a requeter : \n";
my $c="";
my $v="";
#while (($c,$v) = each(%hash_portfolios)) {
foreach $c (sort keys %hash_portfolios) {
	$v=$hash_portfolios{$c};
	print "$c==>$v\n";
}

##### Recuperer chaque portoflio pour analyse
#while (($c,$v) = each(%hash_portfolios)) {
foreach $c (sort keys %hash_portfolios) {
	$v=$hash_portfolios{$c};
	
	print "Portoflio $c ==lie a mmd_is==>$v\n";
	$block_xml=get_portfolios_detail_from_mms_id_and_portfolio_id($v,$c); # Appel de l'API sur le mms_id pour récuperer toutes les portfolios
	print $block_xml,"\n";
	$block_xml=~ /<url>jkey=(.*)<\/url>/;
	my $url=$1;
	### s'il y a une url contenant le motif (motif_url) : on active
	######### on remplace  <availability desc="Not Available">10</availability> par  <availability desc="Available">11</availability>
	if ($block_xml=~/$hash_conf{motif_url}/) {
		$block_xml=~ s/$balise_not_available/$balise_available/;
		### S'il y un proxy de déclarer dans le fichier de conf, on l'active
		$balise_proxy=$hash_conf{proxy};
		if (length($balise_proxy)>0) { #demande activation proxy
			$block_xml=~ s/<proxy_enabled.*\/proxy_enabled>/$balise_proxy_enable/;
			$block_xml=~ s/<proxy>.*<\/proxy>/<proxy>$balise_proxy<\/proxy>/;
		}
		print $block_xml,"\n";
		### API PUT pour mettre à jour la notice
		$block_xml=put_portfolios_detail_from_mms_id_and_portfolio_id($v,$c,$block_xml);
		ecrire_log("Activation","MMS_ID=$v --> Porfolio_id=$c --> $url");

	} else {
		ecrire_log("Pas d'activation","MMS_ID=$v --> Porfolio_id=$c --> $url");
	}
	
}

#### Envoi rapport par email
if ($hash_conf{rapport}=~ /yes/) {# dans conf la demande de rapport est à yes
	print "\nEnvoi rapport\n";
	
	my $Message = new MIME::Lite
	From =>epure($hash_conf{from}),
	To =>epure($hash_conf{email_destinataire}),
	Subject => epure($hash_conf{email_subject}).$date_jour_rapport,
	Type =>'multipart/mixed';
	attach $Message
		Type =>'TEXT',
		Encoding => 'quoted-printable',
		Data => "Bonjour,\nRapport de traitement automatique : voir PJ.\n";
	attach $Message
		Type =>'application/text',
		Path => "journal-activer-portfolios-$date_jour_rapport.txt" ,
		Filename => "journal-activer-portfolios-$date_jour_rapport.txt" ;
		
	my $return=$Message->send_by_smtp(epure($hash_conf{smtp}));
	print "Envoi rapport vers $hash_conf{email_destinataire} : $return\n";

	
	}


########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	
	return $res;
}

#### Fonction erreur : ecrire une ligne dans le rapport d'erreur
sub ecrire_log() {
	my ($motif,$ligne)=@_;

open (ERR, ">>journal-activer-portfolios-$date_jour_rapport.txt") or die "Ouverture fichier de journal.txt impossible , cause : $! \n";
print ERR "$motif\t$ligne\n";
close (ERR);

}


############## fonction API GET pour questionner Alma : Recuperer les blocs Portfolio sur la base du MMS_id
sub get_portfolios_from_mms_id{
	my ($mms_id)=@_;  # on récupére le mms_id en paramétre
	
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id/portfolios?apikey=".$hash_conf{api_cle_bibs};
	


	#print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			#print $res->content;
			print "Retour appel API OK pour $mms_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}

############## fonction API GET pour questionner Alma : Recuperer les identifiant Portfolio sur la base du MMS_id
sub get_portfolios_detail_from_mms_id_and_portfolio_id{
	my ($mms_id,$portfolio_id)=@_;  # on récupére le mms_id et le portfolio id en paramétre
	
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id/portfolios/$portfolio_id?apikey=".$hash_conf{api_cle_bibs};
	#print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			#print $res->content;
			print "Retour appel API OK pour $mms_id --- $portfolio_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}


############## fonction API PUT pour questionner Alma : Mettre a jour notice portfolio 
sub put_portfolios_detail_from_mms_id_and_portfolio_id {
		my ($mms_id,$portfolio_id,$block_xml)=@_;  # on récupére le mms_id et le portfolio id en paramétre ainsi que le block xml
		
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id/portfolios/$portfolio_id?apikey=".$hash_conf{api_cle_bibs};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(PUT => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	$req->content($block_xml); 

	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			#print $res->content;
			print "Retour appel API OK pour $mms_id --- $portfolio_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
	
}
