#!/usr/bin/perl
##############################
# auteur : Rachid
# date : 27-01-2023
# objet : créer des porfolios dans ALma via API. Source de données : un fichier CSV de MMS_ID
# 	Le programme va recuperer chaque notice (API GET Bib), et extraire tous les 856$u (URL) de la notice
#	Pour chaque URL, le programme va creer un portfolio lié à la notice (API POST Bib)
#	Le portfolio est créer selon le modéle du fichier modele-portfolio.xml : ce fichier contient 2 motifs à remplacer
#	avant le post : [MMS_ID] et [URL]
#	09-02-2023 : ajout cas csv multicolonne : le fichier csv peut avoir plusieurs colonne , c'est la 1er qui contient le mms_id
#	25/09/2023 : ajout 856$z dans "note public"
#	28/09/2023 : creer les portfolio dans l'ordre d'apparition des 856
#	04/12/2023 : ajouter dans les logs une information de creation du portfolio
#	29/11/2024 : passage du fichier de conf en parametre
###########
#	passage de parametre -> creer_portfolio.pl	fichier-conf.conf	liste.csv
##############################
use strict;
use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use URI::Encode qw(uri_encode uri_decode);; #encoder decoder les URL   - requiert la bibliotheque liburi-encode-perl
use utf8;
use Encode;

my $nom_fic_csv="";
my %hash_conf=();
my $nom_fic_conf="";
my @tab_inter = ();
my $i=0;
my $mms_id="";
my %hash_mms_id=(); # MMS_ID=> PPN
my $block_xml="";
my $twig_bib="";
my %tab_mms_id_url=(); # table de hash des mmd_id et url
my %tab_mms_id_url_ordonnee=(); # table de hash des mmd_id et url avec l'ordre d'apparition dans la notice
my $ordre_apparition_856=0;
my $nbr_portfolios=0;

if (!(exists $ARGV[0])) {print "Manque le fichier de conf en parametre.\n";exit(0)};
$nom_fic_conf=$ARGV[0];
if (!(exists $ARGV[1])) {print "Manque le fichier des MMS_ID en parametre.\n";exit(0)};
$nom_fic_csv=$ARGV[1];

##### LIRE LE FICHIER DE CONF

open (CONF, $nom_fic_conf) or die "Ouverture fichier de configuration $nom_fic_conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
@tab_inter = <CONF>; 
close(CONF);
$i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	print $tab_inter[$i];
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf
}
print "-" x 20, "\n";


################ TRAITEMENT DU CSV LIGNE A LIGNE
open (CSV, $nom_fic_csv) or die "Ouverture fichier de CSV $nom_fic_csv impossible , cause : $! \n";
print "lecture des MMS_ID:\n";
print "-" x 20, "\n";
@tab_inter = <CSV>; 
close(CSV);
for ($i=1;$i<=$#tab_inter;$i++) { #supprimer les lignes vides ou sans mms_id
	print "$tab_inter[$i] =>\n";
	if ($tab_inter[$i]=~ /^\d.*/) { # si cela ne commence par par un chiffre
		print "OK pour [".$tab_inter[$i]."]\n";
	} else {
		print "delete [".$tab_inter[$i]."]\n";
		delete $tab_inter[$i];
		
	}
}

######## Ouvrir le rapport d'errreur des lignes non traité : par exemple PPN inconnu
open (ERR, ">journal-creer-portfolios.txt") or die "Ouverture fichier de journal-creer-portfolios.txt impossible , cause : $! \n";
print ERR "Motif\tCommentaire\n"; # on récupére l'entête , on ajoute une colone avant pour renseigner ensuite le motif du rejet
close (ERR);
####### Chargé le fichier XML modele de portfolio
open (XML, "modele-portfolio.xml") or die "Ouverture fichier de modele-portfolio.xml impossible , cause : $! \n";
my @tab_fic=<XML>;
close (XML);
my $bloc_modele_portfolio="";
print "Modele XML de portfolio :\n";
print "[-------------------DEBUT XML\n";
for ($i=0;$i<=$#tab_fic;$i++) {
	$bloc_modele_portfolio.=epure($tab_fic[$i]);
	
}
print $bloc_modele_portfolio,"\n";
print "FIN XML-------------------]\n";
##### Traitement principale
######### Recuperer toutes les notices
$i=1; # on passe la ligne d'entete => on commence à 1
for ($i;$i<=$#tab_inter;$i++) {
	print "############ Traitement de la ligne : ################\n$tab_inter[$i]#########################\n";
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne dès que le CSV comporte plusieurs colonne
	#$mms_id=epure($tab_inter[$i]);
	$mms_id=epure($ligne[0]);
	print "############ Traitement du MMS_ID : ################\n$mms_id\n#########################\n";
	ecrire_log("Recherche portfolio",$mms_id);
	## passer les ligne vide
	$twig_bib=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
	);
	$block_xml=get_bib_mms_id($mms_id);
	#print $twig_bib->sprint; # affichage du contenu

	#print $block_xml,"\n";
	$twig_bib->parse($block_xml); # ici tout est dans la structure twig
	# Parcourir le bloc xml pour extraire les URL des 856$u
	my $root= $twig_bib->get_xpath('//bib/record/',0);
	my @les_datafield= $root->children('datafield');  ### on récuperer une table de hash de tous les "enfants"
	
	foreach my $un_datafield (@les_datafield)      # liste chaque datafield de la liste
		{ 
				
				my $tag = $un_datafield->{'att'}->{'tag'};
				
				if ($tag=="856") { # on est dans le bon datafield
					print "\n##### 856 #######\n";
					$ordre_apparition_856++;
					#print $un_datafield->sprint;
					#print "\n############\n";
					my $url="";
					my $note_public="";
					my @les_subfield= $un_datafield->children('subfield');  ### on récuperer une table de hash de tous les "enfants"
					foreach my $un_subfield (@les_subfield)      # liste chaque datafield de la liste
						{
						my $code = $un_subfield->{'att'}->{'code'};
						#print $code,"\n";
						if ($code=~ /^u$/) { # on est dans le bon subfield
							print "\n\t##### sous champ u #######\n";
							$url=$un_subfield->text();
							#print "\t".$un_subfield->text()."\n";
							print "\t".$url."\n";

							$tab_mms_id_url{"[".$mms_id."][".$url."]"}="";
							$tab_mms_id_url_ordonnee{"[".$mms_id."][".$url."]"}=$ordre_apparition_856;
							ecrire_log($mms_id,$url);
							$nbr_portfolios++;
						}
						if ($code=~ /^z$/) { # on est dans le bon subfield
							print "\n\t##### sous champ z #######\n";
							$note_public=$un_subfield->text();
							print "\t".$note_public."\n";
							$tab_mms_id_url{"[".$mms_id."][".$url."]"}=$note_public;
							
						}
					}
				}
		}
	
		$twig_bib->purge; #liberer la mémoire
}

### Recuperer les portfolio existant pour ces notices Car il faudra exclure les portfolio déjà existant pour ne pas creer de doublon
$i=1; # on passe la ligne d'entete => on commence à 1
my $twig_portfolios;
my %hash_portfolios=();
for ($i;$i<=$#tab_inter;$i++) {
	#$mms_id=epure($tab_inter[$i]);
	#print "############ Traitement du MMS_ID : ################\n$mms_id#########################\n";
	print "############ Traitement de la ligne : ################\n$tab_inter[$i]#########################\n";
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne dès que le CSV comporte plusieurs colonne
	#$mms_id=epure($tab_inter[$i]);
	$mms_id=epure($ligne[0]);
	print "############ Traitement du MMS_ID : ################\n$mms_id\n#########################\n";
	
	
	## passer les ligne vide
	$twig_portfolios=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
	);
	$block_xml=get_portfolios_from_mms_id($mms_id); # Appel de l'API sur le mms_id pour récuperer toutes les portfolios
	#print $block_xml,"\n";
	if  ($block_xml !~ /total_record_count="0"/) { # s'il y a un portfolio
		$twig_portfolios->parse($block_xml); # ici tout est dans la structure twig
		print $twig_portfolios->sprint , "\n";
		my $root= $twig_portfolios->root->first_child('portfolios');  
		print "=============> Recuperation de tous les id de portfolios :\n";
		my @tab_portfolios_id = $twig_portfolios->root->children('portfolio'); 
		foreach my $un_portfolio_id (@tab_portfolios_id) {
			my $portfolio_id=$un_portfolio_id->first_child('id')->text ; #portfolio_id
			$hash_portfolios{$portfolio_id}=$mms_id;
			ecrire_log("portfolio existant pour $mms_id:",$portfolio_id);
			
			}
	}  else {
				ecrire_log("Aucun portfolio existant",$mms_id);

	}

}

### Pour chaque portfolio, il faut retrouver l'URL
foreach my $c (sort keys %hash_portfolios) {
	my $mms_id=$hash_portfolios{$c};
	my $portfolio_id=$c;
	print "Requete API sur portfolio [$mms_id][$portfolio_id]\n";
	$block_xml=get_portfolios_detail_from_mms_id_and_portfolio_id($mms_id,$portfolio_id); # Appel de l'API sur le mms_id pour récuperer toutes les portfolios
	#print $block_xml,"\n";
	if ($block_xml=~ /<url>jkey=(.*)<\/url>/) {
		my $url=$1;
		print $mms_id ."---->".$url."\n";
		my $cle_a_suppr="[".$mms_id."][".$url."]";
		print "\n\nSupression de cette clé si elle existe dans la table des portolios à creer : $cle_a_suppr\n#####\n";
		delete $tab_mms_id_url{$cle_a_suppr};
		delete $tab_mms_id_url_ordonnee{$cle_a_suppr};
		ecrire_log("Existe déjà : Suppression de la liste des portfolios à traiter :",$cle_a_suppr);
	}
}
#exit(0);




## Liste de tous les portfolio à creer
print "\n###################\nListe de tous les portfolios à creer : \n";
#print "###\nTotal = $nbr_portfolios\n";
print "######\n";

my $c="";
my $v=0;
my $url="";
my $note_public="";
my $portfolio_a_creer="";
## Creer les portfolios dans l'ordre d'apparition des 856 grace à "tab_mms_id_url_ordonnee"
%tab_mms_id_url_ordonnee= reverse (%tab_mms_id_url_ordonnee); # on inverse la table , l'indice devient la clé
foreach $v (sort keys %tab_mms_id_url_ordonnee) { # on parcourt avec sort
	$c=$tab_mms_id_url_ordonnee{$v};
	print $c,"\n";
	$c=~/\[(.*)\]\[(.*)\]/;
	$mms_id=$1;
	$url=$2;
	$note_public=Encode::encode_utf8 $tab_mms_id_url{$c};
	print "--->Creation du portfolio $url pour le mms_id $mms_id:\n";
	$portfolio_a_creer=$bloc_modele_portfolio;
	$portfolio_a_creer=~ s/\[MMS_ID\]/$mms_id/g;
	$portfolio_a_creer=~ s/\[URL\]/$url/g;
	$portfolio_a_creer=~ s/\[NOTE_PUBLIC\]/$note_public/g; #ajout note public
	#print "\n$portfolio_a_creer\n";
	$block_xml=post_create_portfolio_mms_id($mms_id,$portfolio_a_creer);
	print $block_xml,"\n";
	ecrire_log("Creation portfolio",$mms_id."==>".$url);

}


########### FONCTIONS
########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	#$res=~ s/\"//g;
	
	return $res;
}

#### Fonction erreur : ecrire une ligne dans le rapport d'erreur
sub ecrire_log() {
	my ($motif,$ligne)=@_;

open (ERR, ">>journal-creer-portfolios.txt") or die "Ouverture fichier de journal-creer-portfolios.txt impossible , cause : $! \n";
print ERR "$motif\t$ligne\n";
close (ERR);

}



############## fonction API GET pour questionner Alma : Recuperer la HOLDING sur la base du MMS_id / holding
sub get_bib_mms_id{
	my ($mms_id)=@_;  # on récupére le mms_id en paramétre
	
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id?view=full&expand=None&apikey=".$hash_conf{api_cle_bibs};

	print "[".$chaine_rq."]\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=1; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n##############################","\n";
			#print $res->content;
			print "\nRetour appel API OK pour $mms_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print $res->content,"\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
			}
		}	
	}
	
	return $status;
}


############## fonction API POST pour ajouter un portfolio : sur la base du MMS_id / URL
sub post_create_portfolio_mms_id{
		my ($mms_id,$bloc_portfolio)=@_;  # on récupére le mms_id et bloc portfolio en paramétre
		my $block_xml="";
		my $res="";
		
		# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id/portfolios/?apikey=".$hash_conf{api_cle_bibs};

	#print $chaine_rq,"\n";
	print $bloc_portfolio,"\n";
	my $req = HTTP::Request->new(POST => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	$req->content($bloc_portfolio);

	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=1; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		$res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			print $res->content;
			$block_xml=$res->content; # block retour suite appel api post : on est censé avoir un block portfolio XML 
			print "Retour appel API OK pour $mms_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			$block_xml=$res->content;
			print "\n############################","\n";
			print $res->status_line, "\n";
			print $block_xml,"\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
			}
		}
	}
	
}


############## fonction API GET pour questionner Alma : Recuperer les blocs Portfolio sur la base du MMS_id
sub get_portfolios_from_mms_id{
	my ($mms_id)=@_;  # on récupére le mms_id en paramétre
	
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id/portfolios?apikey=".$hash_conf{api_cle_bibs};
	#print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			#print $res->content;
			print "Retour appel API OK pour $mms_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}


############## fonction API GET pour questionner Alma : Recuperer les identifiant Portfolio sur la base du MMS_id
sub get_portfolios_detail_from_mms_id_and_portfolio_id{
	my ($mms_id,$portfolio_id)=@_;  # on récupére le mms_id et le portfolio id en paramétre
	
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/bibs/$mms_id/portfolios/$portfolio_id?apikey=".$hash_conf{api_cle_bibs};
	#print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			#print $res->content;
			print "Retour appel API OK pour $mms_id --- $portfolio_id\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}
