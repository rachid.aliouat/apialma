#!/usr/bin/perl -w
##################
# Rachid Aliouat
# Le 15/10/2021
# API analytics avec un rapport statique sur les nouveautes electronique : e_inventory
# 
#################
use strict;
use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use URI::Encode qw(uri_encode uri_decode);; #encoder decoder les URL   - requiert la bibliotheque liburi-encode-perl

my $token="";
my @tab_nouveautes=();
my $indice=0;
my $j=0;
my $nom_fic_conf="";
my %hash_entete=();
my %hash_doublons=(); #stocker chaque item unique : s'en servire dans un test avant écriture du bloc item dans xml

$nom_fic_conf=$ARGV[0];
##### LIRE LE FICHIER DE CONF
my %hash_conf=();
open (CONF, $nom_fic_conf) or die "Ouverture fichier de configuration $nom_fic_conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
my @tab_inter = <CONF>; 
close(CONF);
my $i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	print $tab_inter[$i];
	my @ligne=split('\t',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf

}
print "-" x 20, "\n";
#################### Mise en place du filtre CODE-BIB et DATE-FROM
#my @cejour = localtime($_[0]);
my $cejour=time();
my $date_extraction= $cejour - ($hash_conf{nbr_jours} * 24 * 60 * 60 );
my @datestring = localtime($date_extraction);
#my @cejour = localtime($_[0]);
my $cejour_jour= $datestring[3];
my $cejour_mois= $datestring[4]+1;
my $cejour_annee= $datestring[5]+1900;
my $date_requete_fr=($cejour_jour>9?"$cejour_jour":"0$cejour_jour")."/".($cejour_mois>9?"$cejour_mois":"0$cejour_mois")."/".$cejour_annee;
my $date_requete_us=$cejour_annee."-".($cejour_mois>9?"$cejour_mois":"0$cejour_mois")."-".($cejour_jour>9?"$cejour_jour":"0$cejour_jour");
print "Extaction des nouveautes depuis le $date_requete_fr (us = $date_requete_us )pour la bibliothèque $hash_conf{code_bib}\n";
my $prompt_filtre = $hash_conf{prompt};
$prompt_filtre=~ s/CODE-BIB/$hash_conf{code_bib}/g; # on y met le Code bib
$prompt_filtre=~ s/DATE-FROM/$date_requete_us/g; # on y met la date from
print $prompt_filtre,"\n";

#exit();

########### Appel de l'API analytics 

my $twig_rapport=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
);

#my $block_xml=get_alma_analytics_by_name($hash_conf{rapport_prompt},$prompt_filtre,""); # Appel de l'API analytics sur le nom d'un rapport dans le fichier de conf
my $block_xml=get_alma_analytics_by_name($hash_conf{rapport_prompt},"",""); # Appel de l'API analytics sur le nom d'un rapport dans le fichier de conf
$block_xml =~/<ResumptionToken>(.*)<\/ResumptionToken>/;
$token =$1;
$twig_rapport->parse($block_xml); # ici tout est dans la structure twig
print $twig_rapport->sprint; # affichage du contenu

aliment_tab_indices ($twig_rapport);
#exit();

aliment_tab_nouveautes($twig_rapport);


# si <IsFinished>false</IsFinished> existe. Alors c'est que l'on a pas tout la liste. Il faut rappeler la requete Analytics en mettant le token à la place du chemin
while ($block_xml =~/<IsFinished>false<\/IsFinished>/) {

	$block_xml=get_alma_analytics_by_name("","",$token); # Appel de l'API analytics sur le token
	$twig_rapport->parse($block_xml); # ici tout est dans la structure twig
	#print $twig_rapport->sprint; # affichage du contenu
	aliment_tab_nouveautes($twig_rapport);

}
print "\n";

open (FIC, ">".$hash_conf{code_bib}."-liste-portfolio.csv") or die "Ouverture fichier de liste portfolio CSV impossible , cause : $! \n";
binmode(FIC, ":utf8");
open (XML, ">".$hash_conf{code_bib}."-liste-portfolio.xml") or die "Ouverture fichier de liste portfolio CSV impossible , cause : $! \n";
binmode(XML, ":utf8");
print XML "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<root>\n";

### creer la ligne d'entete dans le CSV
#0	Author	ISBN	MMS Id	Publication Date	Publisher	Title	Portfolio Activation Date	Portfolio Creation Date	Library Code	PO Creator	PO Line Identifier	PO Number	Reporting Code - 1st	Reporting Code - 2nd	Reporting Code - 3rd	Portfolio Id
#my $entete="[Author]\t[ISBN]\t[MMS_ID]\t[Publication_Date]\t[Publisher]\t[Title]\t[Portfolio_Activation_Date]\t[Portfolio_Creation_Date]\t[Library_Code]\t[PO Creator]\t[PO_Line_Identifier]\t[PO Number]\t[Reporting Code - 1st]\t[Reporting Code - 2nd]\t[Reporting Code - 3rd]\t[Portfolio_Id]\n";
my $entete="[item_Author_contrib]\t[item_Author]\t[item_ISBN_NORM]\t[item_ISBN]\t[item_MMS_Id]\t[item_Publication_Date]\t[item_Publisher]\t[item_Title]\t[item_Permanent_Call_Number]\t[item_Item_Creation_Date]\t[item_Library_Code]\t[item_Location_Code]\t[item_Barcode]\t[item_Acquisition_Method]\t[item_PO_Line_Creation_Date]\t[item_PO_Line_Identifier]\t[item_PO_Line_Reference]\t[item_libelle_collection]\t[item_note_interne_3]\t[item_acq_code_primaire]\t[item_acq_code_secondaire]\t[item_acq_code_tertiaire]\t[code_stat1]\t[code_stat2]\t[code_stat3]\n";
print $entete;
print FIC $entete;


for ($i=0;$i<=$#tab_nouveautes;$i++) {
	# on filtre sur le code BU présent dans la colonne PO Number (colonne 19)
	if ($tab_nouveautes[$i][17]=~ /$hash_conf{code_bib}/) {
		for ($j=1;$j<=25;$j++) {
			print $tab_nouveautes[$i][$j],"\t";
			print FIC $tab_nouveautes[$i][$j],"\t";
		}
		print "\n";
		print FIC "\n";
		if($hash_doublons{$tab_nouveautes[$i][5].$tab_nouveautes[$i][12].$tab_nouveautes[$i][9]}<1){# il n'a pas déjà été écrit
			print XML "<item>\n";
			print XML "\t<bib>".$tab_nouveautes[$i][11]."</bib>\n";
			print XML "\t<num>".$tab_nouveautes[$i][13]."</num>\n";
			print XML "\t<vignette_mini>https://images-eu.ssl-images-amazon.com/images/P/".$tab_nouveautes[$i][4].".08.THUMBZZZ.jpg</vignette_mini>\n";
			print XML "\t<vignette_maxi>https://images-eu.ssl-images-amazon.com/images/P/".$tab_nouveautes[$i][4].".THUMBZZZ.jpg</vignette_maxi>\n";
			print XML "\t<url>".$hash_conf{prefixe_url}.$tab_nouveautes[$i][5]."</url>\n";
			print XML "\t<titre>".$tab_nouveautes[$i][8]."</titre>\n";
			print XML "\t<auteur>".((length($tab_nouveautes[$i][2])>0)?$tab_nouveautes[$i][2]:$tab_nouveautes[$i][1])."</auteur>\n";
			print XML "\t<annee>".$tab_nouveautes[$i][6]."</annee>\n";
			print XML "\t<editeur>".$tab_nouveautes[$i][7]."</editeur>\n";
			print XML "\t<isbn>".$tab_nouveautes[$i][3]."</isbn>\n";
			print XML "\t<maj>".((length($tab_nouveautes[$i][15])>2)?$tab_nouveautes[$i][15]:$tab_nouveautes[$i][10])."</maj>\n";
			print XML "\t<collection>".$tab_nouveautes[$i][12]."</collection>\n";
			print XML "\t<cote>".$tab_nouveautes[$i][9]."</cote>\n";
			print XML "\t<libelle_collection>".$tab_nouveautes[$i][18]."</libelle_collection>\n";
			print XML "\t<note_interne_3>".$tab_nouveautes[$i][19]."</note_interne_3>\n";
			print XML "\t<acq_code_primaire>".$tab_nouveautes[$i][20]."</acq_code_primaire>\n";
			print XML "\t<acq_code_secondaire>".$tab_nouveautes[$i][21]."</acq_code_secondaire>\n";
			print XML "\t<acq_code_tertiaire>".$tab_nouveautes[$i][22]."</acq_code_tertiaire>\n";
			print XML "\t<code_stat1>".$tab_nouveautes[$i][23]."</code_stat1>\n";
			print XML "\t<code_stat2>".$tab_nouveautes[$i][24]."</code_stat2>\n";
			print XML "\t<code_stat3>".$tab_nouveautes[$i][25]."</code_stat3>\n";
			print XML "</item>\n";
			$hash_doublons{$tab_nouveautes[$i][5].$tab_nouveautes[$i][12].$tab_nouveautes[$i][9]}++; #on memorise mms_id - collection - cote
		}

	}	

	}

close (FIC);
print XML "</root>\n";
close (XML);

################ Fonctions
######## Fonction alimente tableau indice de colonne pour chaque libellé de colonne car l'ordre des colonnes peut changer si on modifie le rapport analytics
#Column0-> 0
#Column1-> Author (contributor)
#Column2-> Author
#Column3-> ISBN (Normalized)
#Column4-> ISBN
#Column5-> MMS Id
#Column6-> Publication Date
#Column7-> Publisher
#Column8-> Title
#Column9-> Permanent Call Number
#Column10-> Item Creation Date
#Column11-> Library Code
#Column12-> Location Code
#Column13-> Location Name
#Column14-> Barcode
#Column15-> Internal Note 3
#Column16-> Acquisition Method
#Column17-> PO Line Creation Date
#Column18-> PO Line Identifier
#Column19-> PO Line Reference (PO Number)
#Column20-> Reporting Code - Secondary


sub aliment_tab_indices{
		my ($twig_rapport)=@_;
	my $root= $twig_rapport->get_xpath('//rowset/xsd:schema/xsd:complexType/xsd:sequence',0); 
	#my $entete=defined $root->first_child('xsd:sequence')?$root->first_child('xsd:sequence')->text():"";
	#print $root->sprint; # affichage du contenu
	print "\n";
	
	my @les_colonnes= $root->children('xsd:element');  ### on récuperer une table de hash de tous les "enfants" du root 

	foreach my $une_colonne (@les_colonnes)      # liste chaque nouveaute de la liste des nouveautes
		{
			#print $une_colonne->sprint;
			#print "\n";
			my $numero_col= $une_colonne->{'att'}->{'name'}; # get the attribute
			#print "\nNumero de colonne : $numero_col\n";
			#$numero_col =~ /Column(\d+)/;
			#my $nbr=$1;
			my $nom_col= $une_colonne->{'att'}->{'saw-sql:columnHeading'}; # get the attribute
			#print "\nNom de colonne : $nom_col\n";
			$hash_entete{$nom_col} = $numero_col;
			print $hash_entete{$nom_col} . "-> ".$nom_col."\n";
		}

}


######## Fonction alimente tableau des nouveautes
sub aliment_tab_nouveautes{
	my ($twig_rapport)=@_;
	#print $twig_rapport->sprint;
	#print "----fonction aliment_tab_nouveautes----\n";
	#my $root= $twig_rapport->root->first_child('ResultXml')->first_child('rowset');  
	my $root= $twig_rapport->get_xpath('//rowset',0); 
	my @les_nouveautes= $root->children('Row');  ### on récuperer une table de hash de tous les "enfants" du root = les entrées <Row> de la racine <rowset>
	#print "#########################\n";
	#print "####".$root->first_child('Row')->first_child('Column12')->text()."####\n";
	foreach my $une_nouveaute (@les_nouveautes)      # liste chaque nouveaute de la liste des nouveautes
		{ 
				#print "############\n";
				my $item_zero=defined $une_nouveaute->first_child($hash_entete{'0'})?$une_nouveaute->first_child($hash_entete{'0'})->text():"";
				my $item_Author_contrib="";#defined $une_nouveaute->first_child($hash_entete{'Author (contributor)'})?$une_nouveaute->first_child($hash_entete{'Author (contributor)'})->text():"";
				#$item_Author_contrib=~ s/\&/[et]/g; # remplace le & commercial par [et] sinon on casse le XML
				#$item_Author_contrib=epure_xml($item_Author_contrib);
				my $item_Author=defined $une_nouveaute->first_child($hash_entete{'Author'})?$une_nouveaute->first_child($hash_entete{'Author'})->text():"";
				#$item_Author=~ s/\&/[et]/g; # remplace le & commercial par [et] sinon on casse le XML
				$item_Author=epure_xml($item_Author);
				my $item_ISBN_NORM="";#defined $une_nouveaute->first_child($hash_entete{'ISBN (Normalized)'})?$une_nouveaute->first_child($hash_entete{'ISBN (Normalized)'})->text():"";
				my $item_ISBN=defined $une_nouveaute->first_child($hash_entete{'ISBN'})?$une_nouveaute->first_child($hash_entete{'ISBN'})->text():"";
				my $item_MMS_Id=defined $une_nouveaute->first_child($hash_entete{'MMS Id'})?$une_nouveaute->first_child($hash_entete{'MMS Id'})->text():"";
				my $item_Publication_Date=defined $une_nouveaute->first_child($hash_entete{'Publication Date'})?$une_nouveaute->first_child($hash_entete{'Publication Date'})->text():"";
				my $item_Publisher=defined $une_nouveaute->first_child($hash_entete{'Publisher'})?$une_nouveaute->first_child($hash_entete{'Publisher'})->text():"";
				#$item_Publisher=~ s/\&/[et]/g; # remplace le & commercial par [et] sinon on casse le XML
				$item_Publisher=epure_xml($item_Publisher);
				my $item_Title=defined $une_nouveaute->first_child($hash_entete{'Title'})?$une_nouveaute->first_child($hash_entete{'Title'})->text():"";
				#$item_Title=~ s/\&/[et]/g; # remplace le & commercial par [et] sinon on casse le XML
				$item_Title=epure_xml($item_Title);
				my $item_Permanent_Call_Number="";#defined $une_nouveaute->first_child($hash_entete{'Permanent Call Number'})?$une_nouveaute->first_child($hash_entete{'Permanent Call Number'})->text():"";
				#$item_Permanent_Call_Number=epure_xml($item_Permanent_Call_Number);
				my $item_Item_Creation_Date=defined $une_nouveaute->first_child($hash_entete{'Portfolio Activation Date'})?$une_nouveaute->first_child($hash_entete{'Portfolio Activation Date'})->text():"";
				my $item_Library_Code=defined $une_nouveaute->first_child($hash_entete{'Library Code'})?$une_nouveaute->first_child($hash_entete{'Library Code'})->text():"";
				my $item_Location_Code="";#defined $une_nouveaute->first_child($hash_entete{'Location Code'})?$une_nouveaute->first_child($hash_entete{'Location Code'})->text():"";
				my $item_Barcode=defined $une_nouveaute->first_child($hash_entete{'Portfolio Id'})?$une_nouveaute->first_child($hash_entete{'Portfolio Id'})->text():"";
				my $item_Acquisition_Method="";#defined $une_nouveaute->first_child($hash_entete{'Acquisition Method'})?$une_nouveaute->first_child($hash_entete{'Acquisition Method'})->text():"";
				my $item_PO_Line_Creation_Date=defined $une_nouveaute->first_child($hash_entete{'Portfolio Creation Date'})?$une_nouveaute->first_child($hash_entete{'Portfolio Creation Date'})->text():"";
				my $item_PO_Line_Identifier=defined $une_nouveaute->first_child($hash_entete{'PO Line Identifier'})?$une_nouveaute->first_child($hash_entete{'PO Line Identifier'})->text():"";
				my $item_PO_Line_Reference=defined $une_nouveaute->first_child($hash_entete{'PO Number'})?$une_nouveaute->first_child($hash_entete{'PO Number'})->text():"";
				my $item_libelle_collection ="" ;#defined $une_nouveaute->first_child($hash_entete{'Location Name'})?$une_nouveaute->first_child($hash_entete{'Location Name'})->text():"";
				my $item_note_interne_3 = "";#defined $une_nouveaute->first_child($hash_entete{'Internal Note 3'})?$une_nouveaute->first_child($hash_entete{'Internal Note 3'})->text():"";
				#$item_note_interne_3=epure_xml($item_note_interne_3);
				my $item_acq_code_primaire =defined $une_nouveaute->first_child($hash_entete{'Reporting Code - 1st'})?$une_nouveaute->first_child($hash_entete{'Reporting Code - 1st'})->text():"";
				my $item_acq_code_secondaire =defined $une_nouveaute->first_child($hash_entete{'Reporting Code - 2nd'})?$une_nouveaute->first_child($hash_entete{'Reporting Code - 2nd'})->text():"";
				my $item_acq_code_tertiaire =defined $une_nouveaute->first_child($hash_entete{'Reporting Code - 3rd'})?$une_nouveaute->first_child($hash_entete{'Reporting Code - 3rd'})->text():"";
				my $item_code_stat1 ="";#defined $une_nouveaute->first_child($hash_entete{'Statistics Note 1'})?$une_nouveaute->first_child($hash_entete{'Statistics Note 1'})->text():"";
				my $item_code_stat2 ="";#defined $une_nouveaute->first_child($hash_entete{'Statistics Note 2'})?$une_nouveaute->first_child($hash_entete{'Statistics Note 2'})->text():"";
				my $item_code_stat3 ="";#defined $une_nouveaute->first_child($hash_entete{'Statistics Note 3'})?$une_nouveaute->first_child($hash_entete{'Statistics Note 3'})->text():"";
				# correction virgule dans auteur : rechercher remplacer espace-virgule_espace par virgule-espace
				#$item_Author_contrib =~ s/ , /, /g;
				$item_Author =~ s/ , /, /g;
				#print "[$item_zero][$item_Author_contrib][$item_Author][$item_ISBN_NORM][$item_ISBN][$item_MMS_Id][$item_Publication_Date][$item_Publisher][$item_Title]
				#[$item_Permanent_Call_Number][$item_Item_Creation_Date][$item_Library_Code][$item_Location_Code][$item_Barcode][$item_Acquisition_Method]
				#[$item_PO_Line_Creation_Date][$item_PO_Line_Identifier][$item_PO_Line_Reference]\n";
				$tab_nouveautes[$indice][0]=$item_zero;
				$tab_nouveautes[$indice][1]="";#$item_Author_contrib;
				$tab_nouveautes[$indice][2]=$item_Author;
				$tab_nouveautes[$indice][3] = "";
				if ($item_ISBN_NORM =~ /.*(\d{13}).*/) {
					$tab_nouveautes[$indice][3] = $1; # on y met ISBN 13
				} 
				$tab_nouveautes[$indice][4]="";

				if ($item_ISBN =~ /.*(\d{9}[\dX]).*/ ) { 
					$tab_nouveautes[$indice][4]=$1; # on y met ISBN 10
				}
				$tab_nouveautes[$indice][5]=$item_MMS_Id;
				$tab_nouveautes[$indice][6]="";
				if ($item_Publication_Date =~ /^.*(\d\d\d\d).*$/) {
					
					$tab_nouveautes[$indice][6]=$1;#$item_Publication_Date;
				} 
				
				$tab_nouveautes[$indice][7]=$item_Publisher;
				$tab_nouveautes[$indice][8]=$item_Title;
				if ($item_Permanent_Call_Number =~ /Unknown/) {
					$item_Permanent_Call_Number="";
				}
				$tab_nouveautes[$indice][9]=$item_Permanent_Call_Number;
				
				if ($item_Item_Creation_Date =~ /\d\d\d\d.\d\d.\d\d/) {
					$item_Item_Creation_Date =~ /(\d\d\d\d).(\d\d).(\d\d)/;
					$tab_nouveautes[$indice][10]=$3."/".$2."/".$1;#$item_Item_Creation_Date;
					} elsif ($item_Item_Creation_Date =~ /.\d\d\d\d.$/) {
						$tab_nouveautes[$indice][10]="01/01/".$item_Item_Creation_Date;
						
					} else {
						$tab_nouveautes[$indice][10]="";
					}
				$tab_nouveautes[$indice][11]=$item_Library_Code;
				$tab_nouveautes[$indice][12]=$item_Location_Code;
				$tab_nouveautes[$indice][13]=epure_xml($item_Barcode);
				$tab_nouveautes[$indice][14]=$item_Acquisition_Method;
				#$tab_nouveautes[$indice][15]=$item_PO_Line_Creation_Date;
				if ($item_PO_Line_Creation_Date =~ /\d\d\d\d.\d\d.\d\d/) {
					$item_PO_Line_Creation_Date =~ /(\d\d\d\d).(\d\d).(\d\d)/;
					$tab_nouveautes[$indice][15]=$3."/".$2."/".$1;#$item_Item_Creation_Date;
					} elsif ($item_PO_Line_Creation_Date =~ /.\d\d\d\d.$/) {
						$tab_nouveautes[$indice][15]="01/01/".$item_PO_Line_Creation_Date;
						
					} else {
						$tab_nouveautes[$indice][15]="";
					}
				
				
				$tab_nouveautes[$indice][16]=$item_PO_Line_Identifier;
				$tab_nouveautes[$indice][17]=$item_PO_Line_Reference;
				$tab_nouveautes[$indice][18]=$item_libelle_collection;
				$tab_nouveautes[$indice][19]=$item_note_interne_3;
				$item_acq_code_primaire =~ s/\-1//g; # parfois la donnée vaut -1 si valeur vide : alors on efface
				$item_acq_code_secondaire =~ s/\-1//g; # parfois la donnée vaut -1 si valeur vide : alors on efface
				$item_acq_code_tertiaire =~ s/\-1//g; # parfois la donnée vaut -1 si valeur vide : alors on efface
				$tab_nouveautes[$indice][20]=$item_acq_code_primaire; 
				$tab_nouveautes[$indice][21]=$item_acq_code_secondaire; 
				$tab_nouveautes[$indice][22]=$item_acq_code_tertiaire;
				$tab_nouveautes[$indice][23]=$item_code_stat1; 
				$tab_nouveautes[$indice][24]=$item_code_stat2; 
				$tab_nouveautes[$indice][25]=$item_code_stat3;  
			$indice++;
			#### nettoyage des champs intermédiaires:
			$item_zero=$item_Author_contrib=$item_Author=$item_ISBN_NORM=$item_ISBN=$item_MMS_Id=$item_Publisher="";
			$item_Title=$item_Permanent_Call_Number=$item_Item_Creation_Date=$item_Library_Code=$item_Location_Code=$item_Barcode=$item_Acquisition_Method="";
			$item_PO_Line_Creation_Date=$item_PO_Line_Identifier=$item_PO_Line_Reference=$item_libelle_collection=$item_note_interne_3="";
			$item_acq_code_primaire=$item_acq_code_secondaire=$item_acq_code_tertiaire="";
			$item_code_stat1=$item_code_stat1=$item_code_stat1="";
			
		}
	
}

############## fonction API GET pour questionner Alma API Analytics: Recuperer les données sur la base du nom du rapport
sub get_alma_analytics_by_name{
	my ($nom_rapport,$prompt_filtre,$token)=@_;  # on récupére le nom du rapport ou le token en paramétre
	my $uri     = URI::Encode->new( { encode_reserved => 0 } );
	my $nom_rapport_encoded = $uri->encode($nom_rapport);
	my $prompt_encoded=$uri->encode($prompt_filtre);

	#my $nom_rapport_encoded = uri_encoded($nom_rapport);
	print "$nom_rapport\n$nom_rapport_encoded\n";
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="";
	if (length($nom_rapport)>0) {
		print "-----appel initiale----\n";
		#$chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?path=$nom_rapport_encoded&limit=25&col_names=true&apikey=".$hash_conf{api_cle_analytics};
		$chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?path=$nom_rapport_encoded&limit=25&col_names=true&apikey=".$hash_conf{api_cle_analytics}."&filter=".$prompt_encoded;
	}
	else { # sinon, ce n'est pas l'appel initial, mais un appel qui demande la suite de la liste via le token
		print "-----appels secondaires suite de la liste----\n";
		$chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/analytics/reports?token=$token&limit=25&col_names=true&apikey=".$hash_conf{api_cle_analytics};

	}
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_analytics}, $hash_conf{api_cle_analytics}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "##############################","\n";
			print $res->content;
			print "Existence notice OK pour $nom_rapport\n";
			print "##############################","\n";
			$status=$res->content;
		} else {
			print $res->content;
			print "\n############################","\n";
			print $res->status_line, "\n";
			print "\n############################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}


########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	#$res=~ s/\"//g;
	
	return $res;
}

sub epure_xml { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\&/&amp;/g;
	$res=~ s/<//g;
	$res=~ s/>//g;
	
	return $res;
}
