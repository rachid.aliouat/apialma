<?php


//$url = 'BUSHS-liste-portfolio.xml';
$fic_xml =  $_GET['flux'];

$document_xml = new DomDocument();
$document_xml->load($fic_xml);

// entete
$page="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
$page.="<html>\n";
$page.= "<head>\n";
$page.= "<title>Visualisation du flux ".$url."</title>\n";
$page.="<meta name=\"robots\" content=\"noindex,nofollow\" />\n";
$page.="<meta name=\"googlebot\" content=\"noarchive\" />\n";
$page.= "</head>\n";
$page.= "<body>\n";
$page.="<table border=0>\n";

//renvoie ts les noeuds
$elements = $document_xml->getElementsByTagName('item');


foreach($elements as $element) {
    //if ( $element->attributes->item(0)->value == 'true' )
//		$libellesMention[$element->getElementsByTagName('termIdentifier')->item(0)->nodeValue] = $element->getElementsByTagName('langstring')->item(0)->nodeValue;
    $titre=$element->getElementsByTagName('titre')->item(0)->nodeValue;
    $vignette_mini=$element->getElementsByTagName('vignette_mini')->item(0)->nodeValue;
    $vignette_maxi=$element->getElementsByTagName('vignette_maxi')->item(0)->nodeValue;
    $vignette_electre=$element->getElementsByTagName('vignette_electre')->item(0)->nodeValue;
    $auteur=$element->getElementsByTagName('auteur')->item(0)->nodeValue;
    $annee=$element->getElementsByTagName('annee')->item(0)->nodeValue;
    $url=$element->getElementsByTagName('url')->item(0)->nodeValue;    
    $maj=$element->getElementsByTagName('maj')->item(0)->nodeValue;

    

$page.="<tr>\n";
$page.="<td>\n";
$page.="<img src=\"". $vignette_maxi ."\" style=\"margin-left: 5px; margin-right: 8px; width:100px;\"  />";
$page.="</td>\n";
$page.="<td>\n";
$page.="<img src=\"". $vignette_electre ."\" style=\"margin-left: 5px; margin-right: 8px; width:100px;\"  />";
$page.="</td>\n";
$page.="<td>\n";
$page.= "<em><strong>".$titre."</strong></em>\n";
$page.= "<br />".$auteur."\n";
$page.= "<br />Publication :".$annee."\n";
$page.= "<br />Disponibilité : <a href=\"" .$url. "\" target=\"_blank\">voir CATALOGUE</a>\n";
$page.= "<br /><span style=\"color:#AAA; font-size: smaller;\">[depuis le :". $maj ."]</span>\n";
$page.="</td>\n";
$page.="</tr>\n";
$page.="<tr><td colspan=\"2\"><hr /></td></tr>";
}


$page.="</table>\n";
$page.= "</body>\n";
$page.="</html>\n";
echo $page;


?>
