#!/usr/bin/perl -w
##################
# Rachid Aliouat
# Le 23/05/2019
# poser une réservation : données nécéssaire =  user_id +  mms_id (resa sur titre) ou item_pid (resa sur exemplaire)
# le 19/07/2019 : générer un fichier rapport : echec et réussite
#######################
### Pour connaitre le item_id , il faut d'abord faire un get item avec le code_barre
use strict;
use warnings;
use LWP::UserAgent; 	# bibliothèque pour les webservice
use XML::Twig;			# bibliothèque pour le XML
use MIME::Lite;			# bibliothèque pour envoye un mail (rapport)

my @table_des_demandes=();
my @ligne=();

##### LIRE LE FICHIER DE CONF
my %hash_conf=();
open (CONF, "config.conf") or die "Ouverture fichier de configuration config.conf impossible , cause : $! \n";
print "lecture de la configuration:\n";
print "-" x 20, "\n";
my @tab_inter = <CONF>; 
close(CONF);
my $i=0;
for ($i=0;$i<=$#tab_inter;$i++) {
	print $tab_inter[$i];
	my @ligne=split('=',$tab_inter[$i]); # decoupage ligne
	$hash_conf{$ligne[0]}=epure($ligne[1]); # alimentation de la table de hash avec la conf

}
print "-" x 20, "\n";

############## Fichier Rapport
open (RAP,">rapport.txt") or die "Ouverture fichier rapport.txt impossible , cause : $! \n";
print RAP "Rapport d'injection des reservations\n---------------------\n";
##############


# Lire le fichier csv passé en parametre : lecteur / exemplaire / date souhaitée(formaté 2019-06-01) / bib de retrait (si pas de bib de retrait = bib de l'exemplaire)
my $nom_fic=$ARGV[0];

print "traitement du fichier $nom_fic\n";

####
########### Lire le fichier CSV des demandes : lecteur / exemplaire / date souhaitée / bib de retrait (si pas de bib de retrait = bib de l'exemplaire)
#####
open (FIC, $nom_fic) or die "Ouverture fichier des demandes impossible , cause : $! \n";
print "lecture du fichier des demandes:\n";
print "-" x 20, "\n";
@tab_inter = <FIC>; 
close(FIC);

for ($i=1;$i<=$#tab_inter;$i++) {
	#print $tab_inter[$i];
	@ligne=split('\t',$tab_inter[$i]); # decoupage ligne
	$table_des_demandes[$i][0]=epure($ligne[0]); # lecteur
	$table_des_demandes[$i][1]=epure($ligne[1]); # exemplaire
	$table_des_demandes[$i][2]=epure($ligne[2]); # date
	$table_des_demandes[$i][3]=epure($ligne[3]); # bib retrait
	print "Lecteur ".$table_des_demandes[$i][0] ." demande exemplaire ". $table_des_demandes[$i][1] ." au plus pour le ". $table_des_demandes[$i][2] ." dans la bib de retrait ". (length($table_des_demandes[$i][3])>1?$table_des_demandes[$i][3]:"de l'exemplaire.")."\n";

}

####
########### recuperer tous les tuples item_pid/code barre et autres information (=> api get item)
#####
my $twig=XML::Twig->new( # on créer un objet TWIG dans lequel on met toute la structure XML
		
        pretty_print  => 'indented',
);

print "Récupératon des données exemplaire pour avoir le item_pid entre autre :\n\------\n";
for ($i=1;$i<=$#table_des_demandes;$i++) {
	print "demande n° $i pour le lecteur $table_des_demandes[$i][0] et l'exemplaire $table_des_demandes[$i][1]...\n";
	my $block_xml=get_item($table_des_demandes[$i][1]);
	if ($block_xml =~ /item/) { ## exemplaire ok 
	$twig->parse($block_xml); # ici tout est dans la structure twig
	#print $twig->sprint; # affichage du contenu
	
	my $root= $twig->root->first_child('item_data');  
	my $item_pid = $root->first_child('pid')->text;
	my $item_cb = $root->first_child('barcode')->text;
	my $item_type = $root->first_child('physical_material_type')->text;
	my $item_policy = $root->first_child('policy')->text;
	my $item_process_type = $root->first_child('process_type')->text;
	my $item_library = $root->first_child('library')->text;
	my $item_location = $root->first_child('location')->text;
	my $item_titre_auteur = $root->first_child('title_abcnph')->text;
	
	print "\npid : $item_pid\n";
	print "code barre : $item_cb\n";
	print "type materiel : $item_type\n";
	print "droit de pret : $item_policy\n";
	print "situation actuelle : $item_process_type\n";
	print "bibliotheque : $item_library\n";
	print "localisation : $item_location\n";
	print "titre / auteur : $item_titre_auteur\n";
	
	$root= $twig->root->first_child('holding_data');  
	my $holding_call = $root->first_child('call_number')->text;
	print "cote : $holding_call\n";
	
	if (length($table_des_demandes[$i][3])<1 ) { #on impose la bib de retrait si elle n'a pas été précisé dans le csv
		$table_des_demandes[$i][3]=$item_library;
	}
	$table_des_demandes[$i][4]=$item_pid;#numero item_pid  
	$table_des_demandes[$i][5]=$item_type;#type materiel (book)
}
	
}
print "\n--------------------\n";
####
########### générer les requetes via API requests  
#####

print "Injection des demandes par API :\n\------\n";
for ($i=1;$i<=$#table_des_demandes;$i++) {
	
	#Contruire le bloc XML
	
	
	my $block_xml="<user_request>";
    $block_xml.=  "<user_primary_id>".$table_des_demandes[$i][0]."<\/user_primary_id>";
    $block_xml.=  "<request_type>HOLD<\/request_type>";
    $block_xml.=  "<request_sub_type desc=\"Patron physical item request\">PATRON_PHYSICAL<\/request_sub_type>";
    $block_xml.=  "<pickup_location_type>LIBRARY<\/pickup_location_type>";
    $block_xml.=  "<pickup_location_library>".$table_des_demandes[$i][3]."<\/pickup_location_library>";
    $block_xml.=  "<material_type>".$table_des_demandes[$i][5]."<\/material_type>";
    $block_xml.=  "<last_interest_date>".$table_des_demandes[$i][2]."<\/last_interest_date>";
    $block_xml.=  "<comment>demande par API<\/comment>";
	$block_xml.="<\/user_request>";
	print $block_xml,"\n###############\n###############\n";
	# ################### QUESTIONER API POST pour créer la demande
   my $reponse=post_demande($table_des_demandes[$i][0],$table_des_demandes[$i][4],$block_xml); # appel de la fonction qui va questionner le webservice POST Loans pour faire le prêt
   if ($reponse =~ /OK/) { # la pret a eu lieu
	   	print "CREATION de la Demande OK (core)\n";
	   	#$nb_creation++;
	   	#$liste_creation_ok.="$table_des_demandes[$i][0]\t$table_des_demandes[$i][1]\n";
	   	print "$table_des_demandes[$i][0]\t$table_des_demandes[$i][1]\n";
	   	print RAP "CREATION RESERVATION OK : $table_des_demandes[$i][0]\t$table_des_demandes[$i][1]\n";
		
	} else { # Echec de la création de la demande
		print "CREATION De la demande ECHEC (core)\n";
		print "################\n";
		#$block_xml = $un_pret->sprint;
		#$nb_echec_creation++;
		#$rapport.="ECHEC  : CODE LECTEUR $table_des_demandes[$i][0]  --- CODE BARRE LIVRE $table_des_demandes[$i][1] \n";
		print "ECHEC  : CODE LECTEUR $table_des_demandes[$i][0]  --- CODE BARRE LIVRE $table_des_demandes[$i][1] \n";
		print RAP "ECHEC  : CODE LECTEUR $table_des_demandes[$i][0]  --- CODE BARRE LIVRE $table_des_demandes[$i][1] \n";
		

   }

}

close (RAP); # fermeture fichier rapport

############################
########## Fonctions
############################

############## fonction API GET pour questionner Alma : Recuperer toutes les information d'un exemplaires
sub get_item{
	my ($cb)=@_;  # on récupére l'uid en paramétre
	
	# Creer un agent pour questionner le webservice = céer un agent c'est comme ouvrir un navigateur internet

	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/items?view=label&item_barcode=$cb&apikey=".$hash_conf{api_cle_bibs};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(GET => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_bibs}, $hash_conf{api_cle_bibs}); # login  + mot de passe => user_api + cle api
	
	# Passer requete au user agent et récuperer la reponse
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /^NOK$/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n#############################################################################################","\n";
			#print $res->content;
			print "\nExistence exemplaire OK pour $cb";
			print "\n#############################################################################################","\n";
			$status=$res->content;
		} else {
			print "\n#############################################################################################","\n";
			print $res->status_line, "\n";
			print "\n#############################################################################################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
	
	return $status;
}


############## fonction API POST pour créer la demande dans Alma 
sub post_demande{
		my ($uid,$item,$fiche_xml)= @_;
	my $fiche_a_importer="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" . $fiche_xml ;
	print "\n#############################################################################################","\n";
	print "AJOUT de la demande pour le lecteur lecteur $uid\n";
	print "\n#############################################################################################","\n";
	print "CONTENU DE LA FICHE :\n";
	print "\n######################## FICHE XML #####################################################################","\n";
	print $fiche_a_importer,"\n";
	print "\n######################## FICHE XML #####################################################################","\n";
	my $ua = LWP::UserAgent->new;
	$ua->agent("MyApp/0.1");
	# Creer la requete
	my $chaine_rq="https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/$uid/requests?user_id_type=all_unique&item_pid=$item&apikey=".$hash_conf{api_cle_users};
	print $chaine_rq,"\n";
	my $req = HTTP::Request->new(POST => $chaine_rq);
	$req->content_type('application/xml');
	$req->authorization_basic($hash_conf{api_user_users}, $hash_conf{api_cle_users}); # login  + mot de passe => user_api + cle api
	$req->content($fiche_a_importer);
	# Passer requete au user agent et récuperer la response
	my $i=0;
	my $max=3; #maximum de tentative
	my $status="NOK";
	while ($status =~ /NOK/){
		my $res = $ua->request($req);
		# Vérifier la reponse
		if ($res->is_success) {
			print "\n#############################################################################################","\n";
			print $res->content;
			print "Ajout OK pour $uid";
			print "\n#############################################################################################","\n";
			$status="OK";
		} else {
			print "\n#############################################################################################","\n";
			print $res->status_line, "\n";
			print $res->content;
			
			print "\n#############################################################################################","\n";
			$i++;
			if ($i==$max) {
				$status="MAX"; # on force la sortie de la boucle meme si c'est pas ok
		}
	}
}
	
		
	return $status;
}


########### fonction Retourne la chaine de caractère passée en paramètre en ne laissant que les caractères autorisés
sub epure { 
	my ($res)=@_; 
	
	$res=~ s/\n//g;
	$res=~ s/\r//g;
	$res=~ s/\"//g;
	
	return $res;
}



############################
sub date_claire { # Passage d'une date et retour d'une date en clair exemple : Lundi 22-09-2008 � 15h00
my @cejour = localtime($_[0]);
my $cejour_jour= $cejour[3];
my $cejour_mois= $cejour[4]+1;
my $cejour_annee= $cejour[5]+1900;
my $cejour_heure= $cejour[2];
my $cejour_minute= $cejour[1];
my $cejour_libelle=("Dimanche","Lundi", "Mardi", "Mercredi" ,"Jeudi", "Vendredi", "Samedi")[$cejour[6]];
#my $texte_date_claire= "$cejour_libelle $cejour_jour-$cejour_mois-$cejour_annee " . chr(133) . " $cejour_heure"."H".(length($cejour_minute)==1?"0" . $cejour_minute:$cejour_minute);
my $texte_date_claire= "$cejour_libelle ".(length($cejour_jour)==1?"0" . $cejour_jour:$cejour_jour)."-".(length($cejour_mois)==1?"0" . $cejour_mois:$cejour_mois)."-$cejour_annee -" . " $cejour_heure"."H".(length($cejour_minute)==1?"0" . $cejour_minute:$cejour_minute);
return  $texte_date_claire;
}
############################
sub date_simple { # Passage d'une date et retour d'une date en clair 
my @cejour = localtime($_[0]);
my $cejour_jour= $cejour[3];
my $cejour_mois= $cejour[4]+1;
my $cejour_annee= $cejour[5]+1900;
my $cejour_heure= $cejour[2];
my $cejour_minute= $cejour[1];
my $cejour_libelle=("Dimanche","Lundi", "Mardi", "Mercredi" ,"Jeudi", "Vendredi", "Samedi")[$cejour[6]];
my $texte_date_claire= (length($cejour_jour)==1?"0" . $cejour_jour:$cejour_jour) . "\/" . (length($cejour_mois)==1?"0" . $cejour_mois:$cejour_mois) . "\/" . $cejour_annee;
return  $texte_date_claire;
}



